<?php
return array(
	//'配置项'=>'配置值'
		/* 模板相关配置 */
		'TMPL_PARSE_STRING' => array(
				'__STATIC__' => __ROOT__ . '/Public/static',
				'__IMG__'    => __ROOT__ . '/Public/home/img',
				'__IMGS__'    => __ROOT__ . '/Public/home/images',
				'__CSS__'    => __ROOT__ . '/Public/home/css',
				'__JS__'     => __ROOT__ . '/Public/home/js',
				'__UPLOADS__' => __ROOT__ . '/Uploads/',
		),
		'DEFAULT_CONTROLLER'    =>  'Index', // 默认控制器名称
);