<?php
/**
 * 编辑器文件上传处理类
 * @package    Edit
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Home\Controller;
use Think\Controller;
class ShareController extends Controller {
	public function __construct()
	{
		parent::__construct();
	}  
    public function share()
	{
		header("Content-Type:text/html;charset=utf-8");
		$tid = intval($_GET["tid"]);
		// echo $tid;exit();

        if ($tid <= 0){
            showMessage('参数错误','','html','error');
        }
        $condition = array();
        $condition['tid'] = $tid;
        $condition['article_show'] = 1;
        $info = M('sectiontopic')->where($condition)->find();
        if(!$info){
            showMessage('记录信息错误','','html','error');
        }
        $logo_url=UrlPath.'Public/api/images/xlqk.png';
        $info['app_logo']=$logo_url;
        $this->assign('info',$info);
        $this->display(); 
        //不指定则访问share.html,	指定方式：$this->display('share.html')	
    }
	
}