<?php
/**
 * 基类
 * @package    Base
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Home\Controller;
use Think\Controller;

class BaseController extends Controller{
	public function __construct()
	{
		parent::__construct();
		//读取配置信息
		$web_stting = F('setting');
		if($web_stting === false) 
		{
			$params = array();
			$list = M('Setting')->getField('name,value');
			foreach ($list as $key=>$val) 
			{
				$params[$key] = unserialize($val) ? unserialize($val) : $val;
			}
			F('setting', $params); 				
			$web_stting = F('setting');
		}
		$this->assign('web_stting',$web_stting);
		//站点状态判断
		if($web_stting['site_status'] != 1){
		   echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />';
		   echo $web_stting['closed_reason'];
		   exit;	
		}else {
			$this->check_login();
			$this->mid = session('member_id');
			if (session('member_id')) {
				$member = M('Member')->where(array('member_id'=>$this->mid))->find();
				$this->assign('member',$member);
                //读取评论权限
                switch ($member['comment_status']) {
                    case 1:
                        if(IS_POST && ACTION_NAME=='rewrite'){
                            // showMessage('没有权限','','html','error');exit();
                        }elseif(IS_AJAX && ACTION_NAME=='comment') {
                            $res=array('status'=>false,'msg'=>'您已被管理员禁止评论，一周以后将自动恢复正常');
                            if($member['comment_lock_endtime']>NOW_TIME){
                                echo json_encode($res);exit();
                            }
                        }
                        break;
                    case 2:
                        if(IS_POST && ACTION_NAME=='rewrite'){
                            // showMessage('没有权限','','html','error');exit();
                        }elseif(IS_AJAX && ACTION_NAME=='comment') {
                            $res=array('status'=>false,'msg'=>'您已被管理员永久禁止评论');
                            echo json_encode($res);exit();
                        }
                        break;
                    default:
                        # code...
                        break;
                }
			}
			$this->assign('seo',seo());
		}
	}
	public function check_login()
	{
		if(session('member_id') || cookie('autologin'))
		{
			if (CONTROLLER_NAME == 'Login') {
				$this->redirect('Index/index');//已经登录直接跳转会员中心
				exit();
			}
		}else {
			if (CONTROLLER_NAME == 'Member') {
				$this->redirect('Login/index');//已经登录直接跳转会员中心
				exit();
			}
		}
	}
	public function seo_set($title='',$key='',$desc='')
	{	
	 	$web_seo = F('seo');
		if($web_seo === false){
			$seo_rs = M('Seo')->where('id=1')->find();	
			F('seo', $seo_rs); 	
			$web_seo = F('seo');		
		}
		$arr_seo = array();
		$arr_seo['title'] = $title ? $title.'-'.$web_seo['title'] : $web_seo['title'];
		$arr_seo['key'] = $key ? $key : $web_seo['keywords'];
		$arr_seo['desc'] = $desc ? $desc : $web_seo['description'];
		$this->assign('arr_seo',$arr_seo);
	}
	
}