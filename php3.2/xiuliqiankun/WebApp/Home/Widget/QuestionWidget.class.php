<?php
namespace Home\Widget;
use Home\Controller\BaseController;
class QuestionWidget extends BaseController{
	public function rightPlate($article_id){
		$this->commentMod = D('Comment');
		$limit = 10;
		$where = array('article_id'=>$article_id);
		$against_rank = $this->commentMod->where($where)->order('against_num desc')->limit($limit)->select();
		$agree_rank = $this->commentMod->where($where)->order('agree_num desc')->limit($limit)->select();
		$this->assign('against_rank',$against_rank);
		$this->assign('agree_rank',$agree_rank);
		$this->display("Widget:Question:rightPlate");
	}
}