<?php
namespace Home\Model;
use Think\Model\RelationModel;
class MemberModel extends RelationModel{
	protected $_link = array(
			'Comment' => array(
					'mapping_type'	=> self::MANY_TO_MANY,
					'class_name'	=> 'Comment',
					'mapping_name'	=> 'Comment',
					'foreign_key'	=> 'member_id',
					'relation_foreign_key' => 'follow_id',
					'relation_table'	=> 'mxd_follow'
			),
			'Article' => array(
					'mapping_type'	=> self::HAS_MANY,
					'class_name'	=> 'Article',
					'mapping_name'	=> 'Article',
					'foreign_key'	=> 'member_id',
					'mapping_order'	=> 'article_time desc'
			),
	);
}