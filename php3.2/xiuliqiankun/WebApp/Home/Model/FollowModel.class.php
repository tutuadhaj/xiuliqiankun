<?php
namespace Home\Model;
use Think\Model\RelationModel;
class FollowModel extends RelationModel{
	protected $_link = array(
			'Comment' => array(
					'mapping_type'	=> self::BELONGS_TO,
					'class_name'	=> 'Comment',
					'mapping_name'	=> 'Comment',
					'foreign_key'	=> 'follow_id',
			),
			'Member' => array(
					'mapping_type'	=> self::BELONGS_TO,
					'class_name'	=> 'Member',
					'mapping_name'	=> 'Member',
					'foreign_key'	=> 'member_id',
			),
	);
}