<?php
namespace Common\Org;
class QQConnect
{
    /**
     * 获取QQconnect Login 跳转到的地址值
     * @return array 返回包含code state
     *
     **/
    public function login($app_id, $callback, $scope)
    {
        $_SESSION['state'] = md5(uniqid(rand(), TRUE)); //CSRF protection
        $login_url = "https://graph.qq.com/oauth2.0/authorize?response_type=code&client_id="
            . $app_id . "&redirect_uri=" . urlencode($callback)
            . "&state=" . $_SESSION['state']
            . "&scope=" . urlencode($scope);
        //显示出登录地址
        header('Location:' . $login_url);
    }

    /**
     * 获取access_token值
     * @return array 返回包含access_token,过期时间的数组
     * */
    private function get_token($app_id, $app_key, $code, $callback, $state)
    {
        if ($state !== $_SESSION['state']) {
            return false;
            exit();
        }
        $url = "https://graph.qq.com/oauth2.0/token";
        $param = array(
            "grant_type" => "authorization_code",
            "client_id" => $app_id,
            "client_secret" => $app_key,
            "code" => $code,
            "state" => $state,
            "redirect_uri" => $callback
        );
        $response = $this->get_url($url, $param);
        if ($response == false) {
            return false;
        }
        $params = array();
        