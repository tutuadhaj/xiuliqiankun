<?php
namespace Common\Controller;
use Think\Controller;
use Common\Controller\RootController;

class HxController extends RootController{

	/**
	 * 环信-服务器端REST API
	 */
	private $client_id = 'YXA6crSXgG9VEeaeUxXpHBPv5w';
	private $client_secret = 'YXA6HSLCodRnBdMjzFfYVhkP7bardpY';
	private $org_name = 'p791600444';
	private $app_name = 'xiuliqiankun';
	private $url = 'https://a1.easemob.com/p791600444/xiuliqiankun/';

	
	//系统验证登录
   //  public function _initialize()
   //  {
   //      //验证token
   //      $key=$_REQUEST['key'];
   //      $this->tokenMod =new \Api\Model\Mb_user_tokenModel();
   //      $mb_user_token_info = $this->tokenMod->getMbUserTokenInfoByToken($key);
   //      $filter_login_validate=include_once(API_ITEM_PATH.'Conf/filter_login_validate_config.php'); 

   //      if(empty($mb_user_token_info) && in_array(ACTION_NAME, $filter_login_validate) ) {
   //          //令牌失效---code=-1
   //          $this->myApiPrint('-2');

   //      }else{
   //          $this->member_info =$mb_user_token_info ? $mb_user_token_info : array();
			// $member_info=M('member')->field('member_name,avatar,member_id,truename,signature,birthday,gender,coin,point,country,province,city,area,school')->where(array('member_id'=>$this->member_info['member_id']))->find();
   //          if($member_info){
   //              $this->member_info=array_merge($this->member_info,$member_info);
   //          }
   //      }
   //  }

	/**
	 * 开放注册模式
	 *
	 * @param $options['username'] 用户名        	
	 * @param $options['password'] 密码        	
	 */
	public function openRegister($options) {
		$url = $this->url . "users";
		$result = $this->postCurl ( $url, $options, $head = 0 );
		$result = json_decode($result, true);
		return $result;
	}
	
	/**
	 * 授权注册模式 || 批量注册
	 *
	 * @param $options['username'] 用户名        	
	 * @param $options['password'] 密码
	 *        	批量注册传二维数组
	 */
	public function accreditRegister($options) {
		$url = $this->url . "users";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, $options, $header );
		$result = json_decode($result, true);
		return $result;
	}
	
	/**
	 * 获取指定用户详情
	 *
	 * @param $username 用户名        	
	 */
	public function userDetails($username) {
		$url = $this->url . "users/" . $username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = 'GET' );
		$result = json_decode($result, true);
		return $result;
	}

	/*
     * 获取IM用户[批量]
     */
    public function userLists($limit)
    {
        $url = $this->url . "users?limit={$limit}";
// echo $url;exit;
        $access_token = $this->getToken ();
        $header [] = 'Authorization: Bearer ' . $access_token;
        $result = $this->postCurl ( $url, '', $header, $type = 'GET' );
        $result = json_decode($result, true);
        return $result;
    }
	
	/**
	 * 重置用户密码
	 *
	 * @param $options['username'] 用户名        	
	 * @param $options['password'] 密码        	
	 * @param $options['newpassword'] 新密码        	
	 */
	public function editPassword($options) {
		$url = $this->url . "users/" . $options ['username'] . "/password";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, $options, $header, $type = 'PUT');
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 删除用户
	 *
	 * @param $username 用户名        	
	 */
	public function deleteUser($username) {
		$url = $this->url . "users/" . $username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = 'DELETE' );
		$result = json_decode($result, true);
		return $result;
	}
	
	/**
	 * 批量删除用户
	 * 描述：删除某个app下指定数量的环信账号。上述url可一次删除300个用户,数值可以修改 建议这个数值在100-500之间，不要过大
	 *
	 * @param $limit="300" 默认为300条        	
	 * @param $ql 删除条件
	 *        	如ql=order+by+created+desc 按照创建时间来排序(降序)
	 */
	public function batchDeleteUser($limit = "300", $ql = '') {
		$url = $this->url . "users?limit=" . $limit;
		if (! empty ( $ql )) {
			$url = $this->url . "users?ql=" . $ql . "&limit=" . $limit;
		}
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = 'DELETE' );
		$result = json_decode($result, true);
		return $result;
	}
	
	/**
	 * 给一个用户添加一个好友
	 *
	 * @param
	 *        	$owner_username
	 * @param
	 *        	$friend_username
	 */
	public function addFriend($owner_username, $friend_username) {
		$url = $this->url . "users/" . $owner_username . "/contacts/users/" . $friend_username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 删除好友
	 *
	 * @param
	 *        	$owner_username
	 * @param
	 *        	$friend_username
	 */
	public function deleteFriend($owner_username, $friend_username) {
		$url = $this->url . "users/" . $owner_username . "/contacts/users/" . $friend_username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "DELETE" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 查看用户的好友
	 *
	 * @param
	 *        	$owner_username
	 */
	public function showFriend($owner_username) {
		$url = $this->url . "users/" . $owner_username . "/contacts/users/";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}

	// +----------------------------------------------------------------------
	// | 聊天室相关的方法
	// +----------------------------------------------------------------------
// chatrooms/{chatroom_id}
	/**
	 * 创建聊天室
	 *
	 * @param
	 *        	$username
	 */
	
	public function build_chatroom($option) {
		$url = $this->url . "chatrooms";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, $option, $header, $type = "POST" );
		$result = json_decode($result, true);
		return $result;
	}

	/**
	 * 删除聊天室
	 *
	 * @param
	 *        	$username
	 */
	
	public function delete_chatroom($classroom_id) {
		$url = $this->url . "chatrooms/".$classroom_id;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "DELETE" );
		$result = json_decode($result, true);
		return $result;
	}

	/**
	 * 聊天室详情
	 */
	public function detail_chatroom($classroom_id) {
		$url = $this->url . "chatrooms/".$classroom_id;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}

	/**
	 * 获取所有聊天室
	 *
	 * @param
	 *        	$username
	 */
	
	public function list_chatroom($pagenum,$pagesize) {
		$url = $this->url . "chatrooms?pagenum=".$pagenum."&pagesize=".$pagesize;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}

	// +----------------------------------------------------------------------
	// | 聊天相关的方法
	// +----------------------------------------------------------------------
	/**
	 * 查看用户是否在线
	 *
	 * @param
	 *        	$username
	 */
	public function isOnline($username) {
		$url = $this->url . "users/" . $username . "/status";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 发送消息群
	 *
	 * @param string $from_user
	 *        	发送方用户名
	 * @param array $username
	 *        	array('1','2')
	 * @param string $target_type
	 *        	默认为：users 描述：给一个或者多个用户(users)或者群组发送消息(chatgroups)
	 * @param string $content        	
	 * @param array $ext
	 *        	自定义参数
	 */
	function yy_hxSend($from_user = "admin", $username, $content, $target_type = "users", $ext) {
		$option ['target_type'] = $target_type;
		$option ['target'] = $username;
		$params ['type'] = "txt";
		$params ['msg'] = $content;
		$option ['msg'] = $params;
		$option ['from'] = $from_user;
		$option ['ext'] = $ext;
		$url = $this->url . "messages";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, $option, $header );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 获取app中所有的群组
	 */
	public function chatGroups() {
		$url = $this->url . "chatgroups";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 创建群组
	 *
	 * @param $option['groupname'] //群组名称,
	 *        	此属性为必须的
	 * @param $option['desc'] //群组描述,
	 *        	此属性为必须的
	 * @param $option['public'] //是否是公开群,
	 *        	此属性为必须的 true or false
	 * @param $option['approval'] //加入公开群是否需要批准,
	 *        	没有这个属性的话默认是true, 此属性为可选的
	 * @param $option['owner'] //群组的管理员,
	 *        	此属性为必须的
	 * @param $option['members'] //群组成员,此属性为可选的        	
	 */
	public function createGroups($option) {
		$url = $this->url . "chatgroups";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, $option, $header );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 获取群组详情
	 *
	 * @param
	 *        	$group_id
	 */
	public function chatGroupsDetails($group_id) {
		$url = $this->url . "chatgroups/" . $group_id;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 删除群组
	 *
	 * @param
	 *        	$group_id
	 */
	public function deleteGroups($group_id) {
		$url = $this->url . "chatgroups/" . $group_id;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "DELETE" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 获取群组成员
	 *
	 * @param
	 *        	$group_id
	 */
	public function groupsUser($group_id) {
		$url = $this->url . "chatgroups/" . $group_id . "/users";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 群组添加成员
	 *
	 * @param
	 *        	$group_id
	 * @param
	 *        	$username
	 */
	public function addGroupsUser($group_id, $username) {
		$url = $this->url . "chatgroups/" . $group_id . "/users/" . $username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "POST" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 群组删除成员
	 *
	 * @param
	 *        	$group_id
	 * @param
	 *        	$username
	 */
	public function delGroupsUser($group_id, $username) {
		$url = $this->url . "chatgroups/" . $group_id . "/users/" . $username;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "DELETE" );
		$result = json_decode($result, true);
		return $result;
	}
	/**
	 * 聊天消息记录
	 *
	 * @param $ql 查询条件如：$ql
	 *        	= "select+*+where+from='" . $uid . "'+or+to='". $uid ."'+order+by+timestamp+desc&limit=" . $limit . $cursor;
	 *        	默认为order by timestamp desc
	 * @param $cursor 分页参数
	 *        	默认为空
	 * @param $limit 条数
	 *        	默认20
	 */
	public function chatRecord($ql = '', $cursor = '', $limit = 20) {
		$ql = ! empty ( $ql ) ? "ql=" . $ql : "order+by+timestamp+desc";
		$cursor = ! empty ( $cursor ) ? "&cursor=" . $cursor : '';
		$url = $this->url . "chatmessages?" . $ql . "&limit=" . $limit . $cursor;
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = "GET " );
		$result = json_decode($result, true);
		return $result;
	}

	/**
	 * 获取指定用户未读消息
	 *
	 * @param $username 用户名        	
	 */
	public function userWeidu($owner_username) {
		$url = $this->url . "users/" . $owner_username . "/offline_msg_count";
		$access_token = $this->getToken ();
		$header [] = 'Authorization: Bearer ' . $access_token;
		$result = $this->postCurl ( $url, '', $header, $type = 'GET' );
		$result = json_decode($result, true);
		return $result;
	}


    /**
     * 获取Token
     */
    public function getToken() {
        $option['grant_type'] = "client_credentials";
        $option['client_id'] = $this->client_id;
        $option['client_secret'] = $this->client_secret;
        $url = $this->url . "token";
        $fp = @fopen( APP_PATH."easemob.txt", 'r' );
        if($fp) {

            $arr = unserialize( fgets( $fp ) );
            if($arr['expires_in'] < time()) {
                $result = json_decode($this->postCurl( $url, $option), true);
                $result['expires_in'] = $result['expires_in'] + time();
                file_put_contents(APP_PATH."easemob.txt", serialize($result));
                return $result['access_token'];
                fclose( $fp );
                exit();
            }
            return $arr['access_token'];
            fclose( $fp );
            exit();
        }
        $result = json_decode($this->postCurl( $url, $option), true);
        $result['expires_in'] = $result['expires_in'] + time();
        file_put_contents(APP_PATH."easemob.txt", serialize($result));
        return $result['access_token'];
        fclose( $fp );
    }

	/**
	 * CURL Post
	 */
	// private function postCurl($url, $option, $header = 0, $type = 'POST') {
	// 	$curl = curl_init (); // 启动一个CURL会话
	// 	curl_setopt ( $curl, CURLOPT_URL, $url ); // 要访问的地址
	// 	curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, FALSE ); // 对认证证书来源的检查
	// 	curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, FALSE ); // 从证书中检查SSL加密算法是否存在
	// 	curl_setopt ( $curl, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0)' ); // 模拟用户使用的浏览器
	// 	if (! empty ( $option )) {
	// 		$options = json_encode ( $option );
	// 		curl_setopt ( $curl, CURLOPT_POSTFIELDS, $options ); // Post提交的数据包
	// 	}
	// 	curl_setopt ( $curl, CURLOPT_TIMEOUT, 30 ); // 设置超时限制防止死循环
	// 	curl_setopt ( $curl, CURLOPT_HTTPHEADER, $header ); // 设置HTTP头
	// 	curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 ); // 获取的信息以文件流的形式返回
	// 	curl_setopt ( $curl, CURLOPT_CUSTOMREQUEST, $type );
	// 	$result = curl_exec ( $curl ); // 执行操作
	// 	//$res = object_array ( json_decode ( $result ) );
	// 	//$res ['status'] = curl_getinfo ( $curl, CURLINFO_HTTP_CODE );
	// 	//pre ( $res );
	// 	curl_close ( $curl ); // 关闭CURL会话
	// 	return $result;
	// }


	private function postCurl($url, $data, $header = 0, $method = "POST")
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        if ($header) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if ($data) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $ret = curl_exec($ch);
        return $ret;
    }
}