<?php
/**
 * Api公共类(验证登录模式：需验证用户接口继承)
 * Interface引用api模式，没有display等view的渲染和页面模版输出
 */
namespace Common\Controller;
use Think\Controller;
use Common\Controller\RootController;

class ApiController extends RootController
{
    //供基类回调初始化
    public function _initialize()
    {

        // if(!empty($_REQUEST['key'])){
        //     $key=$_REQUEST['key'];
        //     $this->tokenMod =new \Api\Model\Mb_user_tokenModel();
        //     $mb_user_token_info = $this->tokenMod->getMbUserTokenInfoByToken($key);
        //     $this->member_info =$mb_user_token_info ? $mb_user_token_info : array();
        //     if(isset($this->member_info['member_id'])){
        //         $member_info=M('member')->field('member_name,avatar,member_id,truename,signature,birthday,gender,coin,point,country,province,city,area,school')->where(array('member_id'=>$this->member_info['member_id']))->find();
        //         $this->member_info=array_merge($this->member_info,$member_info);
        //         //考虑用户被禁用或是被删除的情况
        //         //code...

        //     }else{

        //         $this->myApiPrint('-2','');
        //     }
        // }else{
        //     $controller_name= strtolower(ACTION_NAME);
        //     //登录和注册不需要传key
        //     $fitel_not_login=array('login','register');
        //     if (!in_array($controller_name, $fitel_not_login)) {
        //         $this->myApiPrint('-2','',array('msg'=>'key空'.$controller_name));
        //     }
            
        // }

        //
        
        //验证token
        $key=$_REQUEST['key'] ? $_REQUEST['key'] : '';

        $this->tokenMod =new \Api\Model\Mb_user_tokenModel();
        $mb_user_token_info = $this->tokenMod->getMbUserTokenInfoByToken($key);
        // $this->myApiPrint(-3,'',array('msg'=>$_REQUEST['key'].'***'));
        $filter_login_validate=include_once(API_ITEM_PATH.'Conf/filter_login_validate_config.php'); 

        if(empty($mb_user_token_info) && in_array(ACTION_NAME, $filter_login_validate) ) {  
            //令牌失效---code=-1
            $this->myApiPrint('-2','');
        }else{
            $this->member_info =$mb_user_token_info ? $mb_user_token_info : array();
            $member_info=M('member')->field($this->select_fields_column['member'])->where(array('member_id'=>$this->member_info['member_id']))->find();
            if($member_info){
                //考虑用户被禁用或是被删除的情况
                if(!$member_info['status']){
                    $this->myApiPrint(-2,'',array('msg'=>'有违规操作，帐号已被管理员冻结'));
                }
                $this->member_info=array_merge($this->member_info,$member_info);
            }

            $this->comMod =new \Api\Model\CommonModel();

            //系统每天根据用户等级赠送等量推荐票
            $exist_tools=$this->comMod->detail('member_tools',array('member_id'=>$this->member_info['member_id'],'goods_id'=>3));//是否有推荐票
            
            $member_level_info=getMemberLevel($this->member_info['point']); //用户等级信息
            
            $member_other_info=$this->comMod->detail('member',array('member_id'=>$this->member_info['member_id']),'latest_given_tickets_time'); //最后获赠系统推荐票的时间

            $today=date('Y-m-d', NOW_TIME);
            $latest_given_tickets_time=$member_other_info['latest_given_tickets_time'] ? $member_other_info['latest_given_tickets_time'] : '';
            // $given_time=date('Y-m-d', $latest_given_tickets_time);
            $count=$member_level_info['level'];

            //在0等级以上才能获得赠送推荐票
            if($count>0 && $today!=$latest_given_tickets_time){
                
                if(empty($exist_tools)){
                    $data['member_id']=$this->member_info['member_id'];
                    $data['goods_id']=3;
                    $data['name']='推荐票';
                    $data['logo']='';
                    $data['amount']=$count;
                    $result=$this->comMod->addData('member_tools',$data);
                }else{
                    //清空推荐票
                    $this->comMod->saveData('member_tools',array('goods_id'=>3,'member_id'=>$this->member_info['member_id']),array('amount'=>0));
                    //赠送推荐票
                    $result=$this->comMod->selfUpdateField('member_tools','amount',array('goods_id'=>3,'member_id'=>$this->member_info['member_id']),$operate="+",$count);
                }
                if($result){
                    $this->comMod->saveData('member',array('member_id'=>$this->member_info['member_id']),array('latest_given_tickets_time'=>$today));
                }

            }

            //结束
            

            
            // $member_info=$this->comMod->table(C('DB_PREFIX').'member')->field('member_name,avatar,member_id,truename,signature,birthday,gender,country,province,city,area,school')->where(array('member_id'=>$mb_user_token_info['member_id']))->find();
                
            //     $this->member_info=$this->filter_member_info($member_info,$$mb_user_token_info['key']);
        }
    }
}