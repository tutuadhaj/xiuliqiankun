<?php
/**
 * Api公共类(游客模式：不需验证用户接口继承)
 * Interface引用api模式，没有display等view的渲染和页面模版输出
 */
namespace Common\Controller;
use Think\Controller;
use Common\Controller\RootController;

class GeneralController extends RootController
{
    public function __construct(){
        parent::__construct();
    }
    

    //公共方法（公开）
    // public function FunctionName($value='')
    // {
    //     # code...
    // }
}