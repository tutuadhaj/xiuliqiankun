<?php
/**
 * 公共配置
 * @package    config
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
return array(

    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------
     *  数据库配置
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------------*/

    'DB_TYPE'   => 'mysql',             // 数据库类型
    'DB_HOST'   => '127.0.0.1',         // 数据库服务器地址
    'DB_NAME'   => 'xlqk_db',           // 数据库名
    'DB_USER'   => 'root',              // 登录用户名
    // 'DB_PWD'    => 'xlqk1234567ujm',              // 登录密码1d404a6445
    'DB_PWD'    => 'root',
    'DB_PORT'   => '3306',              // 端口
    'DB_PREFIX' => 'mxd_',               // 数据库表前缀
    /* 数据库备份*/
    'DB_BACKUP' =>'./Backup/',
    /*---------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 基础配置
     * ------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
    'WKY_KEY' => 'WKY_5',                    //安全密钥（加密key）
    'SiteUrl' => 'http://xiuliqiankun.cn/xiuliqiankun', //站点域名 末尾不加斜杠(控制后台首页链接)

    /* URL配置 */
    'URL_CASE_INSENSITIVE' => false,    //默认false 表示URL区分大小写 true则表示不区分大小写
    'URL_PARAMS_BIND'      => false,    // URL变量绑定到操作方法作为参数 关闭
    'URL_MODEL'            => 2,        //URL模式 重写模式（通过web服务器重写规则实现伪静态）

    /* 模块配置 */
    'MODULE_ALLOW_LIST'    =>    array('Page','Toadmin','Api'),  //模块列表
    'DEFAULT_MODULE'       =>    'Toadmin',                   //默认模块

    /* 支付宝 */
    'alipay_config' => array(
        'partner' => '',	//这里是你在成功申请支付宝接口后获取到的PID;
        'key' => '',		//这里是你在成功申请支付宝接口后获取到的Key;
        'sign_type' => strtoupper('MD5'),
        'input_charset' => strtolower('utf-8'),
        'cacert'=> getcwd().'\\cacert.pem',
        'transport'=> 'http',
    ),

    /* 其他业务配置值 */
    'ARTICLE_MODEL_CONFIG'=>array(
        'viewpoint_limit' => 30,
        'viewpoint_free_word_count' => 50,   //订阅模式论点前50字免费观看
        'viewpoint_word_count_reward_limit'=>10,  //论点字数奖励最多收费10金币，到达10金币以后就不需要消耗金币
        'viewpoint_onecoin_increase_word_count'=>10, //论点字数奖励，每个多看10个字要多消费1金币
        'viewpoint_replies_limit' => 10,   //论点回复查看金币消耗上限10金币，到达10金币以后就不需要消耗金币
        'viewpoint_replies_increase_coin'=>1,  //论点回复数每多一个查看时需要多消耗1金币
        'viewpoint_worth_increase_coin'=>1000,   //论点值点击数超过1000个，查看时需要再多支付1个金币，最多支付10个
        'pack_article_wordth'=>0.5,  //打包：论题的每个论点和评论的价值之和的50%  （初步理解所有论点值数和评论回复数（不论支持还是反对，取其中最高值） ）
    ),
);