<?php
namespace Api\Common;


class MyUpload
 {
  /**声明**/
  var $upfile_type,$upfile_size,$upfile_name,$upfile;
  var $d_alt,$extention_list,$tmp,$arri;
  var $datetime,$date;
  var $filestr,$size,$ext,$check;
  var $flash_directory,$extention,$file_path,$base_directory;
  var $url; //文件上传成功后跳转路径;
  var $al_ph_type=array('image/jpg','image/jpeg','image/png','image/pjpeg','image/gif','image/bmp','image/x-png');
  function MyUpload()
  {
   /**构造函数**/
   $this->set_url("index.php");          //初始化上传成功后跳转路径;
   $this->set_extention();             //初始化扩展名列表;
   $this->set_size(5000000);              //初始化上传文件KB限制;
   $this->set_date();               //设置目录名称;
   $this->set_datetime();             //设置文件名称前缀;
   $this->set_base_directory("attachmentFile");  //初始化文件上传根目录名，可修改！;
  }
 
  /**文件类型**/
  function set_file_type($upfile_type)
  { 
   $this->upfile_type = $upfile_type;       //取得文件类型;
  }
 
  /**获得文件名**/
  function set_file_name($upfile_name)
  {
   $this->upfile_name = $upfile_name;       //取得文件名称;
  }
 
  /**获得文件**/
  function set_upfile($upfile)
  {
   $this->upfile = $upfile;            //取得文件在服务端储存的临时文件名;
  }
   
  /**获得文件大小**/
  function set_file_size($upfile_size)
  {
   $this->upfile_size = $upfile_size;       //取得文件尺寸;
  }
 
  /**设置文件上传成功后跳转路径**/
  function set_url($url)
  {
   $this->url = $url;               //设置成功上传文件后的跳转路径;
  }
 
  /**获得文件扩展名**/
  function get_extention()
  {
    $this->extention = preg_replace('/.*\.(.*[^\.].*)*/iU','\\1',$this->upfile_name); //取得文件扩展名;
    //print_r($this->extention) ."取得扩展名"."<br>";
  }
     
  /**设置文件名称**/
  function set_datetime()
  {
   $this->datetime = date("YmdHis",  time()+8*3600);        //按时间生成文件名;
    sleep(1);

  }
 
  /**设置目录名称**/
  function set_date()
  {
   $this->date = date("Y-m-d");          //按日期生成目录名称;
  }
 
  /**初始化允许上传文件类型**/
  function set_extention()
  {
  
   $this->extention_list = "|gif|jpg|jpeg|bmp|png"; //默认允许上传的扩展名称;
//   echo "set_exetention".$this->extention_list."<br>";
  } 
 
  /**设置最大上传KB限制**/
  function set_size($size)
  {
   $this->size = $size;              //设置最大允许上传的文件大小;
  }
 
  /**初始化文件存储根目录**/
  function set_base_directory($directory)
  {
   $this->base_directory = $directory; //生成文件存储根目录;
  }
 
  /**初始化文件存储子目录**/
  function set_flash_directory()
  {
   $this->flash_directory = $this->base_directory."/".$this->date; //生成文件存储子目录;
  }
 
  /**错误处理**/
  function showerror($errstr="未知错误！"){
   echo "<script language=javascript>alert('$errstr');location='javascript:history.go(-1);';</script>";
   exit();
  }
 
  /**跳转**/
  function go_to($str,$url)
  {
   echo "<script language='javascript'>alert('$str');location='$url';</script>";
   exit();
  }

  /**如果根目录没有创建则创建文件存储目录**/
  function mk_base_dir()
  {
   if (! file_exists($this->base_directory)){   //检测根目录是否存在;
    mkdir($this->base_directory,0777);     //不存在则创建;
   }
  }

  /**如果子目录没有创建则创建文件存储目录**/
  function mk_dir()
  {
   if (! file_exists($this->flash_directory)){   //检测子目录是否存在;
    mkdir($this->flash_directory,0777);     //不存在则创建;
   }
  } 
 
  /**以数组的形式获得分解后的允许上传的文件类型**/
  function get_compare_extention()
  {
   $this->ext = explode("|",$this->extention_list);//以"|"来分解默认扩展名;
//   print_r($this->ext)."<br>";

  }
 
  /**检测扩展名是否违规**/
  function check_extention()
  {
   if(!in_array($this->extension,$this->ext)){
       $this->showerror("正确的扩展名必须为".$this->extention_list."其中的一种！");
    } 
  }
 
  
  /**检测文件大小是否超标**/
  function check_size()
  {
   if($this->upfile_size > round($this->size*1024))     //文件的大小是否超过了默认的尺寸;
   {
    $this->showerror("上传附件".$this->upfile_name."不得超过".$this->size."KB"); //超过则警告;
   }
  }

  /**文件完整访问路径**/
  function set_file_path()
  {
      
   $this->file_path = $this->flash_directory."/".$this->datetime.".".$this->extention; //生成文件完整访问路径;
   
  }
 
  /**上传文件**/
//  function copy_file()
//  {
//   if(copy($this->upfile,$this->file_path)){        //上传文件;
////    print $this->go_to($this->url,$this->url."?path=".$this->file_path);  //上传成功;
//   echo "<script> alert($this->$file_path);</script>";
//   }else {
//    print $this->showerror("意外错误，请重试！");     //上传失败;
//   }
//  }
 
  /**完成保存**/
  function save()
  {
   $this->set_flash_directory();  //初始化文件上传子目录名;
   $this->get_extention();     //获得文件扩展名;
   $this->get_compare_extention(); //以"|"来分解默认扩展名;
   $this->check_extention();    //检测文件扩展名是否违规;
   $this->check_size();      //检测文件大小是否超限;  
   $this->mk_base_dir();      //如果根目录不存在则创建；
   $this->mk_dir();        //如果子目录不存在则创建;
   $this->set_datetime();   //此处为设置文件名的时间命名，需要延迟防止重名覆盖只能上传一个文件
   $this->set_file_path();     //生成文件完整访问路径;
//   $this->copy_file();        //上传文件;
  }              # 保存文件
            
//生成缩略图
function produce_thumb($src_file , $new_width , $new_height,$path) {

////$data[0]是宽，$data[1]是高1024*768；
$data = @getimagesize($src_file); 
//Load image
switch($data[2]) {
case 2 :
    $src_img=imagecreatefromjpeg($src_file);
    $type = ".jpg";
break;
case 3:
    $src_img=imagecreatefrompng($src_file);
    $type = ".png";
break;
case 1 :
    $src_img=imagecreatefromgif($src_file);
    $type=".gif";
break;
}
$dst_file =str_replace(strrchr($src_file, "."),"",$src_file)."_thumb".$type;
$w=imagesx($src_img);
$h=imagesy($src_img);
$ratio_w=1.0 * $new_width / $w;
$ratio_h=1.0 * $new_height / $h;
$ratio=1.0;
// 生成的图像的高宽比原来的都小，或都大 ，原则是 取大比例放大，取大比例缩小（缩小的比例就比较小了）
if( ($ratio_w < 1 && $ratio_h < 1) || ($ratio_w > 1 && $ratio_h > 1)) {
if($ratio_w < $ratio_h) {
$ratio = $ratio_h ; // 情况一，宽度的比例比高度方向的小，按照高度的比例标准来裁剪或放大
}else {
$ratio = $ratio_w ;
}
// 定义一个中间的临时图像，该图像的宽高比 正好满足目标要求
$inter_w=(int)($new_width / $ratio);
$inter_h=(int) ($new_height / $ratio);
$inter_img=imagecreatetruecolor($inter_w , $inter_h);
imagecopy($inter_img, $src_img, 0,0,0,0,$inter_w,$inter_h);
// 生成一个以最大边长度为大小的是目标图像$ratio比例的临时图像
// 定义一个新的图像
$new_img=imagecreatetruecolor($new_width,$new_height);
imagecopyresampled($new_img,$inter_img,0,0,0,0,$new_width,$new_height,$inter_w,$inter_h);
switch($data[2]) {
case 2 :
imagejpeg($new_img, $dst_file,100); // 存储图像
break;
case 3 :
imagepng($new_img,$dst_file,9);
break;
case 1 :
imagegif($new_img,$dst_file,100);
break;
default:
break;
}
} // end if 1
// 2 目标图像 的一个边大于原图，一个边小于原图 ，先放大平普图像，然后裁剪
// =if( ($ratio_w < 1 && $ratio_h > 1) || ($ratio_w >1 && $ratio_h <1) )
else{
$ratio=$ratio_h>$ratio_w? $ratio_h : $ratio_w; //取比例大的那个值
// 定义一个中间的大图像，该图像的高或宽和目标图像相等，然后对原图放大
$inter_w=(int)($w * $ratio);
$inter_h=(int) ($h * $ratio);
$inter_img=imagecreatetruecolor($inter_w , $inter_h);
//将原图缩放比例后裁剪
imagecopyresampled($inter_img,$src_img,0,0,0,0,$inter_w,$inter_h,$w,$h);
// 定义一个新的图像
$new_img=imagecreatetruecolor($new_width,$new_height);
imagecopy($new_img, $inter_img, 0,0,0,0,$new_width,$new_height);
switch($data[2]) {
case 2 :
imagejpeg($new_img, $dst_file,100); // 存储图像
break;
case 3 :
imagepng($new_img,$dst_file,100);
break;
case 1 :
imagegif($new_img,$dst_file,100);
break;
default:
break;
}
}// if3
return $dst_file;
}
 }

?>

