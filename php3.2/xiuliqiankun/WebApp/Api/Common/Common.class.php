<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-10-10
 * Time: 下午2:42
 */

namespace Api\Common;


class Common
{
    //获取星期一的时间戳
    function getmonsun(){
        $curtime=time();
        $curweekday = date('w');
         //为0是 就是 星期七
        $curweekday = $curweekday?$curweekday:7;
        $curmon = $curtime - ($curweekday-1)*86400;
        // $cursun = $curtime + (7 - $curweekday)*86400;
        // $cur['mon'] = $curmon;
        // $cur['sun'] = $cursun;

        return $curmon;
    }
    
    //本周1
    public static function this_monday(){ 
            $timestamp = time(); 
            $monday_date = date('Y-m-d', $timestamp-86400*date('w',$timestamp)+(date('w',$timestamp)>0?86400:-6*86400)); 
                $res = strtotime($monday_date); 
        return $res; 
       
    } 

    //计算连续签到天数
    public static function continue_days(array $date){
        $continue_day=1;
        foreach($date as $key=>$val){
            $base=($date[$key-1])?$date[$key-1]:$val;
            if($date[$key]==date("Y-m-d",strtotime("-1 day",strtotime($base)))){
                $continue_day++;
            }
        }
        return $continue_day;
    }

    /*
    * 获取相对时间
    *
    * */
    /**
     * 友好的时间显示
     *
     * @param int    $sTime 待显示的时间
     * @param string $type  类型. normal | mohu | full | ymd | other
     * @param string $alt   已失效
     * @return string
     */
    public static function friendlyDate($sTime,$type = 'normal',$alt = 'false') {
        if (!$sTime)
            return '';
        //sTime=源时间，cTime=当前时间，dTime=时间差
        $cTime      =   time();
        $dTime      =   $cTime - $sTime;
        $dDay       =   intval(date("z",$cTime)) - intval(date("z",$sTime));
        //$dDay     =   intval($dTime/3600/24);
        $dYear      =   intval(date("Y",$cTime)) - intval(date("Y",$sTime));
        //normal：n秒前，n分钟前，n小时前，日期
        if($type=='normal'){
            if( $dTime < 60 ){
                if($dTime < 10){
                    return '刚刚';    //by yangjs
                }else{
                    return intval(floor($dTime / 10) * 10)."秒前";
                }
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
                //今天的数据.年份相同.日期相同.
            }elseif( $dYear==0 && $dDay == 0  ){
                //return intval($dTime/3600)."小时前";
                return '今天'.date('H:i',$sTime);
            }elseif($dYear==0){
                return date("m月d日 H:i",$sTime);
            }else{
                return date("Y-m-d H:i",$sTime);
            }
        }elseif($type=='mohu'){
            if( $dTime < 60 ){
                return $dTime."秒前";
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
            }elseif( $dTime >= 3600 && $dDay == 0  ){
                return intval($dTime/3600)."小时前";
            }elseif( $dDay > 0 && $dDay<=7 ){
                return intval($dDay)."天前";
            }elseif( $dDay > 7 &&  $dDay <= 30 ){
                return intval($dDay/7) . '周前';
            }elseif( $dDay > 30 ){
                return intval($dDay/30) . '个月前';
            }
            //full: Y-m-d , H:i:s
        }elseif($type=='full'){
            return date("Y-m-d , H:i:s",$sTime);
        }elseif($type=='ymd'){
            return date("Y-m-d",$sTime);
        }else{
            if( $dTime < 60 ){
                return $dTime."秒前";
            }elseif( $dTime < 3600 ){
                return intval($dTime/60)."分钟前";
            }elseif( $dTime >= 3600 && $dDay == 0  ){
                return intval($dTime/3600)."小时前";
            }elseif($dYear==0){
                return date("Y-m-d H:i:s",$sTime);
            }else{
                return date("Y-m-d H:i:s",$sTime);
            }
        }
    }

    /*
    * 将数字转为万分位
    *
    * */
    public static function scanNumFormat($num){
        return $num>=10000?sprintf("%.1f", $num/10000).'万':$num;
    }

    /*
 * 检查请求有效性与参数
 * @return 混合模型
 * */
    public static function checkRequestTime($time){
        if(($time) > 0){
            //300秒/5分钟的有效请求
            $v =time()-$time;
            if($v<300 && $v>0) return true;
            else return false;
        }else return false;
    }

    /**
     * 生成定长22位的订单号码
     *
     */
    public static function MyOrderNo22(){
        $code  =date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        $code .= randCodeM(22-strlen($code),1);
        return $code;
    }

    /**
     * 26位
     * 生成环信用户Id（拼音和数字，不能是中文，邮箱，且不能包含#号等特殊字符）
     * ord() 函数返回字符串的首个字符的 ASCII 值。
     */
    public static function setHuanxinId(){
        $code  ="xlqk".date('Ymd').substr(implode(NULL, array_map('ord', str_split(substr(uniqid(), 7, 13), 1))), 0, 8);
        $code .= randCodeM(30-strlen($code),1);
        return $code;
    }

    /**
     * 浮点数舍去指定位数小数点部分。全舍不入
     * @param $n float浮点值
     * @param $len 截取长度字数
     * @return string 截取后的值
     */
    public static function sub_float($n,$len)
    {
        stripos($n, '.') && $n= (float)substr($n,0,stripos($n, '.')+$len+1);
        return $n;
    }

    /**
     * 公共错误返回
     * @param $msg 需要打印的错误信息
     * @param $code 默认打印200信息
     * @param $extend_data 分页扩展数据等
     */
    public static function myApiPrint($code='',$data='',$extend_data=array()){

        $code=$code ? $code : 200;

        // $result = array(
        //     'code' => $code,
        //     'msg' => $GLOBALS['MsgCodeData'][$code] ? $GLOBALS['MsgCodeData'][$code] : ''
        // );
        $result['status']=($code>=100 && $code<300) ? true : false;
        $result['code']=$code;
        $result['msg']=$GLOBALS['MsgCodeData'][$code] ? $GLOBALS['MsgCodeData'][$code] : '';
        $extend_data=$extend_data ? $extend_data : array();
        $result = array_merge($result, $extend_data);
        $result['data']=$data ? $data : array();
        if($data && !is_array($data[0])){
            $result['data']=array($data);
        }
        // 返回JSON数据格式到客户端 包含状态信息
        header('Content-Type:application/json; charset=utf-8');
        exit(json_encode($result));exit;
    }

    /*
    *------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    * 支付宝相关
    *-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
   */

    //安全性接口，验签操作
    public static function mySign($para_temp){

        //除去待签名参数数组中的空值和签名参数
        $para_filter = self::myParaFilter($para_temp);

        //对待签名参数数组排序
        $para_sort = self::myArgSort($para_filter);

        //把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
        $prestr = self::myParaLinkstring($para_sort);
        return $prestr;
    }

    //安全性接口，验签操作
    public static function myParaFilter($para) {
        $para_filter = array();
        while (list ($key, $val) = each ($para)) {
            if($key === "sign" || $key === "sign_pass" || $val === "")continue;
            else	$para_filter[$key] = $para[$key];
        }
        return $para_filter;
    }

    /**
     * 把数组所有元素，按照“参数=参数值”的模式用“&”字符拼接成字符串
     * @param $para 需要拼接的数组
     * return 拼接完成以后的字符串
     */
    static function myParaLinkstring($para) {
        $arg  = "";
        while (list ($key, $val) = each ($para)) {
            $arg.=$key."=".$val."&";
        }
        //去掉最后一个&字符
        $arg = substr($arg,0,count($arg)-2);

        //如果存在转义字符，那么去掉转义
        if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}

        return $arg;
    }

    /**
     * 对数组排序
     * @param $para 排序前的数组
     * return 排序后的数组
     */
    public static function myArgSort($para) {
        ksort($para);
        reset($para);
        return $para;
    }



    /**
     * 图片二进制存入物理地址
     * @param $data base64加密图片流字符串
     * @param $uid  用户ID
     * @param $type 图片类型，默认gif
     * @return 数组，booler 与 返回值
     */
    public static function myStream2Img($data,$uid,$type='.gif',$name=''){
        $name = (empty($name))?time().$type:$name.$type;
        $dir = '/Public/Upload/'.$uid.'/';
        $s2i = new \Common\Org\Stream2Image($name,$dir);// 实例化上传类
        $re = $s2i->stream2Image(base64_decode($data));
        //@file_put_contents('a1_dir.log',$dir.$name.PHP_EOL,FILE_APPEND);
        if(true === $re){@chmod('./'.$dir,0777);@chmod('./'.$dir.$name,0777); return $dir.$name;}
        else {
            $result = array(
                'code' => 300,
                'msg' => $s2i->print_errInfo,
                'result' => ''
            );
            self::myApiPrint('',$result);
        }
    }

} 