<?php
/**
 * 学生社团模型
 *
 * 
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

namespace Api\Model;
use Api\Common\Common;
use Think\Model;


class CommunityModel extends CommonModel{
    
	//社团详情信息
    function getClubDetail($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        $data=$this->table($this->table_prefix.'club')->where($condition)->field($fields)->find();
        return $this->clubHandle($data,'detail');
    }

    //社团成员列表
    // function getClubMembers($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
    //     $list=M('club_member_relation')->alias('e')->join('LEFT JOIN mxd_member as m'.' ON e.member_id = m.member_id' )->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    //     return $this->clubMemberHandle($list);
    // }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------
    *数据处理
    * ------------------------------------------------------------------------------------------------------------------------------------------------------*/

    //学生社团数据处理
    public function clubHandle($data,$style='list'){
        if(!empty($data)){
                    
            $member_info=$this->getMemberInfoByID($data['uid'],'member_name,avatar,member_id','school');
            $type_info=$this->getClubTypeInfoByID($data['type'],'text');
            
            $data['logo']=$data['logo'] ? PicPath.$data['logo'] : '';
            $data['time']=date("m-d H:i",$data['time']);
            $data['type']=$type_info['text'];
            $data['history']=!empty($data['history']) ? $data['history'] : '';
            $data['present']=!empty($data['present']) ? $data['present'] : '';
            $data['future']=!empty($data['future']) ? $data['future'] : '';
            $data['level']=!empty($data['level']) ? $data['level'] : '';
            if($style=='list'){
                unset($data['level']);
                unset($data['type']);
                unset($data['time']);
                unset($data['history']);
                unset($data['present']);
                unset($data['future']);
            }
            unset($data['join']);
            unset($data['count']);
            unset($data['status']);
            unset($data['show']);
        }

        return $data;
    }

    //社团成员数据处理
    public function clubMemberHandle($data){
        $data_count=count($data);
        $result=array();
        for($i=0;$i<$data_count;$i++){
            //数据处理
            $result[$i]['icon']= $data[$i]['avatar'] ? PicPath.$data[$i]['avatar'] : '';
            $result[$i]['uid']= $data[$i]['member_id'] ? $data[$i]['member_id'] : 0;
            $result[$i]['name']= $data[$i]['member_name'] ? $data[$i]['member_name'] : '';
        }
        return $result;
    }
    
    //社团申请数据处理
    public function clubApplyHandle($data,$style='list'){
        $result=array();
        if(!empty($data)){
            $data_uid = $data['uid'] ? $data['uid'] : 0;      
            $member_info=$this->getMemberInfoByID($data_uid,'member_name,avatar,member_id','school');
            $school_info=unserialize($member_info['school']);
            $result['id']=$data['id'];
            $result['uid']=$data_uid; //申请人id
            $result['cid']=$data['cid']; //申请同乡会的id
            $result['school']=$school_info['name']; //申请人学校信息
            $result['member_name']=$member_info['member_name'] ? $member_info['member_name'] : '';  //申请人名称
            $result['icon']=$member_info['avatar'] ? PicPath.$member_info['avatar'] : ''; //申请人头像
            $result['club']=$data['club_name']; //申请同乡会的名称
            $result['time']=date("m-d H:i",$data['add_time']);
            if($style!='list'){
                $result['img']=$data['img'] ? PicPath.$data['img'] : '';
                $result['video']=$data['video'] ? PicPath.$data['video'] : '';
                $result['sound']=$data['sound'] ? PicPath.$data['sound'] : '';
            }
        }

        return $result;
    }

}
