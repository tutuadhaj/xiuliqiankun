<?php
/**
 * 公用模型
 *
 * 
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

namespace Api\Model;
use Api\Common\Common;
use Think\Model;
use Think\Page;


class CommonModel extends Model{
    public $table_prefix;
    public function __construct(){
        $this->table_prefix=C('DB_PREFIX');
        parent::__construct('sectiontopic'); //默认初始化表
    }
//
//    //新增
//    public function add($param){
//        return $this->add($param);
//    }
//
//
//    // 更新
//    public function save($condition){
//        return $this->where($condition)->delete();
//    }
//
//    // 详情
//    public function getTopicInfo($condition) {
//        return $this->where($condition)->find();
//    }



    /*
     *------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 数据处理方法
     *-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    */



    //榜单类似数据处理
    public function bangdanHandle($data,$style='list'){
        $result=array();
        if(!empty($data)){
            $result=$this->getListTemplate($data,$style);
        }
        return $result;
    }

    //通用评论数据处理
    public function commentHandle($data){
        $data_count=count($data);
        $result=array();
        for($i=0;$i<$data_count;$i++){
            //数据处理
            $result[$i]['icon']= $data[$i]['avatar'] ? PicPath.$data[$i]['avatar'] : DEFAULT_CIRCLE_AVATAR;
            $result[$i]['name']= $data[$i]['truename'] ? $data[$i]['truename'] : '';
            $result[$i]['content']= $data[$i]['content'];
            $result[$i]['like']= $data[$i]['agree'];//评论点赞此时
            $result[$i]['time']= Common::friendlyDate($data[$i]['add_time'],'mohu');
            $result[$i]['cid']= $data[$i]['id']; //评论id
            $result[$i]['uid']= $data[$i]['uid']; //用户id
            
            if(!$data[$i]['uid']){
                $cond['reply_uid']=$data[$i]['uid']; //被评论者id
                $user=$this->detail('member',$cond,$fields='truename');
                $result[$i]['reply_name']= isset($user['truename']) ? $user['truename'] : '';
            }
            

        }
        return $result;
    }

    //投票推荐数据处理
    public function voteHandle($data){
        $data_count=count($data);
        $result=array();
        for($i=0;$i<$data_count;$i++){
            //数据处理
            $result[$i]['icon']= $data[$i]['avatar'] ? PicPath.$data[$i]['avatar'] : DEFAULT_CIRCLE_AVATAR;
            $result[$i]['uid']= $data[$i]['uid'];
            $result[$i]['member_name']= $data[$i]['truename'] ? $data[$i]['truename'] : '';
        }
        return $result;
    }

    //赞数据处理
    public function zanHandle($data){
        $data_count=count($data);
        $result=array();
        for($i=0;$i<$data_count;$i++){
            //数据处理
            $result[$i]['icon']= $data[$i]['avatar'] ? PicPath.$data[$i]['avatar'] : DEFAULT_CIRCLE_AVATAR;
            $result[$i]['name']= $data[$i]['truename'] ? $data[$i]['truename'] : '';
            $result[$i]['content']= $data[$i]['content'];
            $result[$i]['time']= Common::friendlyDate($data[$i]['add_time'],'mohu');
            $result[$i]['cid']= $data[$i]['id'];
        }
        return $result;
    }
    
    //广告数据处理
    public function advMapping($data){
        $result=array();
        $result['aid']=$data['ap_id'];
        $result['pic']=$data['ap_pic'] ? PicPath.$data['ap_pic'] : DEFAULT_ADV_PIC;
        $result['url']=$data['ap_link'] ? $data['ap_link'] : '';
        return $result;
    }

    //公告数据处理
    public function noticeMapping($data){
        $result=array();
        $result['text']=$data['doc_title'];
        $result['url']=$data['doc_url'] ? $data['doc_url'] : '';
        return $result;
    }

    //获取List模板
    public function getListTemplate($data){

        $type=$data['ac_id'];
        // $type_array1=array();
        // $type_array2=array();
        // $type_array3=array();
        // $type_array4=array();
        //公共
        $member_info=$this->getMemberInfoByID($data['member_id'],'truename,member_name,member_id,avatar');
        $class_info=$this->getClassInfoByID($data['ac_id'],'ac_name,ac_id');
        
        $result['tid']=$data['article_id'];
        $result['title']=$data['article_title'];
        $result['desc']=$data['article_desc'] ? $data['article_desc'] : '';
        $result['url']=$data['article_url'] ? $data['article_url'] : '';
        $result['cover_img']=$data['article_pic'] ? PicPath.$data['article_pic'] : DEFAULT_TOPIC_ICON;
        if($type==26){
            $result['cover_img']='';
        }
        if($data['ac_id']==23){
            $result['address']=$data['address'] ? $data['address'] : '';
            $result['sponsor']=$data['sponsor'] ? $data['sponsor'] : '';
            $result['start_time']=date("Y-m-d H:i",$data['start_time']);
            $result['end_time']=date("Y-m-d H:i",$data['end_time']);
        }

        $result['uid']=$member_info['member_id'] ? $member_info['member_id'] : 0;
        $result['member_id']=$member_info['member_id'] ? $member_info['member_id'] : 0;
        $result['author_icon']=$member_info['avatar'] ? PicPath.$member_info['avatar'] : '';
        $result['author']=$member_info['truename'] ? $member_info['truename'] : '';

        $result['time']=date("m-d H:i",$data['article_time']);
        $result['like']=$data['zan_like'];
        $result['view']=$data['view'];
        $result['vote']=$data['vote'];
        $result['comment_count']=$data['comment_count'];
        
        $result['type_value']=$class_info['ac_id'];
        $result['type_name']=$class_info['ac_name'];

        if($style!='list'){
            $result['content']=$data['article_content'] ? $data['article_content'] : '';
            $pic_arr=explode(',', $data['pic']);
            
            if(in_array($type, array(26,29))){
                $result['img']='';
            }else{
                foreach ($pic_arr as $key => $value) {
                    $temp_pic_path=!empty($value) ? PicPath.$value : '';
                    $result['img'].=','.$temp_pic_path;
                }
                $result['img']=ltrim($result['img'],',');//图片
            }
            
            $result['video']=$data['video'] ? PicPath.$data['video'] : '';//视频
            $result['sound']=$data['sound'] ? PicPath.$data['sound'] : '';//音频
        }
        //文章媒体附件类型
        if(in_array($type, array(20,25))){
            $result['file_type']='video'; 
        }elseif(in_array($type, array(21,28,33,34,35))){
            $result['file_type']='sound'; 
        }elseif (in_array($type, array(26,29))) {
            //社团文档，同乡会交流分享
            $result['file_type']='word'; 
        }else{
            $result['file_type']='image';

            //区分英语角的主题列表
            if($result['video']!=""){
                $result['file_type']='video'; 
            }elseif($result['sound']!=""){
                $result['file_type']='sound'; 
            }
            
        }

        return $result;
    }

    //我的消息帖子数据
    public function MyMessageHandle($data){
        $type=$data['ac_id'];
        $result['tid']=$data['article_id'];
        $result['title']=$data['article_title'];
        $result['desc']=$data['article_desc'] ? $data['article_desc'] : '';
        $result['cover_img']=$data['article_pic'] ? PicPath.$data['article_pic'] : DEFAULT_MSG_TOPIC_ICON;
        $result['type_value']=$data['ac_id'];
        //文章媒体附件类型
        if(in_array($type, array(20,25))){
            $result['file_type']='video'; 
        }elseif(in_array($type, array(21,28,33,34,35))){
            $result['file_type']='sound'; 
        }elseif (in_array($type, array(26,29))) {
            //社团文档，同乡会交流分享
            $result['file_type']='word'; 
        }else{
            $result['file_type']='image';

            //区分英语角的主题列表
            if($result['video']!=""){
                $result['file_type']='video'; 
            }elseif($result['sound']!=""){
                $result['file_type']='sound'; 
            }
            
        }
        return $result;
    }

    //乾坤币明细列表数据处理
    public function CoinHandle($data){
        $data_count=count($data);
        for($i=0;$i<$data_count;$i++){
            //数据处理
            $data[$i]['logo']=$data[$i]['logo'] ? PicPath.$data[$i]['logo'] : DEFAULT_MSG_TOPIC_ICON;
            $data[$i]['add_time']= date("Y-m-d H:i:s",$data[$i]['add_time']);
            $data[$i]['amount']= $data[$i]['amount']>0 ? '+'.$data[$i]['amount'] : $data[$i]['amount'];
        }
        return $data;
    }

    /*
     *------------------------------------------------------------------------------------------------------------------------------------------------------------------------
     * 数据操作表
     *-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
    */
   
    //原生sql查询
    public function queryData($sql)
    {
        $Model =  M();
        $data=$Model->query($sql);
        return $data;
    }
    //detail
    public function detail($table,$condition,$fields='*') {
        return $this->table($this->table_prefix.$table)->where($condition)->field($fields)->find();
    }
   
    //初始化分页对象
    public function initPage($table,$condition,$pageNum)
    {
        $count = $this->table($this->table_prefix.$table)->where($condition)->count();
        $page = new Page($count,$pageNum);
        return $page;
    }

    //通用分页列表
    public function getList($table,$condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        return $this->table($this->table_prefix.$table)->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    }
    
   //添加
   public function addData($table,$param){
        $result=$this->table($this->table_prefix.$table)->field(true)->add($param);
        
       return $result;
   }

   // 删除
   public function delData($table,$condition){
        $result=$this->table($this->table_prefix.$table)->where($condition)->delete();
       return $result;
   }

   //修改
   public function saveData($table,$condition,$param){
        $result=$this->table($this->table_prefix.$table)->field(true)->where($condition)->save($param);
       return $result;
   }
    
    //签到
    public function addSignFinishRecord($uid,$task_id)
    {
        $data['uid']=$uid;
        $data['task_id']=$task_id;
        $data['finish_time']=NOW_TIME;
        return $this->addData('sign_task_finished',$data);
    }
    //获取用户信息
    public function getMemberInfoByID($id,$fields='*') {

        $result=$this->table($this->table_prefix.'member')->where(array('member_id' => $id))->field($fields)->find();
        $today=date('Y-m-d', NOW_TIME);
        $latest_time=$result['latest_signed_time'] ? $result['latest_signed_time'] : '';
        if($today==$latest_time){
            $result['is_signed']=1;
        }else{
            $result['is_signed']=0;
        }
        return $result;
    }
    //获取文章分类信息
    public function getClassInfoByID($id,$fields='*') {
        return $this->table($this->table_prefix.'section_class')->where(array('ac_id' => $id))->field($fields)->find();
    }

    //获取学生社团类型信息
    public function getClubTypeInfoByID($id,$fields='*') {
        return $this->table($this->table_prefix.'club_type')->where(array('id' => $id))->field($fields)->find();
    }

    //添加乾坤币日志
    public function addCoinLog($table,$data,$type,$extra)
    {
        $coin_rule=C('COIN_RULE');
        switch ($type) {
            // case 'sys_add':
            //     $data['type']='sys_add';
            //     $data['desc']='管理员调节金币【增加】';
            //     $data['admin_name']='admin';
            //     $data['amount']=$data['coin'];
            //     break;
            // case 'sys_reduce':
            //     $data['type']='sys_add';
            //     $data['desc']='管理员调节金币【减少】';
            //     $data['admin_name']='admin';
            //     $data['amount']=$data['coin'];
            //     break;    
            case 'video':
                $data['type']='video';
                $data['desc']='发布视频';
                $data['admin_name']='system';
                $data['amount']=$coin_rule['video'];
                break;
            case 'voice':
                $data['type']='voice';
                $data['desc']='发布音频';
                $data['admin_name']='system';
                $data['amount']=$coin_rule['voice'];
                break;
            case 'image':
                $data['type']='image';
                $data['desc']='发布图片';
                $data['admin_name']='system';
                $data['amount']=$coin_rule['image'];
                break;
            case 'zhuanfa':
                $data['type']='zhuanfa';
                $data['desc']='分享转发';
                $data['admin_name']='system';
                $data['amount']=!empty($extra) ? $extra['amount'] : $coin_rule['zhuanfa'];
                break;
            case 'signed':
                $data['type']='signed';
                $data['desc']='签到';
                $data['admin_name']='system';
                $data['amount']=$coin_rule['signed'];
                break;
            case 'serial_signed':
                $data['type']='serial_signed';
                $data['desc']='连续签到';
                $data['admin_name']='system';
                $data['amount']=$coin_rule['serial_signed'];
                break;
            case 'exchange_goods':
                $data['type']='exchange_goods';
                $data['desc']='兑换物品，兑换物品名称：'.$data['goods_name'];
                $data['admin_name']='system';
                $data['amount']=$data['coin'];
                break;
        }
        $data['member_id']=$data['member_id'];
        $data['member_name']=$data['member_name'] ? $data['member_name'] : '';
        $data['add_time']=NOW_TIME;
        $this->addData($table,$data);
    }

    //添加积分日志
    public function addPointLog($table,$data,$type)
    {
        $point_rule=C('POINT_RULE');
        switch ($type) {   
            case 'video':
                $data['type']='video';
                $data['desc']='发布视频';
                $data['admin_name']='system';
                $data['amount']=$point_rule['video'];
                break;
            case 'voice':
                $data['type']='voice';
                $data['desc']='发布音频';
                $data['admin_name']='system';
                $data['amount']=$point_rule['voice'];
                break;
            case 'image':
                $data['type']='image';
                $data['desc']='发布图片';
                $data['admin_name']='system';
                $data['amount']=$point_rule['image'];
                break;
        }

        $data['member_id']=$data['member_id'];
        $data['member_name']=$data['member_name'] ? $data['member_name'] : '';
        $data['add_time']=NOW_TIME;
        $this->addData($table,$data);
    }


    //自增、自减
    public function selfUpdateField($table,$field,$where,$operate="+",$step=1) {
        if(is_array($field)) {
            foreach($field as $value){
                $data[$value]   =   array('exp',$value.$operate.$step);
            }
        }else{
            $data[$field]   =   array('exp',$field.$operate.$step);
        }
        // print_r($data);
        return $this->table($this->table_prefix.$table)->field(true)->where($where)->save($data);
    }

    //获取幻灯片
    public function getAdsByCode(){
        // switch ($_REQUEST['type'])
        // {
        //     case 20: //榜单视频
        //         $condition['ap_code']='banner_bangdan_video';
        //         break;
        //     case 21: //榜单音频
        //         $condition['ap_code']='banner_bangdan_voice';
        //         break;
        //     case 22: //榜单照片
        //         $condition['ap_code']='banner_bangdan_photo';
        //         break;
        //     case 34: //英文台词演绎经典
        //         $condition['ap_code']='banner_english_doc_list';
        //         break;
        // }
        $condition['ap_code']='banner_bangdan';
        
        $ads=M('adv_position')->where($condition)->limit(0,5)->select();

        return $ads ? $ads :array();
    }

    //获取单页文章信息
    public function getDocument(){
        $condition=array();
        switch ($_REQUEST['type'])
        {
            case 1:
                $condition['doc_code']='latest_notice';
                break;
            case 2:
                $condition['doc_code']='contact_us';
                break;
            case 3:
                $condition['doc_code']='agreement';
                break;
            default:
                break;
        }
        $document=M('document')->where($condition)->find();
        return $document ? $document :array();
    }




}
