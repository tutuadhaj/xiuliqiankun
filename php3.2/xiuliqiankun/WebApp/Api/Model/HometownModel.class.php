<?php
/**
 * 同乡会模型
 *
 * 
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

namespace Api\Model;
use Api\Common\Common;
use Think\Model;


class HometownModel extends CommonModel{
    
    //根据id获取同乡会信息
    public function getAssociationInfoByID($id,$fields='*') {
        $data=$this->table($this->table_prefix.'association')->where(array('aid' => $id))->field($fields)->find();
        $data['img']=PicPath.$data['img'];
        return $data;
    }

	//获取同乡会申请信息
    public function getAssociationJoinInfoByID($id,$fields='*') {
        return $this->table($this->table_prefix.'association_join')->where(array('id' => $id))->field($fields)->find();
    }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------
    *数据处理
    * ------------------------------------------------------------------------------------------------------------------------------------------------------*/
    
    //同乡会申请数据处理
    public function associationApplyHandle($data,$style='list'){
        $result=array();
        if(!empty($data)){
            $data_uid = $data['uid'] ? $data['uid'] : 0;        
            $member_info=$this->getMemberInfoByID($data_uid,'member_name,avatar,member_id','school');
            $school_info=unserialize($member_info['school']);
            $result['id']=$data['id'];
            $result['uid']=$data_uid; //申请人id
            $result['aid']=$data['aid']; //申请同乡会的id
            $result['school']=$school_info['name']; //申请人学校信息
            $result['member_name']=$member_info['member_name'];  //申请人名称
            $result['icon']=$member_info['avatar'] ? PicPath.$member_info['avatar'] : ''; //申请人头像
            $result['association']=$data['association_name']; //申请同乡会的名称
            $result['time']=date("m-d H:i",$data['add_time']);
            if($style!='list'){
                $result['img']=$data['img'] ? PicPath.$data['img'] : '';
                $result['video']=$data['video'] ? PicPath.$data['video'] : '';
                $result['sound']=$data['sound'] ? PicPath.$data['sound'] : '';
            }
        }

        return $result;
    }
}
