<?php
/**
 * 手机端令牌模型
 *
 * 
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

namespace Api\Model;
use Think\Model;

class mb_user_tokenModel extends Model{

    /**
	 * 查询
     *
	 * @param array $condition 查询条件
     * @return array
	 */
    public function getMbUserTokenInfo($condition) {
        return $this->where($condition)->find();
    }

    public function getMbUserTokenInfoByToken($token) {
        if(empty($token)) {
            return null;
        }
        return $this->getMbUserTokenInfo(array('token' => $token));
    }

	/**
	 * 新增
	 *
	 * @param array $param 参数内容
	 * @return bool 布尔类型的返回结果
	 */
	public function addMbUserToken($param){
        return $this->add($param);
	}
	
	/**
	 * 删除
	 *
	 * @param int $condition 条件
	 * @return bool 布尔类型的返回结果
	 */
	public function delMbUserToken($condition){
        return $this->where($condition)->delete();
	}

    /**
     * 分页列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getTokenList($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        return $this->table($this->table_prefix.'mb_user_token')->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    }
}
