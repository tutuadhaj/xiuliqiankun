<?php
/**
 * 榜单模型
 *
 * 
 *
 *
 * @copyright  Copyright (c) 2007-2012 ShopNC Inc. (http://www.shopnc.net)
 * @license    http://www.shopnc.net
 * @link       http://www.shopnc.net
 * @since      File available since Release v1.1
 */

namespace Api\Model;
use Api\Common\Common;
use Think\Model;


class SectiontopicModel extends CommonModel{
	
	// 通用分类板块帖子分页列表
    public function getSectionTopicList($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        return $this->table($this->table_prefix.'sectiontopic')->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    }

    //榜单详情信息
    function getBandanTopicDetail($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        $data=$this->table($this->table_prefix.'sectiontopic')->where($condition)->field($fields)->find();
        return $this->bangdanHandle($data,'detail');
    }

    //是否赞过该帖子
    function getTopicSelfZanStatus($tid,$member_info) {
        
        $status=false;
        if(empty($member_info)){
            return $status;
        }
        $condition['aid']=$tid;
        $condition['uid']=$member_info['member_id'];
        $data=$this->table($this->table_prefix.'app_zan')->where($condition)->field('*')->find();
        if(!empty($data)){
            $status=true;
        }else{
            $status=false;
        }
        return $status;
    }

    //评论列表
    function getCommentList($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        $list=M('app_comment')->alias('e')->join('LEFT JOIN mxd_member as m'.' ON e.uid = m.member_id' )->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
        return $this->commentHandle($list);
    }

    //投票推荐列表
    function getRecommend($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        $list=M('app_vote')->alias('e')->join('LEFT JOIN mxd_member as m'.' ON e.uid = m.member_id' )->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
        return $this->voteHandle($list);
    }


}
