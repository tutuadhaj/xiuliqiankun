<?php
/**
 * 公开接口API
 * 英语角等接口
 * 不需要验证用户身份token
 * @author David
 *
 */
namespace Api\Controller;

use Common\Controller\ApiController;
use Think\Page;
use Api\Common\Common;

class EnglishController extends ApiController{

    //获取聊天室幻灯片
    public function getEnglishChatAds(){
        $condition['ap_code']='banner_english_doc_list'; 
        $ads=M('adv_position')->where($condition)->limit(0,5)->select();
        foreach($ads as $key=>$value){
            $value=$this->comMod->advMapping($value);
            $ads[$key] = $value;
        }
        $list = $ads ? $ads :array();
        $this->myApiPrint('',$list);
    }

    //英语角首页
    public function getEnglishIndex(){
        $city_id=$_REQUEST['city_id'] ? $_REQUEST['city_id'] : 0;
        // $this->myApiPrint(-3,'',array('msg'=>$city_id));
        // $currentTopic=$this->bangdanMod->getSectionTopicList(array('other_type'=>'week'),'*','article_sort desc,article_id desc',0,5);
        $this_monday=Common::this_monday(); //本周一
        $last_monday=$this_monday-7*86400;
        $next_monday=$this_monday+7*86400;
        $where='where ac_id=30 and article_show=1 and city_id='.$city_id;
        // $where='where vote!=0 and ac_id=30';  //投票数必须大于0
        $order=' order by total_vote desc,article_id desc';
        $between_condition="between ".$last_monday." and ".$this_monday;
        $sql='select s.*,IFNULL(count2,0), (vote+IFNULL(count2,0)) as total_vote from '.C('DB_PREFIX').'sectiontopic as s left join (SELECT aid as vote_aid,COUNT(id) as count2 FROM '.C('DB_PREFIX').'app_vote WHERE add_time '.$between_condition.' GROUP BY aid DESC ) as temp on s.article_id=vote_aid '.$where.$order.' limit 0,5';
        $currentTopic =$this->comMod->queryData($sql);//本期主题列表
        //处理数据
        $list=array();
        $extend_data['actData']=array();
        $actData=M('adv_position')->where(array('ap_code'=>'banner_english_doc_list'))->select();
        foreach($actData as $key=>$value){
            $extend_data['actData'][] = $this->comMod->advMapping($value);
        }
        foreach($currentTopic as $key=>$value){
            $list[] = $this->comMod->bangdanHandle($value);
        }

        $this->myApiPrint('',$list,$extend_data);
    }

    //英语角主题列表（（视频、音频、图片）加文字）
    public function getEnglishTopicList(){
        $city_id=$_REQUEST['city_id'] ? $_REQUEST['city_id'] : 0;
        $this_monday=Common::this_monday(); //本周一
        $last_monday=$this_monday-7*86400;
        $next_monday=$this_monday+7*86400;
        $where='where ac_id=30 and city_id='.$city_id;
        // $where='where vote!=0 and ac_id=30';  //投票数必须大于0
        $order=' order by total_vote desc,article_id desc';
        $between_condition="between ".$last_monday." and ".$this_monday;
        $sql='select s.*,IFNULL(count2,0), (vote+IFNULL(count2,0)) as total_vote from '.C('DB_PREFIX').'sectiontopic as s left join (SELECT aid as vote_aid,COUNT(id) as count2 FROM '.C('DB_PREFIX').'app_vote WHERE add_time '.$between_condition.' GROUP BY aid DESC ) as temp on s.article_id=vote_aid '.$where.$order.' limit 0,5';
        $currentTopic =$this->comMod->queryData($sql);//本期主题列表

        //投票主题列表
        $cond['ac_id']=30;
        $cond['article_show']=1;
        $table='sectiontopic';
        // $voteTopic = $this->comMod->getList($table,$cond,'*','article_id desc',0,5);
        $page=$this->comMod->initPage($table,$cond,5); //每页显示5条记录
        $totalPages = ceil($page->totalRows / $page->listRows); 
        $extend_data = $this->mobile_page($totalPages);
        $voteTopic=$this->comMod->getList($table,$cond,'*','article_id desc',$page->firstRow,$page->listRows);
        //轮播图
        $actData=M('adv_position')->where(array('ap_code'=>'banner_english_doc_list'))->select();
        //处理数据
        $extend_data['currentTopic']=array();
        $extend_data['voteTopic']=array();
        $extend_data['actData']=array();

        foreach($voteTopic as $key=>$value){
            $extend_data['voteTopic'][] = $this->comMod->bangdanHandle($value);
        }
        foreach($currentTopic as $key=>$value){
            $extend_data['currentTopic'][] = $this->comMod->bangdanHandle($value);
        }
        //轮播
        foreach($actData as $key=>$value){
            $extend_data['actData'][] = $this->comMod->advMapping($value);
        }

        $this->myApiPrint('','',$extend_data);
    }


    //英语角主题详情
    public function getEnglishTopicDetail(){
        $condition=array();
        $condition['article_id']=$_REQUEST['tid'];
        $condition['article_show']=1;
        $detail=$this->bangdanMod->getBandanTopicDetail($condition);
        if(empty($detail['member_id'])){
            $this->myApiPrint(-3,'',array('msg'=>'该文章不存在或没有权限操作'));
        }
        $recommend=$this->bangdanMod->getRecommend(array('aid'=>$_REQUEST['tid']),'*','id desc',0,6);
        $extend_data['recommend']=array();
        foreach($recommend as $key=>$value){
            $extend_data['recommend'][] = $value;
        }
        $this->myApiPrint('',$detail,$extend_data);
    }

    //英语角英文经典台词列表（视频或音频上传）(翻唱，台词，朗诵)
    public function getEnglishDocList(){
        $condition=array();
        $condition['ac_id']=$_REQUEST['type']; 
        $condition['article_show']=1;
        //帖子名称搜索
        if(!empty($_REQUEST['keywords'])){
            $condition['article_title']=array('like','%'.$_REQUEST['keywords'].'%');
        }
        $order='latest_comment_time desc,article_id desc';
        $actData=$this->comMod->getAdsByCode(); //幻灯片
        $count = $this->comMod->table(C('DB_PREFIX')."sectiontopic")->where($condition)->count();
        $page = new Page($count,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); //总页数
        $extend_data= $this->mobile_page($totalPages);
        $list=$this->bangdanMod->getSectionTopicList($condition,'*',$order,$page->firstRow,$page->listRows);
        foreach($actData as $key=>$value){
            $extend_data['actData'][] = $this->comMod->advMapping($value);
        }
        foreach($list as $key=>$value){
            $list[$key]=$this->comMod->bangdanHandle($value);
        }

        $this->myApiPrint('',$list,$extend_data);
    }


}