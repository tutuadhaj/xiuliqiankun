<?php
/**
 * 非公开接口API
 *  等
 * 需要验证用户身份token
 * @author David
 *
 */
namespace Api\Controller;

use Common\Controller\ApiController;
use Think\Page;
use Api\Common\Common;

class TestController extends ApiController{


    //社团申请创建列表
    public function updateClubType()
    {
        $label_data=$this->comMod->getList('club_type',array(),'*','sort desc,id desc');
        $list=$this->comMod->getList('club',array(),'*','id desc');
        foreach ($list as $key => $value) {

echo $label_data[$key]['id'].'---';
            $this->comMod->saveData('club',array('id'=>$value['id']),array('type'=>$label_data[$key]['id']));  
        }

        $this->myApiPrint(200,$club_list,$extend_data);

    }

    public function deleteData()
    {

        $table='app_zan';
        $condition=array();
        $condition['aid']=97;
        $condition['uid']=22;
        $this->comMod->delData($table,$condition);

    }

    //社团申请创建列表
    public function getClubApplyList()
    {
        $table='club';
        $cond=array('uid'=>$this->member_info['member_id']);
        $cond['status']=0;
        $page=$this->comMod->initPage($table,$cond,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); 
        $extend_data = $this->mobile_page($totalPages);
        $list=$this->comMod->getList($table,$cond,'*','id desc',$page->firstRow,$page->listRows);
        $club_list=array();
        foreach ($list as $key => $value) {
            $club_list[]=$this->communityMod->clubHandle($value);
        }

        $this->myApiPrint(200,$club_list,$extend_data);

    }

    //社团申请创建详情
    public function getClubApplyDetail()
    {   
        $condition=array();
        $condition['status']=0;
        $condition['id']=$_REQUEST['id'];
        $detail=$this->comMod->detail('club',$condition,$fields='*');
        $extends_data=$this->communityMod->clubHandle($detail,'detail');
        if($extend_data){
            $this->myApiPrint('',$extend_data);
        }else{
            $this->myApiPrint(200);
        }
    }

    //通过社团申请创建审核
    public function allowClubApply()
    {
        $data=array('status'=>1);
        $condition['id']=$_REQUEST['id'];
        $result=M("club")->where($condition)->save($data);
        if($result){
            $this->myApiPrint(200);
        }else{
            $this->myApiPrint(-3);
        }
    }


    public function getLevel()
    {
        $result=getMemberLevel(2000);
        echo json_encode($result);exit;
    }

    
    public function shuom($value='')
    {
        //分类说明
        /*
        *
        *同乡会-42
        *
        *27-家乡美 (home)
        *28-方言俏 (dialet)
        *29-交流分享 (communication)
        *
        * 榜单-15
        * 
        *20-最热视频 (hot_video)
        *21-最热语音 (hot_sound)
        *22-最热照片 (hot_pic)
        *
        * 社团-16
        * 
        *23-活动预热 (activity_discuss) 
        *24-活动照片 (activity_pic)
        *25-活动视频 (activity_video)
        *26-文档呈现 (hot_video)
        *
        * 英语角-18
        * 
        *30-本期主题 (currentTopic)
        *33-经典英文歌翻唱 (classical_fanchang)
        *34-经典英文台词演绎 (classical_taici)
        *35-经典英文朗诵 (classical_langsong)
        *
         */
        

    }

    //修改字段值
    public function query()
    {
        $a=Common::this_monday();
        echo date('Y-m-d H:i:s',$a); exit;
        $data=$this->comMod->query("select * from mxd_member");
        print_r($data);exit;
        $this->myApiPrint(200,'',array('msg'=>'修改成功'));
        
    }

    //修改字段值
    public function update_more()
    {


            $cond['member_id']=11;
            $data['coin']   =   array('exp','coin+2');
            $data['point']   =   array('exp','point-2');
            $this->comMod->saveData('member',$cond,$data);  
            $this->myApiPrint(200,'',array('msg'=>'修改成功'));
        
    }

    public function add_comment()
    {

        $data=array();
        $data['uid']=$_REQUEST['uid'];
        $data['content']=$_REQUEST['content'];
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['reply_cid']=$_REQUEST['reply_cid'];  //被回复的评论id
        $data['reply_uid']=$_REQUEST['reply_uid'];  //被回复的用户id
        $data['add_time']=NOW_TIME;
        $result=M('app_comment')->add($data);
        if($result){
            $this->myApiPrint('');
        }
    }

    public function add_vote()
    {

        $data=array();
        $data['uid']=$_REQUEST['uid'];
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['add_time']=NOW_TIME;
        $result=M('app_vote')->add($data);
        if($result){
            $this->myApiPrint('');
        }
    }

    public function add_zan()
    {

        $data=array();
        $data['uid']=$_REQUEST['uid'];
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['add_time']=NOW_TIME;
        $result=M('app_vote')->add($data);
        if($result){
            $this->myApiPrint('');
        }
    }

    //file[]多文件数组方式上传
    function upload_one(){
        if(!empty($_FILES['file']['size']))
        {
            $arc_img = 'aid_cover_'.$_REQUEST['uid'];
            $param = array('savePath'=>'association/aid_cover/','subName'=>'','files'=>$_FILES['file'],'saveName'=>$arc_img,'saveExt'=>'');
            $up_return = upload_one($param,0); //任意文件都能传
            if($up_return == 'error'){
                $this->myApiPrint(4005);
            }else{
                $info['img'] = $up_return;
            }
            print_r($info['img']);exit;
        }else{
            print_r('空数据');exit;
        }
        $this->myApiPrint('201');

    }

    //添加文档
    public function MultiUpload()
    {

        $type=$_REQUEST['type'];
        $uid=$this->member_info['member_id'];
        $cover_img=$_FILES['cover_img']; //保存封面图片对象
        $flag=false;
        // 媒体上传
        if(!empty($_FILES['file']['size']))
        {
            // print_r($_FILES['file']);
            // $unique_time=floor(microtime()*1000);
            unset($_FILES['cover_img']);//过滤封面图片
            //文章媒体附件类型
            if(in_array($type, array(20,25))){
                $media_type='video'; 
                $media_type2=1; 
                foreach ($_FILES['file']['name'] as $key => $value) {
                    if(empty($value)){
                        continue;
                    }
                    if(!$this->is_video_file($value)){
                        $this->myApiPrint(4005);
                    }
                }
            }elseif(in_array($type, array(21,28,33,34,35))){
                $media_type='sound'; 
                $media_type2=2; 
                foreach ($_FILES['file']['name'] as $key => $value) {
                    if(empty($value)){
                        continue;
                    }
                    if(!$this->is_sound_file($value)){
                        $this->myApiPrint(4005);
                    }
                }
            }else{
                $media_type='pic';  //多图
                foreach ($_FILES['file']['name'] as $key => $value) {
                    if(empty($value)){
                        continue;
                    }
                    if(!$this->is_photo_file($value)){
                        $this->myApiPrint(4005);
                    }
                }
            }
            $flag=true; //通过媒体文件验证
            //根据
            if($_REQUEST['cid']){
                $save_dir_path='club/media/'.$media_type;
            }elseif($_REQUEST['aid']){
                $save_dir_path='association/media/'.$media_type;
            }else{
                $save_dir_path='topic/media/'.$media_type;
            }

            $param = array('savePath'=>$save_dir_path.'/','subName'=>'','files'=>$_FILES,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
            if($media_type=='pic'){
                $up_return = multi_upload2($param); 
                
            }else{
                $up_return = multi_upload2($param,$media_type2); //图片
            }
            $up_return=implode(',', $up_return);
            //返回数据
            if($up_return == 'error'){
                $this->myApiPrint(4005);
            }else{
                $info[$media_type] = $up_return;
            }
            // echo $up_return.'<br/>';
        }else{
            $this->myApiPrint(4005);//上传失败
        }

        //封面上传
        if(!empty($cover_img['size']) && $flag)
        {
            $param = array('savePath'=>'club/club_article_cover/','subName'=>'','files'=>$cover_img,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
            $up_return = upload_one($param);
            if($up_return == 'error'){
                $this->myApiPrint(4005);
            }else{
                $info['article_pic'] = $up_return;
            }
            // echo $up_return.'<br/>';
        }

        //保存数据
        $info['ac_id'] = $type; //文章分类id
        $info['member_id'] = $uid ? $uid : 0; //作者id
        $info['cid'] = $_REQUEST['cid']; //社团id
        $info['article_title'] = $_REQUEST['title'];
        $info['article_content'] = $_REQUEST['content'];
        $info['article_time'] = NOW_TIME;
        $result =  M("sectiontopic")->add($info);
        if($result){
            $this->myApiPrint(200);
        }else{
            $this->myApiPrint(-3);
        }
    }

//发布文章帖子
    public function creatArticle()
    {
        $type=$_REQUEST['type'];
        $uid=$this->member_info['member_id'];
        $cover_img=$_FILES['cover_img']; //保存封面图片对象
        $flag=false;
        $extends_data=array(); //错误自定义扩展信息
        //处理$_FILES数据，并返回媒体附件类型
        foreach ($_FILES as $key => $value) {
            //过滤封面图片
            if($key=='cover_img'){
                unset($_FILES['cover_img']); //剔除封面文件
            }else{
                if(!$value['size']){
                    $extends_data['msg']='请上传媒体附件';
                    $this->myApiPrint(4005,'',$extends_data);
                }
                $media_type=$this->returnUploadType($value['name'],$type); //返回媒体类型
                $_FILES[]=$value;
                unset($_FILES[$key]);
            }
        }
        //返回媒体附件数据
        if($_REQUEST['cid']){
            $save_dir_path='club/media/'.$media_type;
        }elseif($_REQUEST['aid']){
            $save_dir_path='association/media/'.$media_type;
        }else{
            $save_dir_path='topic/media/'.$media_type;
        }
        $param = array('savePath'=>$save_dir_path.'/','subName'=>'','files'=>$_FILES,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
        $up_return = multi_upload2($param); 
        $up_return=implode(',', $up_return);
        if($up_return == 'error'){
            $extends_data['msg']='媒体附件上传失败';
            $this->myApiPrint(4005,'',$extends_data);
        }else{
            $info[$media_type] = $up_return;
        }
        //需要上传封面时候上传
        if($cover_img && $cover_img['size']){
            $param = array('savePath'=>'club/club_article_cover/','subName'=>'','files'=>$cover_img,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
            $up_return = upload_one($param);
            if($up_return == 'error'){
                $extends_data['msg']='封面照上传失败';
                $this->myApiPrint(4005,'',$extends_data);
            }else{
                $info['article_pic'] = $up_return;
            }
        }else{
            $extends_data['msg']='请上传封面照片';
            $this->myApiPrint(4005,'',$extends_data);
        }
        // if(empty($info['article_pic']) || empty($info[$media_type])){

        // }
        //保存数据
        $info['ac_id'] = $type; //文章分类id
        $info['member_id'] = $uid ? $uid : 0; //作者id
        $info['cid'] = $_REQUEST['cid']; //社团id
        $info['article_title'] = $_REQUEST['title'];
        $info['article_content'] = $_REQUEST['content'];
        $info['article_time'] = NOW_TIME;
        $result =  M("sectiontopic")->add($info);
        if($result){
            $this->myApiPrint(200);
        }else{
            $this->myApiPrint(-3);
        }


        //测试
        // $this->myApiPrint(200,'',$_FILES);
        // $file1=$_FILES['file']['name'][0] ? $_FILES['file']['name'][0] : '空';
        // $file2=$_FILES['file']['name'][1] ? $_FILES['file']['name'][1] : '空';
        // $file3=$_FILES['file']['name'][2] ? $_FILES['file']['name'][2] : '空';
        // $msg='文件数：'.count($_FILES['file']['name']).'\n\t'.'文件1：'.$file1.'\n\t'.'文件2：'.$file2.'\n\t'.'文件3：'.$file3.'\n\t';
        // $this->myApiPrint(200,'',array('msg'=>$msg));

    }

    //根据板块获取帖子列表接口
    public function index(){
        $condition=array();
        $condition['ac_id']=$_REQUEST['type'];

        
        $condition['article_show']=1;
        $list=array();
        $extend_data=array();
        $extend_data['actData']=array();
        $count = $this->comMod->table(C('DB_PREFIX')."sectiontopic")->where($condition)->count();
        $page = new Page($count,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); //总页数
        $list = $this->bangdanMod->getSectionTopicList($condition,'*','article_sort desc,article_id desc',$page->firstRow,$page->listRows);
        $extend_data=$this->mobile_page($totalPages);
        //data
        foreach($list as $key=>$value){
            $value2=$this->comMod->bangdanHandle($value);
            if($key<3){
                $data_lsit[0]['layoutType']='1';
                $data_lsit[0]['itemData'][]=$value2;
            }else{
                $data_lsit[1]['layoutType']='2';
                $data_lsit[1]['itemData'][]=$value2;
            }
            unset($value2);
        }

        $this->myApiPrint('',$data_lsit,$extend_data);
    }




    /* 单文件上传文件,带缩略图，暂时未用 */
    public function upload(){
        $s_imgpath=$this->oneupload($dir='./Uploads/club/club_article_cover/',120,120);
        $data['thumb'] = $s_imgpath; 
        $data['image_url']=str_replace("_thumb", "", $s_imgpath);
        print_r($data);
    }
    
    function oneupload($dir='./upload/memberhead',$thumb_width,$thumb_height)
    {
        $f_upload = new \Api\Common\MyUpload(); 
        if($_FILES['photo']['tmp_name']){
            $f_upload->set_file_type($_FILES['photo']['type']);   # 获得文件类型
            $f_upload->set_file_name($_FILES['photo']['name']);   # 获得文件名称
            $f_upload->set_file_size($_FILES['photo']['size']);   # 获得文件尺寸
            $f_upload->set_upfile($_FILES['photo']['tmp_name']);  # 服务端储存的临时文件名
            $f_upload->set_size(5120);               # 设置每个单文件上传不超过5120KB
            $f_upload->set_base_directory($dir);    # 文件存储根目录名称
            $f_upload->save();
            if( move_uploaded_file($_FILES['photo']['tmp_name'],$f_upload->file_path)){   
                if($thumb_width!=""){
                   $s_imgpath=$f_upload->produce_thumb($f_upload->file_path, $thumb_width, $thumb_height, $f_upload->flash_directory."/");
                   $s_imgpath=str_replace("./", "", $s_imgpath);
                   return $s_imgpath; 
                }else{
                    return str_replace("./", "", $f_upload->file_path);
                }
            }
        }
    }
}