<?php
/**
 * 公开接口API
 * 榜单等接口
 * 不需要验证用户身份token
 * @author David
 *（已经测试）
 */
namespace Api\Controller;

use Common\Controller\ApiController;
use Think\Page;

class BangdanController extends ApiController{
    

    //获取版本更新
    public function updateInfo(){
        $result=array(
            'version'=>2,
            'description'=>'更新内容说明如下:\n',
            'url'=>'http://xiuliqiankun.cn/xiuliqiankun/Uploads/apk/xlqk.apk'
        );
        $this->myApiPrint(200,$result);
    }

    //获取开屏广告接口
    public function startAdv()
    {
        $condition['ap_code']='welcome';
        $ads=$this->comMod->detail('adv_position',$condition,$fields='ap_id,ap_name,ap_link,ap_pic');

        $ads['aid']=$ads['ap_id'] ? $ads['ap_id'] : 0;
        $ads['pic']=$ads['ap_pic'] ? PicPath.$ads['ap_pic'] : DEFAULT_ADV_PIC;
        $ads['url']=$ads['ap_link'] ? $ads['ap_link'] : '';

        unset($ads['ap_id']);
        unset($ads['ap_pic']);
        unset($ads['ap_link']);

        $this->myApiPrint(200,$ads);
    }
    

    //根据客户端定位城市获取本地城市信息
    public function getLocatCity()
    {
        $condition['name']=array('like','%'.$_REQUEST['name'].'%');
        $result=$this->comMod->table(C('DB_PREFIX')."district")->where($condition)->find();
        if($result){
            $region['id'] = $result['id'];
            $region['name'] = $result['name'];
            $region['first_letter'] = $result['first_letter'];
            $this->myApiPrint('',$region);
        }else{
            $this->myApiPrint(-3);
        }
    }

    //获取城市选择列表
    public function getHotCityList()
    {
        $table='district';
        $cond['upid']=array('neq',35);//中国（去除国外城市）
        $cond['level']=$_REQUEST['level'] ? $_REQUEST['level'] : 1;  //默认取省
        $cond['usetype']=0;//非热门市
        if($_REQUEST['id']){
            $cond['upid']=$_REQUEST['id'];
        }
        //搜索城市
        if(intval($_REQUEST['name'])){
            $cond['name']=array('like','%'.$_REQUEST['name'].'%');
        }
        $page=$this->comMod->initPage($table,$cond,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); 
        $extend_data = $this->mobile_page($totalPages);
        $list=$this->comMod->getList($table,$cond,'*','first_letter asc',$page->firstRow,$page->listRows);
        foreach ($list as $key => $value) {
            $region['id'] = $value['id'];
            $region['name'] = $value['first_letter'].'-'.$value['name'];
            $region['first_letter'] = $value['first_letter'];
            $list[$key]=$region;
        }

        $this->myApiPrint('',$list,$extend_data);
    } 

    //发布文章帖子
    public function creatArticle()
    {
        $coin_rule=C('COIN_RULE');
        $point_rule=C('POINT_RULE');
        $add_coin_amount=0; //自动获得金币数
        $add_point_amount=0; //自动获得积分数
        $log_operate_name='document'; //积分和金币获得的操作名称(默认文档发布是不能获得积分和乾坤币的)

        $type=$_REQUEST['type'];
        $city_id=$_REQUEST['city_id'] ? $_REQUEST['city_id'] : 0;
        if(!$city_id){
            $this->myApiPrint(-3,'',array('msg'=>'未定位到城市信息，无法发帖'));
        }
        $uid=$this->member_info['member_id'] ? $this->member_info['member_id'] : 0;
        $cover_img=$_FILES['cover_img']; //保存封面图片对象
        $flag=false;
        $extends_data=array(); //错误自定义扩展信息
        //处理$_FILES数据，并返回媒体附件类型
        foreach ($_FILES as $key => $value) {
            //过滤封面图片
            if($key=='cover_img'){
                unset($_FILES['cover_img']); //剔除封面文件
            }else{
                // if(!$value['size']){
                //     $extends_data['msg']='请上传媒体附件';
                //     $this->myApiPrint(4005,'',$extends_data);
                // }
                //返回媒体类型，并验证附件文件格式
                if(!empty($_REQUEST['media_type'])){
                    //media_type:video|sound|pic
                    $media_type=$_REQUEST['media_type'];
                }else{
                    $media_type=$this->returnUploadType($value['name'],$type); 
                }

                $_FILES[]=$value;
                unset($_FILES[$key]);
            }
        }
        //需要上传媒体文件时候上传
        if($media_type){
            
            if($media_type=='sound'){           
                $add_coin_amount=$coin_rule['voice']; 
                $add_point_amount=$point_rule['voice']; 
                $log_operate_name='voice';
            }elseif($media_type=='video'){
                $add_coin_amount=$coin_rule['video'];
                $add_point_amount=$point_rule['video']; 
                $log_operate_name='video';
            }elseif($media_type=='pic'){
                $add_coin_amount=$coin_rule['image'];
                $add_point_amount=$point_rule['image']; 
                $log_operate_name='image';
            }
            //返回媒体附件数据
            if($_REQUEST['cid']){
                $save_dir_path='club/media/'.$media_type;
            }elseif($_REQUEST['aid']){
                $save_dir_path='association/media/'.$media_type;
            }else{
                $save_dir_path='topic/media/'.$media_type;
            }
            $param = array('savePath'=>$save_dir_path.'/','subName'=>'','files'=>$_FILES,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
            $up_return = multi_upload2($param); 
            $up_return=implode(',', $up_return);
            if($up_return == 'error'){
                $extends_data['msg']='媒体附件上传失败';
                $this->myApiPrint(4005,'',$extends_data);
            }else{
                $info[$media_type] = $up_return;
            }

            if($media_type=='video' && empty($info[$media_type])){
               $this->myApiPrint(-3,'',array('msg'=>'视频可能太大，无法上传'));  
            }
        }

        //需要上传封面时候上传
        if($cover_img && $cover_img['size']){
            $param = array('savePath'=>'club/club_article_cover/','subName'=>'','files'=>$cover_img,'saveName'=>$_SERVER['REQUEST_TIME'],'saveExt'=>'');
            $up_return = upload_one($param);
            if($up_return == 'error'){
                $extends_data['msg']='封面照上传失败';
                $this->myApiPrint(4005,'',$extends_data);
            }else{
                $info['article_pic'] = $up_return;
            }
        }
        // else{
        //     $extends_data['msg']='请上传封面照片';
        //     $this->myApiPrint(4005,'',$extends_data);
        // }
        // if(empty($info['article_pic']) || empty($info[$media_type])){

        // }
        //保存数据
        $info['member_id'] = $uid; //作者id
        $info['ac_id'] = $type; //文章分类id
        $info['cid'] = $_REQUEST['cid'] ? $_REQUEST['cid'] : 0; //社团id
        $info['aid'] = $_REQUEST['aid'] ? $_REQUEST['aid'] : 0; //同乡会id
        $info['article_title'] = $_REQUEST['title'];
        $info['article_content'] = $_REQUEST['content'];
        $info['article_time'] = NOW_TIME;
        $info['city_id'] = $city_id; //发布城市
        
        if($type==23){
            //最新活动发布特有
            $info['sponsor'] = $_REQUEST['sponsor'];
            $info['address'] = $_REQUEST['address'];
            $info['start_time'] = strtotime($_REQUEST['start_time']);
            $info['end_time'] = strtotime($_REQUEST['end_time']);
        }
        $result =  M("sectiontopic")->add($info);
        if($result){
            if($type!=23 && $log_operate_name!='document'){
                $if_can_get_coin=$this->if_can_get_coin($uid,$type);
                //发布成功后，用户自动增加相应积分和乾坤币(社团发活动不能获得积分和乾坤币)
                //添加乾坤币记录(视频、音频、图片分别每天有3次获取金币和积分的机会)
                if($if_can_get_coin['flag']){
                    
                    $addcoin_info_record=$if_can_get_coin['addcoin_info'];
                    unset($if_can_get_coin['flag']);
                    unset($if_can_get_coin['addcoin_info']);
                    $addcoin_info_serial=serialize($if_can_get_coin);
                    $auto_update_data[$addcoin_info_record]=$addcoin_info_serial;
                    $auto_update_data['coin']   =   array('exp','coin+'.$add_coin_amount);
                    $auto_update_data['point']   =   array('exp','point+'.$add_point_amount);

                    //金币日志
                    $log_data['member_id']=$uid;
                    $log_data['member_name']=$this->member_info['member_name'];
                    $this->comMod->addCoinLog('coin_log',$log_data,$log_operate_name);

                    //添加积分记录
                    $this->comMod->addPointLog('point_log',$log_data,$log_operate_name);

                    //修改用户积分和金币数量
                    $this->comMod->saveData('member',array('member_id'=>$uid),$auto_update_data);
                }else{
                    $this->myApiPrint(-3);
                }
                
            }
            $this->myApiPrint(200);
        }else{
            $this->myApiPrint(-3);
        }


        //测试
        // $this->myApiPrint(200,'',$_FILES);
        // $file1=$_FILES['file']['name'][0] ? $_FILES['file']['name'][0] : '空';
        // $file2=$_FILES['file']['name'][1] ? $_FILES['file']['name'][1] : '空';
        // $file3=$_FILES['file']['name'][2] ? $_FILES['file']['name'][2] : '空';
        // $msg='文件数：'.count($_FILES['file']['name']).'\n\t'.'文件1：'.$file1.'\n\t'.'文件2：'.$file2.'\n\t'.'文件3：'.$file3.'\n\t';
        // $this->myApiPrint(200,'',array('msg'=>$msg));

    }

    //通用评论列表(ok)
    public function getCommentList(){
        // $condition['type']=$_REQUEST['type'];
        $condition['aid']=$_REQUEST['tid'];
        $order='id desc';
        $count = $this->comMod->table(C('DB_PREFIX')."app_comment")->where($condition)->count();
        $page = new Page($count,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); //总页数
        $extend_data = $this->mobile_page($totalPages);
        $comment=$this->bangdanMod->getCommentList($condition,'*',$order,$page->firstRow,$page->listRows);
        $extend_data['count']=$count;
        $this->myApiPrint(200,$comment,$extend_data);
    }

   //  protected function getCommentList($parent_id = 0,&$result = array()){       
   //  $arr = M('comment')->where(parent_id = '.$parent_id.')->order(create_time desc)->select();   
   //  if(empty($arr)){
   //      return array();
   //  }
   //  foreach ($arr as $cm) {  
   //      $thisArr=&$result[];
   //      $cm[children] = $this->getCommlist($cm[id],$thisArr);    
   //      $thisArr = $cm;                                    
   //  }
   //  return $result;
   // }


    //通用推荐票用户列表
    public function getRecommendList(){
        // $condition['type']=$_REQUEST['type'];
        $condition['aid']=$_REQUEST['tid'];
        $order='id desc';
        $count = $this->comMod->table(C('DB_PREFIX')."app_vote")->where($condition)->count();
        $page = new Page($count,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); //总页数
        $extend_data = $this->mobile_page($totalPages);
        $recommend=$this->bangdanMod->getRecommend($condition,'*',$order,$page->firstRow,$page->listRows);

        $this->myApiPrint(200,$recommend,$extend_data);
    }

    //添加评论(添加语音的回复没有加)
    public function add_comment()
    {
        $add_time=NOW_TIME;
        $uid=$this->member_info ? $this->member_info['member_id'] : 0;
        $data=array();
        $data['uid']=$uid;
        $data['content']=$_REQUEST['content'];
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['reply_cid']=$_REQUEST['reply_cid'];  //被回复的评论id
        $data['reply_uid']=$_REQUEST['reply_uid'];  //被回复的评论的用户id
        $data['add_time']=$add_time;
        $result=M('app_comment')->add($data);
        if($result){
            //评论数+1,更新最新评论时间
            $cond['article_id']=$_REQUEST['aid'];
            $sectiontopic_data['comment_count']   =   array('exp','comment_count+1');
            $sectiontopic_data['latest_comment_time']   = $add_time;  
            $this->comMod->saveData('sectiontopic',$cond,$sectiontopic_data);

            $this->myApiPrint('');
        }
    }

    //投推荐票
    public function add_vote()
    {

        $data=array();
        $uid=$this->member_info ? $this->member_info['member_id'] : 0;
        $data['uid']=$uid;
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['reply_uid']=$_REQUEST['reply_uid'];
        $data['add_time']=NOW_TIME;
        //查询用户总的推荐票数
        $member_tools=$this->comMod->detail('member_tools',array('member_id'=>$uid,'goods_id'=>3),$fields='amount');
        if($member_tools['amount'] > 0){

            //推荐票数+1
            $result=M('app_vote')->add($data);
            $cond['article_id']=$_REQUEST['aid'];
            $this->comMod->selfUpdateField('sectiontopic','vote',$cond,$operate="+",1);

            //我的道具推荐票-1
            $this->comMod->selfUpdateField('member_tools','amount',array('member_id'=>$uid,'goods_id'=>3),$operate="-",1);
            
            //限制用户每天可以投票的数量
            $today_start=strtotime(date('Y-m-d'));    //今天早上凌晨0点
            $today_end=strtotime(date('Y-m-d'))-1+24*3600;    //今天晚上12点
            $condition['uid']=$uid;
            $condition['add_time']=array('between',$today_start.','.$today_end);
            $count = $this->comMod->table(C('DB_PREFIX')."app_vote")->where($condition)->count();
            if($count>6){
                $this->myApiPrint(-3,'',array('msg'=>'每天推荐票最多只能使用6次哦'));
            }

            $this->myApiPrint(200,'',array('msg'=>'推荐成功'));
        }else{
            $this->myApiPrint(-3,'',array('msg'=>'推荐票用完了哟'));
        }
        
    }

    //收藏（喜欢）
    public function add_zan()
    {
        
        $data=array();
        $uid=$this->member_info ? $this->member_info['member_id'] : 0;
        $data['uid']=$uid;
        $data['ip']=get_client_ip();
        $data['type']=$_REQUEST['type'];   //评论类型
        $data['aid']=$_REQUEST['aid'];    //被评论的文章id
        $data['reply_uid']=$_REQUEST['reply_uid'];
        $data['add_time']=NOW_TIME;

        $operate='';
        //查询是否喜欢
        $zanstatus=$this->bangdanMod->getTopicSelfZanStatus($_REQUEST['tid'],$this->member_info);
        if($zanstatus){
            //已收藏
            $operate="-"; //取消收藏
            $result=M('app_zan')->where(array('aid'=>$_REQUEST['aid'],'uid'=>$uid))->delete();
            $msg='取消收藏';
        }else{
            //未收藏
            $operate="+"; //加入收藏
            $result=M('app_zan')->add($data);
            $msg='收藏成功';
        }

        if($result){
            //zan数量+1或-1
            $cond=array();
            $cond['article_id']=$_REQUEST['aid'];
            $this->comMod->selfUpdateField('sectiontopic','zan_like',$cond,$operate,1);
        }

        $this->myApiPrint(200,'',array('msg'=>$msg));
        
    }

    //第三方分享后，用户获得乾坤币
    public function shareBack()
    {
        
        $today=date('Y-m-d');
        $update_share_data=array();
        $coin_rule=C('COIN_RULE');
        $add_coin_amount=$coin_rule['zhuanfa'];
        $uid=$this->member_info['member_id'] ? $this->member_info['member_id'] : 0;
        $cond['member_id']=$uid;
        $memberInfo=$this->comMod->detail('member',$cond,'share_info');

        if($memberInfo['share_info']!=null && !empty($memberInfo['share_info'])){

            $share_info=unserialize($memberInfo['share_info']);

            if($share_info['date']!=$today){
                $update_share_data['date']=$today;
                $update_share_data['count']=1;
            }else{
                $update_share_data['count']=$share_info['count']+1;
                $update_share_data['date']=$share_info['date'];
            }

            if($share_info['count']>3){
                
                $add_coin_amount=1;
            }

        }else{
            $update_share_data['count']=1;
            $update_share_data['date']=$today;
            $add_coin_amount=1;
        }

        $share_info_serial=serialize($update_share_data);
        $data['share_info']=$share_info_serial;  //更新用户分享信息
        //用户乾坤币++
        $data['coin']   =   array('exp','coin+'.$add_coin_amount);
        $this->comMod->saveData('member',array('member_id'=>$uid),$data);
        //添加乾坤币记录
        $log_data['member_id']=$uid;
        $log_data['member_name']=$this->member_info['member_name'];
        $this->comMod->addCoinLog('coin_log',$log_data,'zhuanfa',array('amount'=>$add_coin_amount));

        $this->myApiPrint(200);

    }

    /*--------------------------------------------------------------------------------------------------------------------------------------------------------
    *榜单
    * ------------------------------------------------------------------------------------------------------------------------------------------------------*/
    //根据类型获取板块列表接口（1-榜单,2-社团_voice,3-同乡会，4-英语角）
    public function getSectionListByType(){
        $typelist=M('section_class')->where(array('ac_code'=>$_REQUEST['type']))->select();
        foreach ($typelist as $key => $value) {
            $new['id']=$value['ac_id'];
            $new['name']=$value['ac_name'];
            $list[]=$new;
        }
        $this->myApiPrint('',$list);
    }

    //根据板块获取帖子列表接口(参数type对应表ac_id)
    // public function getTopicListBySection(){
    //     $city_id=$_REQUEST['city_id'] ? $_REQUEST['city_id'] : 0;
    //     $condition=array();
    //     $order='article_sort desc,article_id desc';
    //     $condition['ac_id']=$_REQUEST['type'];
    //     if(!empty($_REQUEST['sort']) && $_REQUEST['sort']=='hot'){
    //         $order='vote desc,article_sort desc,article_id desc';   //最热(按推荐数排序)
    //     }

    //     //
    //     if($city_id){
    //         $condition['city_id']=$_REQUEST['city_id'];
    //         $this->myApiPrint(-3,'',array('msg'=>$_REQUEST['city_id'].'---'));
    //     }

    //     $condition['article_show']=1;
    //     $list=array();
    //     $extend_data=array();
    //     $extend_data['actData']=array();
    //     $count = $this->comMod->table(C('DB_PREFIX')."sectiontopic")->where($condition)->count();
    //     $page = new Page($count,$this->pageNum);
    //     $totalPages = ceil($page->totalRows / $page->listRows); //总页数
    //     $list = $this->bangdanMod->getSectionTopicList($condition,'*',$order,$page->firstRow,$page->listRows);
    //     $extend_data=$this->mobile_page($totalPages);
    //     //data
    //     foreach($list as $key=>$value){
    //         $value2=$this->comMod->bangdanHandle($value);
    //         if($key<3){
    //             $data_lsit[0]['layoutType']='1';
    //             $data_lsit[0]['itemData'][]=$value2;
    //         }else{
    //             $data_lsit[1]['layoutType']='2';
    //             $data_lsit[1]['itemData'][]=$value2;
    //         }
    //         unset($value2);
    //     }

    //     //幻灯片
    //     $actData=$this->comMod->getAdsByCode();
    //     foreach($actData as $key=>$value){
    //         $extend_data['actData'][] = $this->comMod->advMapping($value);
    //     }
    //     //通知
    //     $noticeData=M('document')->where(array('doc_code'=>'latest_notice'))->select();
    //     foreach($noticeData as $key=>$value){
    //         $extend_data['notice'][] = $this->comMod->noticeMapping($value);
    //     }
    //     $this->myApiPrint('',$data_lsit,$extend_data);
    // }

    //根据板块获取帖子列表接口(参数type对应表ac_id)
    public function getTopicListBySection(){
        $city_id=$_REQUEST['city_id'] ? $_REQUEST['city_id'] : 0;
        $order='article_sort desc,article_id desc';

        $cur_page = intval($_REQUEST['curpage']);//当前页数

        if(!empty($_REQUEST['sort']) && $_REQUEST['sort']=='hot'){
            //最热(按推荐数,评论时间，发布时间排序)
            $order=' order by total_vote desc,latest_comment_time desc,article_id desc';
            if($cur_page>10){
                $if_top_100=true;
            }
        }else{
            //最新（按评论时间，发布时间排序）
            $order=' order by latest_comment_time desc,article_id desc';
        }

        $where='';
        if($_REQUEST['type']=='image'){
            $where=' where pic is not null';
        }elseif($_REQUEST['type']=='voice'){
            $where=' where sound is not null';
        }elseif($_REQUEST['type']=='video'){
            $where=' where video is not null';
        }

        $where.=' and article_show=1';
        //榜单列表显示平台全部帖子，不区分地域
        // if($city_id){
        //     if(!empty($where)){
        //         $where.=' and city_id='.$_REQUEST['city_id'];
        //     }else{
        //         $where.=' where city_id='.$_REQUEST['city_id'];
        //     }
            
        // }

        $beforeday_4_clock=strtotime(date('Y-m-d'))-36*3600;  //前天下午四点
        $yesterday_4_clock=strtotime(date('Y-m-d'))-8*3600;  //昨天下午四点
        $today_4_clock=strtotime(date('Y-m-d'))+16*3600;    //今天下午四点
        $current_time=NOW_TIME; //当前时间
        if($current_time>=$today_4_clock){
            $between_condition="between ".$yesterday_4_clock." and ".$today_4_clock;
        }else{
            $between_condition="between ".$beforeday_4_clock." and ".$yesterday_4_clock;
        }

        $list=array();
        $extend_data=array();
        $extend_data['actData']=array();
        $extend_data['notice']=array();
        $sql='select count(article_id) as count from '.C('DB_PREFIX').'sectiontopic '.$where;
        $Model =  M();
        $data1=$Model->query($sql);
        $count=!empty($data1) ? $data1[0]['count'] : 0;
        $page = new Page($count,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); //总页数
        
        if($cur_page>1){
            $limit=($page->firstRow-1).','.$page->listRows;
            $limit=$if_top_100 ? $page->firstRow.',0' : $limit;
        }else{
            $limit='0,9';
        }

        $sql='select s.*,IFNULL(count2,0), (vote+IFNULL(count2,0)) as total_vote from '.C('DB_PREFIX').'sectiontopic as s left join (SELECT aid as vote_aid,COUNT(id) as count2 FROM '.C('DB_PREFIX').'app_vote WHERE add_time '.$between_condition.' GROUP BY aid DESC
) as temp on s.article_id=vote_aid '.$where.$order.' limit '.$limit;
        $list =$this->comMod->queryData($sql);
        $extend_data=$this->mobile_page($totalPages);
        //data
        foreach($list as $key=>$value){
            $value2=$this->comMod->bangdanHandle($value);
            if ($cur_page==1) {
                if($key<3){
                    $data_lsit[0]['layoutType']='1';
                    $data_lsit[0]['itemData'][]=$value2;
                }else{
                    $data_lsit[1]['layoutType']='2';
                    $data_lsit[1]['itemData'][]=$value2;
                }
            }else{
                $data_lsit[0]['layoutType']='2';
                $data_lsit[0]['itemData'][]=$value2;
            }
            
            unset($value2);
        }

        //幻灯片
        $actData=$this->comMod->getAdsByCode();
        foreach($actData as $key=>$value){
            $extend_data['actData'][] = $this->comMod->advMapping($value);
        }
        //通知
        $noticeData=M('document')->where(array('doc_code'=>'latest_notice'))->select();
        foreach($noticeData as $key=>$value){
            $extend_data['notice'][] = $this->comMod->noticeMapping($value);
        }
        $this->myApiPrint('',$data_lsit,$extend_data);
    }


    //搜索帖子 
    public function searchTopic()
    {   

        $table='sectiontopic';
        $cond['article_title']=array('like','%'.$_REQUEST['title'].'%');
        $cond['article_show']=1;
        $page=$this->comMod->initPage($table,$cond,$this->pageNum);
        $totalPages = ceil($page->totalRows / $page->listRows); 
        $extend_data = $this->mobile_page($totalPages);
        $list=$this->comMod->getList($table,$cond,'*','article_sort desc,article_id desc',$page->firstRow,$page->listRows);
        foreach ($list as $key => $value) {
            $value2=$this->comMod->bangdanHandle($value);
            $data_lsit[]=$value2;
        }
        $this->myApiPrint(200,$data_lsit,$extend_data);
    }

    //榜单帖子详情
    public function getDocDetail(){
        $condition=array();
        $condition['article_id']=$_REQUEST['tid'];
        $condition['article_show']=1;
        $detail=$this->bangdanMod->getBandanTopicDetail($condition);

        
        if(empty($detail['member_id'])){
            $this->myApiPrint(-3,'',array('msg'=>'该文章不存在或没有权限操作'));
        }

        //查询是否喜欢
        $zanstatus=$this->bangdanMod->getTopicSelfZanStatus($_REQUEST['tid'],$this->member_info);
        if($zanstatus){
            $detail['isLike']='1'; //喜欢
        }else{
            $detail['isLike']='0'; //不喜欢
        }
        $extend_data['recommend']=array();
        $recommend=$this->bangdanMod->getRecommend(array('aid'=>$_REQUEST['tid']),'*','id desc',0,6);
        foreach($recommend as $key=>$value){
            $extend_data['recommend'][] = $value;
        }

        //限制用户每天可以投票的数量
        $uid=$this->member_info ? $this->member_info['member_id'] : 0;
        $today_start=strtotime(date('Y-m-d'));    //今天早上凌晨0点
        $today_end=strtotime(date('Y-m-d'))-1+24*3600;    //今天晚上12点
        $condition['uid']=$uid;
        $condition['ip']=get_client_ip();
        $condition['aid']=$_REQUEST['tid'];
        $condition['add_time']=array('between',$today_start.','.$today_end);
        $count = $this->comMod->table(C('DB_PREFIX')."app_view")->where($condition)->count();

        if($_REQUEST['refresh']){
            $this->myApiPrint(200,$detail,$extend_data);  //在本页面刷新不计入浏览数更新
        }else{
            if($count==0){
                $detail['view']=$detail['view']+1;
            }
        }

        if($count==0){
            //用户浏览记录表增加记录
            $uid=$this->member_info ? $this->member_info['member_id'] : 0;
            $data['uid']=$uid;
            $data['ip']=get_client_ip();
            $data['add_time']=NOW_TIME;
            $data['aid']=$_REQUEST['tid'];    //文章id
            $data['reply_uid']=$_REQUEST['reply_uid']; //没用
            $data['type']=$_REQUEST['type'];   //不需要 
            $result=M('app_view')->add($data);
            //浏览数+1
            $cond['article_id']=$_REQUEST['tid'];
            $result=$this->comMod->selfUpdateField('sectiontopic','view',$cond,$operate="+",1);
        }

        
        $this->myApiPrint(200,$detail,$extend_data);
    }

    public function returnUploadType($file_name,$type)
    {
        $extends_data['msg']='上传文件格式错误';
        if(in_array($type, array(20,25))){
            $media_type='video';  //视频
            if(empty($file_name)){
                continue;
            }
            if(!$this->is_video_file($file_name)){
                $this->myApiPrint(4005,'',$extends_data);
            }
        }elseif(in_array($type, array(21,28,33,34,35))){
            $media_type='sound';  //音频
            if(empty($file_name)){
                continue;
            }
                // $extends_data['msg']='tyty'.$file_name;
                // $this->myApiPrint(4005,'',$extends_data);
                // echo json_encode($data);exit;
            if(!$this->is_sound_file($file_name)){
                $this->myApiPrint(4005,'',$extends_data);
            }
        }else{
            $media_type='pic';  //多图
            if(empty($file_name)){
                continue;
            }
            if(!$this->is_photo_file($file_name)){
                $this->myApiPrint(4005,'',$extends_data);
            }
        }
        return $media_type;
    }

    //每天视频、语音、照片和文档获得乾坤币次数是否超过3次判断
    public function if_can_get_coin($uid,$type)
    {
        $addcoin_info_column='';
        if(in_array($type, array(20,25))){
            //视频
            $addcoin_info_column='add_video_coin_info';

        }elseif(in_array($type, array(21,28,33,34,35))){
            //音频
            $addcoin_info_column='add_voice_coin_info';
        }else{
            //多图
            $addcoin_info_column='add_img_coin_info';
        }
        
        $today=date('Y-m-d');
        $memberInfo=$this->comMod->detail('member',array('member_id'=>$uid),$addcoin_info_column);
        $update_exchange_data=array();
        $update_exchange_data['flag']=true;

        if($memberInfo[$addcoin_info_column]!=null && !empty($memberInfo[$addcoin_info_column])){

            $exchange_info=unserialize($memberInfo[$addcoin_info_column]);

            if($exchange_info['date']!=$today){
                $update_exchange_data['date']=$today;
                $update_exchange_data['count']=1;
            }else{
                $update_exchange_data['count']=$exchange_info['count']+1;
                $update_exchange_data['date']=$exchange_info['date'];
            }

            if($exchange_info['count']>=3 && $exchange_info['date']==$today){
                $update_exchange_data['flag']=false;
            }

        }else{
            $update_exchange_data['count']=1;
            $update_exchange_data['date']=$today;
        }
         
        $update_exchange_data['addcoin_info']=$addcoin_info_column;    


        return $update_exchange_data;
    }

}