<?php
/**
 * 非公开接口API
 *  等
 * 需要验证用户身份token
 * @author David
 *
 */
namespace Api\Controller;

use Common\Controller\HxController;
use Think\Page;
use Api\Common;

class ChatController extends HxController{

    //对应表查询的字段列表
    protected $hx_userinfo_column='hx_id,member_name,truename,avatar,member_id';

    public function test(){
        $rs = new \Api\Common\Hxcall();
     // 注册的用户
     //echo $rs->hx_register('qwerasd', 'qazwsx', '福州123' );
     // 给IM用户的添加好友
     // echo $rs->hx_contacts('admin888', 'qwerasd');
     /* 发送文本消息 */
     // echo $rs->hx_send('213123','admin888','dfadsr214wefaedf');
     /* 消息数统计 */
     // echo $rs->hx_msg_count('admin888');
     /* 获取IM用户[单个] */
     // echo $rs->hx_user_info('admin888');
     /* 获取IM用户[批量] */
     echo $rs->hx_user_infos('20');
     /* 删除IM用户[单个] */
     // echo $rs->hx_user_delete('wwwwww');
     /* 修改用户昵称 */
     // echo $rs->hx_user_update_nickname('asaxcfasdd','网络科技');
     /* 重置IM用户密码 */
     // echo $rs->hx_user_update_password('asaxcfasdd','asdad');
     /* 解除IM用户的好友关系 */
     // echo $rs->hx_contacts_delete('admin888', 'qqqqqqqq');
     /* 查看好友 */
     //echo $rs->hx_contacts_user('admin888');
    }

    //获取本地用户信息
    public function get_LocalUserInfo()
    {
        $member_name=$_REQUEST['name'];
        $info=array();
        $where['member_name']  = $member_name;
        $where['hx_id']  = $member_name;
        $where['_logic'] = 'or';
        $map['_complex'] = $where;
        $info=$this->comMod->detail('member',array($map),$this->hx_userinfo_column);

        $info['avatar']=$info['avatar'] ? PicPath.$info['avatar'] : DEFAULT_TOPIC_ICON;
        $this->myApiPrint(200,$info);
    }

    //获取本地用户列表信息
    public function get_LocalUserList() {
        
        if(empty($_REQUEST['name'])){
            $this->myApiPrint(-3,'',array('msg'=>'参数有误'));
        }
        $list=array();
        $members=explode(',', $_REQUEST['name']);
        // $members=array_unique($members); //去除重复name用户
        if(!empty($members)){
            foreach ($members as $key => $value) {
                $where['member_name']  = $value;
                $where['hx_id']  = $value;
                $where['_logic'] = 'or';
                $map['_complex'] = $where;
                $info=$this->comMod->detail('member',array($map),$this->hx_userinfo_column);
                // $info=$this->comMod->detail('member',array('member_name'=>$value),'member_name,truename,avatar,member_id');
                if(!empty($info)){
                    $info['avatar']=$info['avatar'] ? PicPath.$info['avatar'] : DEFAULT_TOPIC_ICON;
                    $info['truename']=$info['truename'] ? $info['truename'] : $info['member_name'];
                    $list[]=$info;
                }else{
                    continue;
                }
            }
        }

// $this->myApiPrint(-3,'',array('msg'=>'参数有误1'.$_REQUEST['name']));
        // $table='member';
        // $cond['member_name'] = array('in',$_REQUEST['name']);
        // $list=$this->comMod->getList($table,$cond,'member_name,truename,avatar,member_id','member_id desc');
        // foreach ($list as $key => $value) {
        //     $value['avatar']=$value['avatar'] ? PicPath.$value['avatar'] : DEFAULT_TOPIC_ICON;
        //     $list[$key]=$value;
        // }
        try {
            $this->myApiPrint(200,$list);
        } catch (Exception $e) {
            $this->myApiPrint(-3,'');
        } 
    }

    //授权注册
    public function register_user()
    {

        $data['username']='2569542';
        $data['password']='111111';
        
        $arrResult=$this->accreditRegister($data);
        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['entities'][0]['uuid']?$arrResult['entities'][0]['uuid']:"";
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //注册环信im失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('');
        }
        
    }

    /**
     * 创建聊天室
     */
    public function create_chatroom() {

        //每个用户只能创建一个聊天室
        // $member_info=$this->comMod->detail('member',array('member_name'=>$_REQUEST['owner']),'chatroom_nums,member_id');
        $where['member_name']  = $_REQUEST['owner'];
        $where['hx_id']  = $_REQUEST['owner'];
        $where['_logic'] = 'or';
        $map['_complex'] = $where;
        $member_info=$this->comMod->detail('member',array($map),'chatroom_nums,member_id');

        if(!empty($member_info) && $member_info['chatroom_nums']>0 ){
            // || !in_array($member_info['member_id'],array(44,45,65,66))
            echo json_encode(array('err_msg'=>'每个用户只能创建并保存一个有效聊天室哦'));exit;
        }

        $data['name']=$_REQUEST['name'];
        $data['description']=$_REQUEST['name'];
        $data['maxusers']=$_REQUEST['maxusers']; //成员最大人数限制（包含群主）
        $data['owner']=$_REQUEST['owner'];
        
        $arrResult=$this->build_chatroom($data); //创建

        // $a=$this->comMod->saveData('member',array('member_id'=>$member_info['member_id']),array('openid'=>$arrResult['data']['id']));


        // echo json_encode(array('err_msg'=>'id:'.$arrResult['data']['id'].'name:'.$_REQUEST['name'].'--maxusers:'.$_REQUEST['maxusers'].'--owner:'.$_REQUEST['owner']));exit;

        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['data']['id']?$arrResult['data']['id']:"";
        if(!empty($result)){
            //更新用户聊天室数量
            // $cond['member_name']=$_REQUEST['owner'];

            $where['member_name']  = $_REQUEST['owner'];
            $where['hx_id']  = $_REQUEST['owner'];
            $where['_logic'] = 'or';
            $map['_complex'] = $where;

            $data['chatroom_nums']=1;

            $result=$this->comMod->saveData('member',$map,$data);  
        }

        echo json_encode($arrResult);exit;
    }

    public function del_chatrooms_more(){
        $room_ids_arr=array();
        $room_ids_arr[0]="257577922515173804";
        $room_ids_arr[1]="259902419368935852";
        $room_ids_arr[2]="259928271615951276";
        $room_ids_arr[3]="260172220867806788";
        $room_ids_arr[4]="260220612117529004";
        $room_ids_arr[5]="261764693935260076";
        $room_ids_arr[6]="262199661413007784";
        $room_ids_arr[7]="263313392989634984";
        $room_ids_arr[8]="265762768286646704";
        $room_ids_arr[9]="268462312120124996";
        foreach ($room_ids_arr as $key => $value) {
            $arrResult=$this->delete_chatroom($value); //删除
            //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
            $result = $arrResult['data']['success'] ? true : false; 
        }
        echo 'eee';
        exit;
    }

    /**
     * 删除聊天室
     */
    public function del_chatroom() {
        $chatroom_id=$_REQUEST['chatroom_id'];
        $arrResult=$this->delete_chatroom($chatroom_id); //删除
        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        
        $result = $arrResult['data']['success'] ? true : false;
        if($result){
            //更新用户聊天室数量
            // $cond['member_name']=$_REQUEST['name'];

            $where['member_name']  = $_REQUEST['name'];
            $where['hx_id']  = $_REQUEST['name'];
            $where['_logic'] = 'or';
            $map['_complex'] = $where;

            $data['chatroom_nums']=0;
            $result=$this->comMod->saveData('member',$map,$data);  
        }

        echo json_encode($arrResult);exit;
    }


    /**
     * 获取群组成员(聊天室也一样)
     *
     * @param
     *          $group_id
     */
    public function get_GroupsUser() {
        $group_id=$_REQUEST['chatroom_id'];
        $arrResult=$this->groupsUser($group_id);
        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['data'];
        if(!isset($arrResult['data'])){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //注册环信im失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('',$arrResult);
        }
    }

    /**
     * 聊天室详情
     */
    public function get_chatroom() {
        $chatroom_id=$_REQUEST['chatroom_id'];
        $arrResult=$this->detail_chatroom($chatroom_id); //创建
        echo json_encode($arrResult);exit;
        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['data'][0]['id']?$arrResult['data'][0]['id']:"";
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('',$arrResult);
        }

    }

    /**
     * 获取所有聊天室
     */
    public function get_allchatroom() {
        $pagenum=$_REQUEST['pagenum'] ? $_REQUEST['pagenum'] : 1;
        $pagesize=$_REQUEST['pagesize'] ? $_REQUEST['pagesize'] : 10000;
        $arrResult=$this->list_chatroom($pagenum,$pagesize); //创建
        echo json_encode($arrResult);exit;
        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['data'][0]['id']?$arrResult['data'][0]['id']:"";
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('',$arrResult);
        }
    }

    
    //获取im用户（批量）
    public function getAllUsers(){
        $arrResult=$this->userLists(100);
        //判断操作是否成功
        $result = $arrResult['entities'][0]['uuid']?$arrResult['entities'][0]['uuid']:"";
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //注册环信im失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('',$arrResult);
        }
    }

    //获取im用户（单个）
    public function getOneUser()
    {

        $arrResult=$this->userDetails('huchao');

        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['entities'][0]['uuid']?$arrResult['entities'][0]['uuid']:"";

        // print_r($arrResult);
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //注册环信im失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('',$arrResult);
        }
        
    }

    //添加好友
    public function methodAddFriend()
    {
        $arrResult=$this->addFriend('huchao', 'huchao1');
        var_dump($arrResult);

        //注册或删除失败时，uuid为空。根据uuid是否有值 判断操作是否成功
        $result = $arrResult['entities'][0]['uuid']?$arrResult['entities'][0]['uuid']:"";
        if(empty($result)){
            $result=array('err_no' =>0,'err_msg'=>"failed");  //注册环信im失败
            $this->myApiPrint('',$result);
        }else{
            $this->myApiPrint('');
        }
    }

}