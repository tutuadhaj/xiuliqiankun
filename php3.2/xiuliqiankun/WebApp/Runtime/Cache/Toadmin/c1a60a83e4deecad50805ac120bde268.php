<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
    <title>管理中心</title>
    <link href="/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
    <script type="text/javascript" SRC="/Public/static/jquery.js"></script>
    <script type="text/javascript" src="/Public/static/jquery.validation.min.js"></script>
    <script type="text/javascript" src="/Public/admin/js/jquery.cookie.js"></script>
    <script type="text/javascript">
        
        //设置按钮 点击展开和收缩（未使用）
        // $(document).ready(function () {
        //     $('span.bar-btn').click(function () {
        //         $('ul.bar-list').toggle('fast');
        //     });
        // });

        //默认显示初始化
        $(document).ready(function(){
            var pagestyle = function() {
                var iframe = $("#workspace");
                var h = $(window).height() - iframe.offset().top;
                var w = $(window).width() - iframe.offset().left;
                if(h < 300) h = 300;
                if(w < 973) w = 973;
                iframe.height(h);
                iframe.width(w);
            }
            pagestyle();
            $(window).resize(pagestyle);
            //turn location
            if($.cookie('now_location_act') != null){
                openItem($.cookie('now_location_op')+','+$.cookie('now_location_act')+','+$.cookie('now_location_nav'));
            }else{
                $('#mainMenu>ul').first().css('display','block');
                //第一次进入后台时，默认定到欢迎界面
                $('#item_base_information').addClass('selected');
                $('#workspace').attr('src','/Toadmin/Setting/aboutus');
            }

            //刷新操作（未使用）
            // $('#iframe_refresh').click(function(){
            //     var fr = document.frames ? document.frames("workspace") : document.getElementById("workspace").contentWindow;;
            //     fr.location.reload();
            // });

        });

        //选择菜单事件
        function openItem(args){
            //cookie
            if($.cookie('admin_id') === null){
                location.href = '/Toadmin/Public/login';
                return false;
            }
            spl = args.split(',');
            op  = spl[0];
            try {
                act = spl[1];
                nav = spl[2];
            }
            catch(ex){}
            if (typeof(act)=='undefined'){var nav = args;}
            $('.actived').removeClass('actived');
            $('#nav_'+nav).parent('li').addClass('actived');

            $('.selected').removeClass('selected');

            //show
            $('#mainMenu ul').css('display','none');
            $('#sort_'+nav).css('display','block');

            if (typeof(act)=='undefined'){
                //顶部菜单事件
                var html = $('#sort_'+nav+'>li').first().html();
                //选中左菜单的第一项
                str = html.match(/openItem\('(.*)'\)/ig);
                arg = str[0].split("'");
                spl = arg[1].split(',');
                op  = spl[0];
                act = spl[1];
                nav = spl[2];
                first_obj = $('#sort_'+nav+'>li').first().children('a');
                $(first_obj).addClass('selected');
                //crumbs
                $('#crumbs').html('<span>'+$('#nav_'+nav+' > span').html()+'</span><span class="arrow">&nbsp;</span><span>'+$(first_obj).text()+'</span>');
            }else{
                //左侧菜单事件
                //location
                $.cookie('now_location_nav',nav);
                $.cookie('now_location_act',act);
                $.cookie('now_location_op',op);
//		$("#item_"+op).addClass('selected');//使用name，不使用ID，因为ID有重复的

                $("a[name='item_"+op+act+"']").addClass('selected');
                //crumbs
                $('#crumbs').html('<span>'+$('#nav_'+nav+' > span').html()+'</span><span class="arrow">&nbsp;</span><span>'+$('#item_'+op).html()+'</span>');
            }
            //设置主页面区内容，控制器Index 方法welcome 
            src = '/Toadmin/'+act+'/'+op;
            $('#workspace').attr('src',src);
        }
        //点击一级类别展开和收缩二级类别
        $(function(){
            bindAdminMenu();
        })
        function bindAdminMenu(){

            $("[nc_type='parentli']").click(function(){
                var key = $(this).attr('dataparam');
                if($(this).find("dd").css("display")=="none"){
                    $("[nc_type='"+key+"']").slideDown("fast");
                    $(this).find('dt').css("background-position","-322px -170px");
                    $(this).find("dd").show();
                }else{
                    $("[nc_type='"+key+"']").slideUp("fast");
                    $(this).find('dt').css("background-position","-483px -170px");
                    $(this).find("dd").hide();
                }
            });
        }
    </script>
</head>

<body style="margin: 0px;" scroll="no">
<table style="width: 100%;" id="frametable" height="100%" width="100%" cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td colspan="2" height="90" class="mainhd"><div class="layout-header"> <!-- Title/Logo - can use text instead of image -->
            <div id="title"><a href="index.php"></a></div>
            <!-- Top navigation -->
            <div id="topnav" class="top-nav">
                <ul>
                    <li class="adminid" title="您好:<?php echo ($admin_name); ?>">您好&nbsp;:&nbsp;<strong><?php echo ($admin_name); ?></strong></li>
                    <li><a href="<?php echo U('Public/logout');?>" title="退出"><span>退出</span></a></li>
                    <!-- <li><a href="<?php echo (C("SiteUrl")); ?>" target="_blank" title="首页"><span>首页</span></a></li> -->
                </ul>
            </div>
            <!-- End of Top navigation -->
            <!-- Main navigation -->
            <nav id="nav" class="main_nav">
                <ul>
                    <li class='actived' onclick="javascript:openItem('setting');"><i class="i01"></i><a href="javascript:void(0);" id='nav_setting' onclick="javascript:openItem('setting');">设置</a></li>
                    <li onclick="javascript:openItem('member');"><i class="i03" ></i><a href="javascript:void(0);" id='nav_member' onclick="javascript:openItem('member');"><span>会员</span></a> </li>
<!--                     <li onclick="javascript:openItem('website')"><i class="i10"></i><a href="javascript:void(0);" id='nav_website' onclick="javascript:openItem('website')">网站</a></li> -->
                    <li onclick="javascript:openItem('APP')"><i class="i10"></i><a href="javascript:void(0);" id='nav_APP' onclick="javascript:openItem('APP')">APP</a></li>
                    <li onclick="javascript:openItem('circle')"><i class="i04"></i><a href="javascript:void(0);" id='nav_circle' onclick="javascript:openItem('circle')">数据</a></li>
                </ul>
            </nav>
        </div></td>
    </tr>
    <tr>
        <td class="menutd" valign="top" width="161"><div id="mainMenu" class="main-menu">
            <ul id='sort_setting' style="display:block;">
                <li><a href="javascript:void(0);" class="selected" id="item_aboutusSetting" name='item_aboutusSetting' onclick="javascript:openItem('aboutus,Setting,setting')">关于我们</a></li>
<!--                 <li><a href="javascript:void(0);"  id="item_indexSeo" name='item_indexSeo' onclick="javascript:openItem('keywords_edit,Setting,setting')">整站关键词屏蔽设置</a></li>
                <li><a href="javascript:void(0);"  id="item_base_informationSetting" name='item_base_informationSetting' onclick="javascript:openItem('base_information,Setting,setting')">站点设置</a></li>
                <li><a href="javascript:void(0);"  id="item_indexSeo" name='item_indexSeo' onclick="javascript:openItem('index,Seo,setting')">SEO设置</a></li>
                <li><a href="javascript:void(0);"  id="item_short_messageSetting" name='item_short_messageSetting' onclick="javascript:openItem('short_message,Setting,setting')">短信设置</a></li>
                <li><a href="javascript:void(0);"  id="item_email_informationSetting" name='item_email_informationSetting' onclick="javascript:openItem('email_information,Setting,setting')">邮箱设置</a></li> -->

                <li><a href="javascript:void(0);"  id="item_admin_listSetting" name='item_admin_listSetting' onclick="javascript:openItem('admin_list,Setting,setting')">系统用户</a></li>
<!--                 <li><a href="javascript:void(0);"  id="item_auth_listSetting" name='item_auth_listSetting' onclick="javascript:openItem('auth_list,Setting,setting')">权限管理</a></li>
                 <li><a href="javascript:void(0);"  id="item_paymentSetting" name='item_paymentSetting' onclick="javascript:openItem('payment,Setting,setting')">支付方式</a></li> -->
                 <li><a href="javascript:void(0);"  id="item_districtSetting" name='item_districtSetting' onclick="javascript:openItem('district,Setting,setting')">区域管理</a></li>
                 <li><a href="javascript:void(0);"  id="item_schoolSetting" name='item_schoolSetting' onclick="javascript:openItem('school,Setting,setting')">学校管理</a></li>
                <!--<li><a href="javascript:void(0);"  id="item_express_listExpress" name='item_express_listExpress' onclick="javascript:openItem('express_list,Express,setting')">快递公司</a></li>-->
                <li><a href="javascript:void(0);"  id="item_indexAdvPosition" name='item_indexAdvPosition' onclick="javascript:openItem('index,AdvPosition,setting')">广告位管理</a></li>
            </ul>

            <ul id='sort_member' style="display:none;">
                <li><a href="javascript:void(0);" id="item_memberMember" name="item_memberMember" onclick="javascript:openItem('member,Member,member')" class="selected">会员管理</a></li>
            </ul>
            <ul id='sort_APP' style="display:none;">

           <!--      <li><a href="javascript:void(0);" class="selected" id="item_article_classArticle" name="item_article_classArticle" onclick="javascript:openItem('article_class,SectionTopic,APP');">栏目分类</a></li> -->

                <li><a href="javascript:void(0);" id="item_topic_listSectionTopic" name="item_topic_listSectionTopic" onclick="javascript:openItem('topic_list,SectionTopic,APP');">帖子管理</a></li>

                <li><a href="javascript:void(0);" id="item_positionClubManager" name="item_positionClubManager" onclick="javascript:openItem('position,ClubManager,APP');">社团管理</a></li>

                <li><a href="javascript:void(0);" id="item_positionHometownManager" name="item_positionHometownManager" onclick="javascript:openItem('position,HometownManager,APP');">同乡会管理</a></li>

<!--                 <li><a href="javascript:void(0);" id="item_commentArticle" name="item_commentArticle" onclick="javascript:openItem('comment,SectionTopic,APP');">论点管理</a></li> -->

                 <li><a href="javascript:void(0);" id="item_positionBillboards" name="item_positionBillboards" onclick="javascript:openItem('position,Billboards,APP');">广告管理</a></li>
                 <li><a href="javascript:void(0);"  id="item_indexSystemMessage" name='item_indexSystemMessage' onclick="javascript:openItem('index,SystemMessage,APP')">系统消息</a></li>
                 <li><a href="javascript:void(0);" id="item_documentSectionTopic" name="item_documentSectionTopic" onclick="javascript:openItem('document,SectionTopic,APP');">系统公告</a></li>
                 
            </ul>
<!--             <ul id='sort_website' style="display:none;">

                <li><a href="javascript:void(0);" class="selected" id="item_article_classArticle" name="item_article_classArticle" onclick="javascript:openItem('article_class,Article,website');">栏目分类</a></li>
                <li><a href="javascript:void(0);" id="item_articleArticle" name="item_articleArticle" onclick="javascript:openItem('article,Article,website');">论题管理</a></li>
                <li><a href="javascript:void(0);" id="item_commentArticle" name="item_commentArticle" onclick="javascript:openItem('comment,Article,website');">论点管理</a></li>
                <li><a href="javascript:void(0);" id="item_indexInviteManage" name="item_indexInviteManage" onclick="javascript:openItem('index,InviteManage,website');">邀请码管理</a></li>
                 <li><a href="javascript:void(0);" id="item_documentArticle" name="item_documentArticle" onclick="javascript:openItem('document,Article,website');">系统文章</a></li>
                 <li><a href="javascript:void(0);" id="item_positionBillboards" name="item_positionBillboards" onclick="javascript:openItem('position,Billboards,website');">广告管理</a></li>
            </ul> -->

            <ul id='sort_circle' style="display:none;">
                <li><a href="javascript:void(0);" class="selected" id="item_cache_clearSetting" name='item_cache_clearSetting' onclick="javascript:openItem('cache_clear,Setting,circle')">清理缓存</a></li>
                <!-- <li><a href="javascript:void(0);" id="item_sitemapTool" name='item_sitemapTool' onclick="javascript:openItem('sitemap,Tool,circle')">站点地图</a></li> -->
                <li><a href="javascript:void(0);" id="item_indexDatabase" name='item_indexDatabase' onclick="javascript:openItem('index,Database,circle')">数据备份</a></li>
            </ul>

        </div>
        </td>
        <td valign="top" width="100%">
            <iframe src="" id="workspace" name="workspace" style="overflow: visible;" frameborder="0" width="100%" height="100%" scrolling="yes" onload="window.parent"></iframe>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>