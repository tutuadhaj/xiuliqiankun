<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title>本地生活</title>
<link href="/xiuliqiankun/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
<script type="text/javascript" src="/xiuliqiankun/Public/static/jquery.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/static/jquery.validation.min.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/admincp.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/jquery.tooltip.js"></script>
<script language="javascript">
var SiteUrl = '<?php echo (C("SiteUrl")); ?>';
var AdminUrl = SiteUrl+'/<?php echo (MODULE_NAME); ?>';
</script>
</head><body>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>系统用户管理</h3>
      <ul class="tab-base">
         <li><a href="<?php echo U('Setting/admin_list');?>"><span>管理</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>添加</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="add_form" method="post" action="<?php echo U('Setting/admin_list',array('op'=>'add'));?>">
    <table class="table tb-type2 nobdb">
      <tbody>
        <tr class="noborder">
          <td colspan="2" class="required"><label class="validation" for="admin_name">账号:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" id="admin_name" name="admin_name" class="txt"></td>
          <td class="vatop tips">登录用户名</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="admin_pwd">密码:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="password" id="admin_pwd" name="admin_pwd" class="txt"></td>
          <td class="vatop tips">登录密码</td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label class="validation" for="admin_repwd">确认密码:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="password" id="admin_repwd" name="admin_repwd" class="txt"></td>
          <td class="vatop tips">再次输入登录密码</td>
        </tr> 
		<tr>
          <td colspan="2" class="required"><label for="admin_auth">用户权限:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform" colspan="2">
          <?php if(is_array($admin_auth)): $i = 0; $__LIST__ = $admin_auth;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$at): $mod = ($i % 2 );++$i;?><input name="admin_auth[]" type="checkbox" value="<?php echo ($at['a_id']); ?>" checked="checked" /><?php echo ($at['a_title']); ?> &nbsp;&nbsp;<?php endforeach; endif; else: echo "" ;endif; ?>
          </td>
        </tr>        
               
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="2"><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span>保存</span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
//按钮先执行验证再提交表
$(function(){$("#submitBtn").click(function(){
    if($("#add_form").valid()){
     $("#add_form").submit();
	}
	});
});
$(document).ready(function(){
	$("#add_form").validate({
		errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            admin_name : {
                required : true,
				minlength: 3,
				maxlength: 20,
				remote	: {
                    url : AdminUrl+'?c=Setting&a=check_admin_name',
                    type:'get',
                    data:{
                    	admin_name : function(){
                            return $('#admin_name').val();
                        }
                    }
                }
            },
            admin_pwd : {
                required : true,
				minlength: 6,
				maxlength: 20
            },
			admin_repwd:{
				required: true,	
				equalTo:'#admin_pwd'
			}			
        },
        messages : {
            admin_name : {
                required : '请输入账号',
				minlength: '账号长度为3-20个字符',
				maxlength: '账号长度为3-20个字符',
				remote	 : '此账号已存在'
            },
            admin_pwd : {
                required : '请输入密码',
				minlength: '密码长度为6-20个字符',
				maxlength: '密码长度为6-20个字符'
            },
			admin_repwd:{
				required: '请再次输入密码',
				equalTo:'两次输入的密码不一致'
			}
        }
	});
});
</script> 
</body></html>