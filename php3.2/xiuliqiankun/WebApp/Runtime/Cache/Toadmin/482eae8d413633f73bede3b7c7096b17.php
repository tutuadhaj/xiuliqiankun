<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title>本地生活</title>
<link href="/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
<script type="text/javascript" src="/Public/static/jquery.js"></script>
<script type="text/javascript" src="/Public/static/jquery.validation.min.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/Public/admin/js/admincp.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.tooltip.js"></script>
<script language="javascript">
var SiteUrl = '<?php echo (C("SiteUrl")); ?>';
var AdminUrl = SiteUrl+'/<?php echo (MODULE_NAME); ?>';
</script>
</head><body>
<div class="page">
    <div class="fixed-bar">
        <div class="item-title">
            <h3>广告位管理</h3>
            <?php echo ($top_link); ?>
        </div>
    </div>
    <div class="fixed-empty"></div>
    <form method="post" enctype="multipart/form-data" name="form1">
        <table class="table tb-type2">
            <tbody>
            <tr class="noborder">
                <td colspan="2" class="required"><label for="name">名称:</label></td>
            </tr>
            <tr class="noborder">
                <td class="vatop rowform"><input id="name" name="name" value="" class="txt" type="text" /></td>
                <td class="vatop tips"><span class="vatop rowform">名称</span></td>
            </tr>

            <tr class="noborder">
                <td colspan="2" class="required"><label for="ap_code">位置代码:</label></td>
            </tr>
            <tr class="noborder">
                <td class="vatop rowform"><input id="ap_code" name="ap_code" value="" class="txt" type="text" /></td>
                <td class="vatop tips"><span class="vatop rowform">位置代码</span></td>
            </tr>


            </tbody>
            <tfoot>
            <tr class="tfoot">
                <td colspan="2" ><a href="JavaScript:void(0);" class="btn" onclick="document.form1.submit()"><span>保存</span></a></td>
            </tr>
            </tfoot>
        </table>
    </form>
</div>
<script language="javascript">

</script>
</body></html>