<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title>本地生活</title>
<link href="/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
<script type="text/javascript" src="/Public/static/jquery.js"></script>
<script type="text/javascript" src="/Public/static/jquery.validation.min.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/Public/admin/js/admincp.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.tooltip.js"></script>
<script language="javascript">
var SiteUrl = '<?php echo (C("SiteUrl")); ?>';
var AdminUrl = SiteUrl+'/<?php echo (MODULE_NAME); ?>';
</script>
</head><body>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>广告位管理</h3>
      <?php echo ($top_link); ?>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form method="get" name="formSearch">
    <table class="tb-type1 noborder search">
      <tbody>
        <tr>
          <th>名称</th>
          <td><input type="text" value="<?php echo ($search['name']); ?>" name="name" class="txt" ></td>
          <td><a href="javascript:document.formSearch.submit();" class="btn-search tooltip" title="查询">&nbsp;</a></td>
        </tr>
      </tbody>
    </table>
  </form>
  <!-- 操作说明 -->
  <table class="table tb-type2" id="prompt">
    <tbody>
      <tr class="space odd">
        <th colspan="12" class="nobg"><div class="title">
            <h5>操作提示</h5>
            <span class="arrow"></span></div></th>
      </tr>
      <tr>
        <td>
		  <ul>
            <li></li>
          </ul>
		</td>
      </tr>
    </tbody>
  </table>
  <form id="list_form" method='post' action="<?php echo U('AdvPosition/del');?>">
    
    <table class="table tb-type2">
      <thead>
        <tr class="space">
          <th colspan="15" class="nobg">列表</th>
        </tr>
        <tr class="thead">
          <!-- <th class="w48"></th> -->
          <th class="w48">ID</th>
          <th class="w200">名称</th>
		  <th class="w200 align-center">操作</th>
        </tr>
      </thead>
      <tbody>
        <?php if(!empty($list)): if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$val): $mod = ($i % 2 );++$i;?><tr class="hover edit">
          <!-- <td><input name='del_id[]' type="checkbox" value="<?php echo ($val['id']); ?>" class="checkitem"></td> -->
          <td><?php echo ($val['id']); ?></td>
    		  <td><?php echo ($val['name']); ?></td>
    		  <td class='align-center'>
            <a class="barbecue" href="<?php echo U('AdvPosition/edit',array('id'=>$val['id']));?>">编辑</a>
      		  <!-- <a href='javascript:if(confirm("确定要删除？"))window.location ="<?php echo U('AdvPosition/del',array('id'=>$val['id']));?>";'>删除</a> -->
          </td>
        </tr><?php endforeach; endif; else: echo "" ;endif; ?>
        <?php else: ?>
        <tr class="no_data">
          <td colspan="10">暂无记录</td>
        </tr><?php endif; ?>
      </tbody>
      <?php if(!empty($list)): ?><tfoot>
        <tr class="tfoot">
          <!-- <td><input type="checkbox" class="checkall" id="checkall_1"></td> -->
          <td id="batchAction" colspan="15">
             <span class="all_checkbox">
           <!--  <label for="checkall_1">全选</label> -->
            </span>&nbsp;&nbsp; 
            <!-- <a href="JavaScript:void(0);" class="btn" onclick="if(confirm('你确定要删除？')){$('#list_form').submit();}"><span>删除</span></a> -->

            <div class="pagination"><?php echo ($page_show); ?></div></td>
        </tr>
      </tfoot><?php endif; ?>
    </table>
  </form>
</div>
<script type="text/javascript" src="/Public/static/common.js"></script>  
<script language="javascript" src="/Public/static/dialog/dialog.js"  id="dialog_js" charset="utf-8"> </script> 
<script type="text/javascript" src="/Public/static/jquery-ui/jquery.ui.js"></script> 
<script type="text/javascript" src="/Public/static/jquery-ui/i18n/zh-CN.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="/Public/static/jquery-ui/themes/ui-lightness/jquery.ui.css"  />
<link href="/Public/static/dialog/custom.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
$('a[nc_type="dialog"]').click(function(){
	var id = $(this).attr('dialog_id');
	var title = $(this).attr('dialog_title') ? $(this).attr('dialog_title') : '';
	var url = $(this).attr('uri');
	var width = $(this).attr('dialog_width');
	CUR_DIALOG = ajax_form(id, title, url, width,0);
	return false;
});
</script>
</body></html>