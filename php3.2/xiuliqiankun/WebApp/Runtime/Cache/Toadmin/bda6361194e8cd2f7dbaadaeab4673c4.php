<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title>本地生活</title>
<link href="/xiuliqiankun/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
<script type="text/javascript" src="/xiuliqiankun/Public/static/jquery.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/static/jquery.validation.min.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/admincp.js"></script>
<script type="text/javascript" src="/xiuliqiankun/Public/admin/js/jquery.tooltip.js"></script>
<script language="javascript">
var SiteUrl = '<?php echo (C("SiteUrl")); ?>';
var AdminUrl = SiteUrl+'/<?php echo (MODULE_NAME); ?>';
</script>
</head><body>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>地区设置</h3>
      <ul class="tab-base">
        <li><a href="<?php echo U('Setting/district');?>"><span>地区管理</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>添加地区</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
    
  <form id="c_form" method="post" action="<?php echo U('Setting/add_district');?>">
   <input type="hidden" name="pid" value="<?php echo ($pid); ?>">
    <table class="table tb-type2 nobdb">
      <tbody>
 
         <tr>
          <td colspan="2" class="required"><label class="validation">地区首字母:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" name="first_letter" id="first_letter" class="txt"></td>
          <td class="vatop tips"></td>
        </tr>
        
        <tr>
          <td colspan="2" class="required"><label class="validation">地区名称:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="" name="c_name" id="c_name" class="txt"></td>
          <td class="vatop tips"></td>
        </tr>
            
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span>保存</span></a></td>
        </tr>
      </tfoot>
    </table>

  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){

	$("#submitBtn").click(function(){
		if($("#c_form").valid()){
			$("#c_form").submit();
		}
	});
	
	$('#c_form').validate({
        errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            first_letter : {
                required : true
            },
            c_name : {
                required   : true
            }
        },
        messages : {
            first_letter : {
                required : '请收入首字母'
			},
            c_name  : {
                required   : '请输入地区名称'
            }
        }
    });
	
});
</script>

</body></html>