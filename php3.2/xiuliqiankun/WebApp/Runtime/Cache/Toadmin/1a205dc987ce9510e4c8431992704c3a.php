<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html;" charset="<?php echo CHARSET;?>">
<title>本地生活</title>
<link href="/Public/admin/css/skin_0.css" rel="stylesheet" type="text/css" id="cssfile"/>
<script type="text/javascript" src="/Public/static/jquery.js"></script>
<script type="text/javascript" src="/Public/static/jquery.validation.min.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.cookie.js"></script>
<script type="text/javascript" src="/Public/admin/js/admincp.js"></script>
<script type="text/javascript" src="/Public/admin/js/jquery.tooltip.js"></script>
<script language="javascript">
var SiteUrl = '<?php echo (C("SiteUrl")); ?>';
var AdminUrl = SiteUrl+'/<?php echo (MODULE_NAME); ?>';
</script>
</head><body>
<div class="page">
  <div class="fixed-bar">
    <div class="item-title">
      <h3>系统公告</h3>
      <ul class="tab-base">
        <li><a href="<?php echo U('Article/document');?>"><span>管理</span></a></li>
        <li><a href="JavaScript:void(0);" class="current"><span>编辑</span></a></li>
      </ul>
    </div>
  </div>
  <div class="fixed-empty"></div>
  <form id="doc_form" method="post">
    <input type="hidden" name="doc_id" value="<?php echo ($vo['doc_id']); ?>" />
    <table class="table tb-type2 nobdb">
      <tbody>
        <tr>
          <td colspan="2" class="required"><label class="validation">标题: </label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="<?php echo ($vo['doc_title']); ?>" name="doc_title" id="doc_title" class="infoTableInput"></td>
          <td class="vatop tips"></td>
        </tr>
        <tr>
          <td colspan="2" class="required"><label>关键词: </label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><input type="text" value="<?php echo ($vo['doc_key']); ?>" name="doc_key" id="doc_key" class="txt"></td>
          <td class="vatop tips"></td>
        </tr>
         <tr class="noborder">
          <td colspan="2" class="required"><label>描述信息:</label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><textarea id="doc_desc" class="tarea" rows="3" name="doc_desc"><?php echo ($vo['doc_desc']); ?></textarea></td>
          <td class="vatop tips"></td>
        </tr>                         
        
        <tr>
          <td colspan="2" class="required"><label class="validation">内容: </label></td>
        </tr>
        <tr class="noborder">
          <td class="vatop rowform"><?php kindEditor('doc_content',$vo['doc_content']);?></td>
          <td class="vatop tips"></td>
        </tr>
      </tbody>
      <tfoot>
        <tr class="tfoot">
          <td colspan="15" ><a href="JavaScript:void(0);" class="btn" id="submitBtn"><span>提交</span></a></td>
        </tr>
      </tfoot>
    </table>
  </form>
</div>
<script>
//按钮先执行验证再提交表单
$(function(){$("#submitBtn").click(function(){
    if($("#doc_form").valid()){
     $("#doc_form").submit();
	}
	});
});
$(document).ready(function(){
	$('#doc_form').validate({
        errorPlacement: function(error, element){
			error.appendTo(element.parent().parent().prev().find('td:first'));
        },
        success: function(label){
            label.addClass('valid');
        },
        rules : {
            doc_title : {
                required   : true
            },
			doc_content : {
                required   : true
            }
        },
        messages : {
            doc_title : {
                required   : '请输入标题'
            },
			doc_content : {
                required   : '请输入内容'
            }
        }
    });

});
</script>
</body></html>