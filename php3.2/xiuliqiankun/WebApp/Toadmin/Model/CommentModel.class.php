<?php
namespace Toadmin\Model;
use Think\Model\RelationModel;
class CommentModel extends RelationModel{
	protected $_link = array(
			'Member' => array(
					'mapping_type'  => self::BELONGS_TO,
					'class_name'    => 'Member',
					'foreign_key'   => 'member_id',
					'mapping_name'  => 'Member',
					'mapping_fields'=> '',  //设置查询字段
			),
			'Article' => array(
					'mapping_type'  => self::BELONGS_TO,
					'class_name'    => 'Article',
					'foreign_key'   => 'article_id',
					'mapping_name'  => 'Article',
					'mapping_fields'=> '',  //设置查询字段
			)
	);
}