<?php
/**
 * 文章模型
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Model;
use Think\Model\RelationModel;

class MemberModel extends RelationModel{
	protected $_link = array(
		'ArticleClass' => array(
				'mapping_type' => self::MANY_TO_MANY,
				'class_name' => 'ArticleClass',
				'mapping_name' => 'ArticleClass', //不添加此参数配置，默认和class_name取一样，需要保证不能和当前模型中的字段名相同，导致字段冲突
				'foreign_key' => 'member_id',
				'relation_foreign_key' => 'ac_id',
				'mapping_order' => 'ac_sort',
		),
	); 	
}
