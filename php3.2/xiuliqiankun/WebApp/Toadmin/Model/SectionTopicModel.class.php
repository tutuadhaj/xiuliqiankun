<?php
/**
 *  板块帖子文章模型
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */

namespace Toadmin\Model;
use Think\Model;

class SectionTopicModel extends Model{
    	
    public function getTopicRelSection($condition = array(), $fields = '*', $order = '', $offset = '', $length = ''){

    	$data=$this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    	foreach ($data as $key => $value) {
    		$section=$this->table(C('DB_PREFIX').'section_class')->where(array('ac_id'=>$value['ac_id']))->find();
            $member=$this->table(C('DB_PREFIX').'member')->field('member_name,truename')->where(array('member_id'=>$value['member_id']))->find();
    		$value['Section']=$section;
            $value['member_name']=$member['member_name'];
            $value['truename']=$member['truename'];
    		$list[]=$value;
    	}

    	return $list;

    }	

    /**
     * 取得总数
     * @param unknown $condition
     */
    public function getTopicCount($condition = array()) {
        return $this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->count();
    }

    /**
     * 取得列表
     * @param unknown $condition
     * @param string $pagesize
     * @param string $fields
     * @param string $order
     */
    public function getTopicList($condition = array(), $fields = '*', $order = '', $offset = '', $length = '') {
        return $this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->field($fields)->limit($offset,$length)->order($order)->select();
    }

    /**
     * 添加
     * @param array $data
     */
    public function addTopic($data) {
        return $this->table(C('DB_PREFIX').'sectiontopic')->insert($data);
    }

    /**
     * 编辑
     * @param unknown $data
     * @param unknown $condition
     */
    public function editTopic($data,$condition = array()) {
        return $this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->update($data);
    }

    /**
     * 取得单条
     * @param unknown $condition
     * @param string $fields
     */
    public function getTopicInfo($condition = array(), $fields = '*') {
        return $this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->field($fields)->find();
    }

    /**
     * 删除
     * @param unknown $condition
     */
    public function delTopic($condition) {
        return $this->table(C('DB_PREFIX').'sectiontopic')->where($condition)->delete();
    }
}

