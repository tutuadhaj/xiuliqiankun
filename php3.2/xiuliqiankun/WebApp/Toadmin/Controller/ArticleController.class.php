<?php
/**
 * 文章
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class ArticleController extends GlobalController {
	public function _initialize() 
	{
        parent::_initialize();
		$this->art = D('Article');
		$this->art_class = M('ArticleClass');
		$this->commentMod = D('Comment');
	}	
	//分类管理
	public function article_class()
	{	
		if(IS_POST) //删除
		{
			if (!empty($_POST['check_ac_id']))
			{
				if (is_array($_POST['check_ac_id']))
				{
					foreach ($_POST['check_ac_id'] as $ac_id)
					{ 
						$this->art_class->where('ac_id='.$ac_id)->delete(); 
						$this->art_class->where('ac_parent_id='.$ac_id)->delete(); 
					}
					$this->success("操作成功",U('article_class'));  	
					exit;						
				}
			}else {
				$this->error("请选择要操作的对象"); 	
			}				
		}else{			
			$list = $this->art_class->where('ac_parent_id=0')->order('ac_sort asc')->select();
			if(is_array($list) && !empty($list))
			{
				foreach($list as $key=>$lt)
				{
					$sub = $this->art_class->where('ac_parent_id='.$lt['ac_id'])->order('ac_sort asc')->select();
					$list[$key]['sub'] = $sub;	
				}	
			}
			$this->assign('list',$list);
			$this->display('articleclass_list');
		}
	}
	//分类编辑
	public function article_class_edit()
	{
		if(IS_POST)
		{
			$ac_id = intval($_POST['ac_id']);
			$data = array();
			$data['ac_parent_id'] = intval($_POST['ac_parent_id']);
			$data['ac_name'] = trim($_POST['ac_name']);
			$data['ac_title'] = str_rp(trim($_POST['ac_title']));
			$data['ac_key'] = str_rp(trim($_POST['ac_key']));
			$data['ac_desc'] = str_rp(trim($_POST['ac_desc']));			
			$data['ac_sort'] = intval($_POST['ac_sort']);
            $data['is_free'] = intval($_POST['is_free']);
			$num = $this->art_class->where('ac_name=\''.$data['ac_name'].'\' and ac_id<>'.$ac_id)->count();
			if($num > 0)
			{
				$this->error("已存在相同的名称",U('article_class_edit',array('ac_id'=>$ac_id))); 	
			}
			$arc_img = 'ac_'.NOW_TIME;
			//图片上传
			if(!empty($_FILES['ac_pic']['size']))
			{
				$param = array('savePath'=>'artic/','subName'=>'','files'=>$_FILES['ac_pic'],'saveName'=>$arc_img,'saveExt'=>'');
				$up_return = upload_one($param);
				if($up_return == 'error')
				{
					$this->error('图片上传失败');
					exit;
				}else{
					$data['ac_pic'] = $up_return;
					//删除图片
					$ac_pic = $this->art_class->where('ac_id='.$ac_id)->getField('ac_pic');
					if($ac_pic)
					{
						@unlink(BasePath.'/Uploads/'.$ac_pic);
					}
				}
			}
			$this->art_class->where('ac_id='.$ac_id)->save($data);
			$this->success("操作成功",U('article_class'));  	
			exit;			 			
		}else{
			$ac_id = intval($_GET['ac_id']);
			$vo = $this->art_class->where('ac_id='.$ac_id)->find();
			$this->ac_list = $this->art_class->where('ac_parent_id=0')->select();
			$this->assign('vo',$vo);
			$this->display('articleclass_edit');	
		}
	}
	//分类添加
	public function article_class_add()
	{
		if(IS_POST)
		{
			$data = array();
			$data['ac_parent_id'] = intval($_POST['ac_parent_id']);
			$data['ac_name'] = str_rp(trim($_POST['ac_name']));
			$data['ac_title'] = str_rp(trim($_POST['ac_title']));
			$data['ac_key'] = str_rp(trim($_POST['ac_key']));
			$data['ac_desc'] = str_rp(trim($_POST['ac_desc']));
			$data['ac_sort'] = intval($_POST['ac_sort']);
            $data['is_free'] = intval($_POST['is_free']);

			$num = $this->art_class->where('ac_name=\''.$data['ac_name'].'\'')->count();
			if($num > 0)
			{
				$this->error("已存在相同的名称",U('article_class_add'));
			}
			$arc_img = 'ac_'.NOW_TIME;
			//图片上传
			if(!empty($_FILES['ac_pic']['size']))
			{
				$param = array('savePath'=>'artic/','subName'=>'','files'=>$_FILES['ac_pic'],'saveName'=>$arc_img,'saveExt'=>'');
				$up_return = upload_one($param);
				if($up_return == 'error')
				{
					$this->error('图片上传失败');
					exit;
				}else{
					$data['ac_pic'] = $up_return;
				}
			}
			$return = $this->art_class->add($data);
			if($return)
			{
				$this->success("操作成功",U('article_class'));
				exit;
			}
		}else{
			$this->ac_list = $this->art_class->where('ac_parent_id=0')->select();
			$this->display('articleclass_edit');
		}
	}
	//文章管理
	public function article()
	{
		if(IS_POST)
		{
			//删除处理
			if (is_array($_POST['del_id']) && !empty($_POST['del_id']))
			{
				foreach ($_POST['del_id'] as $article_id)
				{
					//删除图片
					$article_pic = $this->art->where('article_id='.$article_id)->getField('article_pic');
					if($article_pic)
					{
						@unlink(BasePath.'/Uploads/'.$article_pic);						
					}
					$this->art->where('article_id='.$article_id)->delete(); 
				}
				$this->success("操作成功",U('article'));  	
				exit;
			}else {
				$this->error("请选择要操作的对象"); 	
			}				
		}
		$map = array();	
		if(trim($_GET['article_title']))$map['article_title'] = array('like','%'.trim($_GET['article_title']).'%');
		if(intval($_GET['ac_id']))$map['ac_id'] = array('eq',intval($_GET['ac_id']));
		
		$totalRows = $this->art->where($map)->count();
		$page = new Page($totalRows,10);	
		$list = $this->art->where($map)->relation('ArticleClass')->limit($page->firstRow.','.$page->listRows)->order('article_sort desc')->select();
			
		$ac_list = getArticleClassList(2);
		if (is_array($ac_list)){
			foreach ($ac_list as $k => $v){
				$ac_list[$k]['ac_name'] = str_repeat("&nbsp;",$v['deep']*2).'├ '.$v['ac_name'];
			}
		}		
		$this->assign('ac_list', $ac_list);	
				
		$this->assign('list',$list);
		$this->assign('search',$_GET);	
		$this->assign('show_page',$page->show());
		$this->display('article_index');				
	}
	//文章添加
	public function article_add()
	{
		if(IS_POST)
		{
			$data = array();
			$data['article_title'] = str_rp(trim($_POST['article_title']));
			$data['ac_id'] = intval($_POST['ac_id']);
			$data['article_key'] = str_rp(trim($_POST['article_key']));
			$data['article_desc'] = str_rp(trim($_POST['article_desc']));
			$data['article_url'] = str_rp(trim($_POST['article_url']));
			$data['article_show'] = intval($_POST['article_show']);
			$data['article_sort'] = intval($_POST['article_sort']);
			$data['article_city_id'] = intval($_POST['article_city_id']);
			$data['article_content'] = str_replace('\'','&#39;',$_POST['article_content']);
			$data['article_time'] = NOW_TIME;
            $arc_img = 'g_'.$data['article_time'];
					
			//图片上传
			if(!empty($_FILES['article_pic']['name']))
			{
				$param = array('savePath'=>'artic/','subName'=>'','files'=>$_FILES['article_pic'],'saveName'=>$arc_img,'saveExt'=>'');				
				$up_return = upload_one($param);
				if($up_return == 'error')
				{
					$this->error('图片上传失败');
					exit;	
				}else{
					$data['article_pic'] = $up_return;	
				}					
			}	
						
			$article_id = $this->art->add($data);
			if($article_id)
			{	
									 
			 	$this->success('操作成功', U('article'));
				exit;		
			}else{
				 $this->error('操作失败');
			}			
		}else{
			$ac_list = getArticleClassList(2);
			if (is_array($ac_list)){
				foreach ($ac_list as $k => $v){
					$ac_list[$k]['ac_name'] = str_repeat("&nbsp;",$v['deep']*2).'├ '.$v['ac_name'];
				}
			}		
			//常用城市
			$this->city_list = D('District')->where('usetype=1')->order('d_sort desc')->select();
			$this->assign('ac_list', $ac_list);	
			$this->assign('ac_list',$ac_list);	
			$this->display('article_edit');		
		}
	}
	//文章编辑
	public function article_edit()
	{
		if(IS_POST)
		{
			$article_id = intval($_POST['article_id']);
			$data = array();
			$data['article_title'] = str_rp(trim($_POST['article_title']));
			$data['ac_id'] = intval($_POST['ac_id']);
			$data['article_key'] = str_rp(trim($_POST['article_key']));
			$data['article_desc'] = str_rp(trim($_POST['article_desc']));
			$data['article_url'] = str_rp(trim($_POST['article_url']));
			$data['article_show'] = intval($_POST['article_show']);
			$data['article_sort'] = intval($_POST['article_sort']);
			$data['article_city_id'] = intval($_POST['article_city_id']);
			$data['member_id'] = intval($_POST['member_id']);
			$data['article_content'] = str_replace('\'','&#39;',$_POST['article_content']);
			$data['article_time'] = NOW_TIME;
            $arc_img = 'g_'.$data['article_time'];
					
			//图片上传
			if(!empty($_FILES['article_pic']['name']))
			{
				$param = array('savePath'=>'artic/','subName'=>'','files'=>$_FILES['article_pic'],'saveName'=>$arc_img,'saveExt'=>'');				
				$up_return = upload_one($param);
				if($up_return == 'error')
				{
					$this->error('图片上传失败');
					exit;	
				}else{
					$data['article_pic'] = $up_return;	
				}					
			}				
			
			$this->art->where('article_id='.$article_id)->save($data);
			$this->success('操作成功', U('article'));
			exit;					
		}else{
			$article_id = intval($_GET['article_id']);
			if($article_id)
			{
				$vo = $this->art->where('article_id='.$article_id)->find();
				$ac_list = $this->art_class->order('ac_sort asc')->select();
				//常用城市
				$this->city_list = D('District')->where('usetype=1')->order('d_sort desc')->select();
				//会员列表
				$this->member_list = D('Member')->where(array())->order('register_time desc')->select();
				$this->assign('vo',$vo);
				$this->assign('ac_list',$ac_list);	
				$this->display('article_edit');					
			}
		}
	}
	//评论
	public function comment(){
		if(IS_POST)
		{
			//删除处理
			if (is_array($_POST['del_id']) && !empty($_POST['del_id']))
			{
				foreach ($_POST['del_id'] as $article_id)
				{
					$this->commentMod->where('id='.$article_id)->delete();
				}
				$this->success("操作成功",U('comment'));
				exit;
			}else {
				$this->error("请选择要操作的对象");
			}
		}elseif (IS_GET){
			$map = array();
			if(trim($_GET['article_title']))$map['content'] = array('like','%'.trim($_GET['article_title']).'%');
			if(intval($_GET['article_id']))$map['article_id'] = array('eq',intval($_GET['article_id']));
			
			$totalRows = $this->commentMod->where($map)->count();
			$page = new Page($totalRows,10);
			$list = $this->commentMod->where($map)->relation(true)->limit($page->firstRow.','.$page->listRows)->order('addtime desc')->select();
            foreach($list as $key=>$value){
                $repoort_list=unserialize($value['report_records']);
                $value['report_count']=!empty($repoort_list) ? count($repoort_list) : 0;
                $list[$key]=$value;
            }
			/*
			//文章所属分类
			$ac_list = getArticleClassList(2);
			if (is_array($ac_list)){
				foreach ($ac_list as $k => $v){
					$ac_list[$k]['ac_name'] = str_repeat("&nbsp;",$v['deep']*2).'├ '.$v['ac_name'];
				}
			}
			$this->assign('ac_list', $ac_list);
			*/
			$article_list = $this->art->where(array())->order('article_sort desc,article_time desc')->select();
			$this->assign('article_list',$article_list);
			
			$this->assign('list',$list);
			$this->assign('search',$_GET);
			$this->assign('show_page',$page->show());
			$this->display();
		}
	}

    //某条评论的举报列表数据
    public function report_list_of_comment(){
        if(IS_POST)
        {
            //删除处理
//            if (is_array($_POST['del_id']) && !empty($_POST['del_id']))
//            {
//                foreach ($_POST['del_id'] as $article_id)
//                {
//                    $this->commentMod->where('id='.$article_id)->delete();
//                }
//                $this->success("操作成功",U('comment'));
//                exit;
//            }else {
//                $this->error("请选择要操作的对象");
//            }
        }elseif (IS_GET){
            $map = array();
            $map['id'] = intval($_GET['id']);
            $info = M('comment')->where($map)->find();

//            $totalRows = $this->commentMod->where($map)->count();
//            $page = new Page($totalRows,10);
//            $list = $this->commentMod->where($map)->limit($page->firstRow.','.$page->listRows)->order('addtime desc')->select();
            $list=unserialize($info['report_records']);

            $this->assign('list',$list);
//            $this->assign('search',$_GET);
//            $this->assign('show_page',$page->show());
            $this->display();
        }
    }

    //编辑评论
	public function curdComment(){
		if (IS_POST) {
			$data['agree_num'] = intval($_POST['agree_num']);
			$data['against_num'] = intval($_POST['against_num']);
			$data['content'] = str_rp(trim($_POST['content']));
			$res = $this->commentMod->where(array('id'=>intval($_POST['id'])))->save($data);
			if ($res) {
				$this->success('编辑成功');
			}else {
				$this->error('编辑失败');
			}
		}elseif (IS_GET) {
			$where['id'] = intval($_GET['id']);
			$info = $this->commentMod->where($where)->find();
			$info['member'] = M('Member')->where(array('member_id'=>$info['member_id']))->find();
			$info['reply_member'] = M('Member')->where(array('member_id'=>$info['reply_member_id']))->find();
			$info['article'] = $this->art->where(array('article_id'=>$info['article_id']))->find();
			$info['c_content'] = $this->commentMod->where(array('id'=>$info['c_pid']))->getField('content');
			$this->assign('info',$info);
			$this->display();
		}
	}
	//系统文章
	public function document()
	{
		$list = M('Document')->select();
		$this->assign('list',$list);
		$this->display('document_index');			
	}
	//系统文章编辑
	public function document_edit()
	{
		$Document = M('Document');
		if(IS_POST)
		{
			$doc_id = intval($_POST['doc_id']);	
			if($doc_id)
			{
				$data = array();
				$data['doc_title'] = str_rp(trim($_POST['doc_title']));	
				$data['doc_key'] = str_rp(trim($_POST['doc_key']));	
				$data['doc_desc'] = str_rp(trim($_POST['doc_desc']));	
				$data['doc_content'] = str_replace('\'','&#39;',$_POST['doc_content']);
				$data['doc_time'] = NOW_TIME;
				$Document->where('doc_id='.$doc_id)->save($data); 
			 	$this->success('操作成功', U('document'));
				exit;				
			}			
		}else{
			$doc_id = intval($_GET['doc_id']);	
			$vo = $Document->where('doc_id='.$doc_id)->find();
			$this->assign('vo',$vo);
			$this->display('document_edit');			
		}
	}
		
	//异步处理 在线编辑
	public function ajax()
	{
		switch ($_GET['branch'])
		{
			case 'article_class_sort':
			case 'article_class_name':
			    $data_array=array();
				if(trim($_GET['column'])=='ac_sort')
				{
					$data_array[trim($_GET['column'])] = intval($_GET['value']);
				}else{
					$data_array[trim($_GET['column'])] = trim($_GET['value']);
				}
				$this->art_class->where('ac_id='.intval($_GET['id']))->save($data_array);
			    echo 'true';exit;
				break;								
		}			
	}

	
	//论题Excel导出
	public function article_port()
	{
 		Vendor('phpExcel.PHPExcel');
		Vendor('phpExcel.Writer.PHPExcel_Writer_Excel5');
		$excel = new \PHPExcel();
		$excel->getActiveSheet()->setTitle('论题下载');
		$header = array('A','B','C','D','E','F');
		$name = array('排序','标题','分类','显示','添加时间','内容');
		for($i=0;$i<count($name);$i++){
			$excel->getActiveSheet()->setCellValue("$header[$i]1","$name[$i]");
			$excel->getActiveSheet()->getColumnDimension($header[$i])->setWidth(40);
		} 
		
		$article = M('article')->field('ac_id,article_title,article_sort,article_content,article_time,article_show')->select();
		foreach ($article as $k=>$v){
			$where['ac_id'] = $v['ac_id'];
			$ac = M('article_class')->where($where)->field('ac_name')->find();
			$article[$k]['ac_name'] = $ac['ac_name'];
		}
		
		$data = array();
		$i = 0;
		foreach ($article as $k => $v){
			$time = date('Y-m-d',$article[$k]['article_time']);
			switch ($article[$k]['article_show']){
				case 0:$article[$k]['article_show'] = '不显示';break;
				case 1:$article[$k]['article_show'] = '已显示';break;
			}
			$data[$i]= array($v['article_sort'],$v['article_title'],$v['ac_name'],$article[$k]['article_show'],$time,$v['article_content']);
			$i++;
		}
		
		$count = count($data)+1;
		for ($i = 2;$i <= $count;$i++) {
			$j = 0;
			foreach ($data[$i - 2] as $k=>$v) {
				$excel->getActiveSheet()->setCellValue("$header[$j]$i","$v");
				$j++;
			}
		}
			
 		$write = new \PHPExcel_Writer_Excel5($excel);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");;
		header('Content-Disposition:attachment;filename="'.$event_title.'-论题下载.xls"');
		header("Content-Transfer-Encoding:binary");
		$write->save('php://output'); 
	}
	
	//论点导出
	public function comment_port()
	{
		Vendor('phpExcel.PHPExcel');
		Vendor('phpExcel.Writer.PHPExcel_Writer_Excel5');
		$excel = new \PHPExcel();
		$excel->getActiveSheet()->setTitle('论点下载');
		$header = array('A','B','C','D','E','F');
		$name = array('论题','发布人','论点','立场','状态','时间');
		for($i=0;$i<count($name);$i++){
			$excel->getActiveSheet()->setCellValue("$header[$i]1","$name[$i]");
			$excel->getActiveSheet()->getColumnDimension($header[$i])->setWidth(40);
		}
		
		$article = M('comment')->field('article_id,member_id,content,position,addtime,reply_member_id')->select();
		foreach ($article as $k => $v){
			$where['article_id'] = $v['article_id'];
			$ac = M('article')->where($where)->field('article_title')->find();
			$article[$k]['article_title'] = $ac['article_title'];		
		}
		foreach ($article as $k => $v){
			$where['member_id'] = $v['member_id'];
			$ac = M('member')->where($where)->field('member_name')->find();
			$article[$k]['member_name'] = $ac['member_name'];
		}
			foreach ($article as $k => $v){
			$where['member_id'] = $v['member_id'];
			$ac = M('member')->where($where)->field('member_name')->find();
			$article[$k]['reply_member'] = $ac['member_name'];
		}
		
		$data = array();
		$i = 0;
		foreach ($article as $k => $v){
			$time = date('Y-m-d',$article[$k]['addtime']);
			switch ($article[$k]['position']){
				case '-1':$article[$k]['position'] = '反驳'; break;
				case '0':$article[$k]['position'] = '论点'; break;
				case '1':$article[$k]['position'] = '支持'; break;
			}
			
			$replay = $article[$k]['position'].$v['reply_member'];
			$data[$i]= array($v['article_title'],$v['member_name'],$v['content'],$article[$k]['position'],$replay,$time);
			$i++;
		}
		
		$count = count($data)+1;
		for ($i = 2;$i <= $count;$i++) {
			$j = 0;
			foreach ($data[$i - 2] as $k=>$v) {
				$excel->getActiveSheet()->setCellValue("$header[$j]$i","$v");
				$j++;
			}
		}
		
		
		
		$write = new \PHPExcel_Writer_Excel5($excel);
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control:must-revalidate, post-check=0, pre-check=0");
		header("Content-Type:application/force-download");
		header("Content-Type:application/vnd.ms-execl");
		header("Content-Type:application/octet-stream");
		header("Content-Type:application/download");;
		header('Content-Disposition:attachment;filename="'.$event_title.'-论点下载.xls"');
		header("Content-Transfer-Encoding:binary");
		$write->save('php://output');
	}
	
	
}