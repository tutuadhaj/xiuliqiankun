<?php
/**
 * 商品
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class InviteManageController extends GlobalController {
	public function _initialize() 
	{
        parent::_initialize();
        $this->inviteMod = D('InviteCode');
	}	
			
	//管理
	public function index()
	{
		$totalRows = $this->inviteMod->count();
		$page = new Page($totalRows,10);
        $map = array();
        $list = $this->inviteMod->where($map)->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();
		$this->assign('list',$list);
		$this->assign('page_show',$page->show());				
		$this->display();
	}

    //生成邀请码
    public function add()
    {
        if (IS_POST) {
            $count=$_POST['count'] ? $_POST['count'] :15;
            $data = array();
            $data['add_time']=NOW_TIME;
            $data['status']=0;
            $data['inviter_id']=AID;
            $data['inviter_name']=decrypt(cookie('admin_name'));
            for($i=0;$i<$count;$i++){
                $numbers = range (10,99);
                shuffle ($numbers);
                $code=array_slice($numbers,0,4);
                $invite_code=$code[0].$code[1].$code[2].$code[3];
                $data['invite_code']=$invite_code;
                $dataList[] = $data;
            }
            $res = $this->inviteMod->addAll($dataList);
            if ($res) {
                $this->success('邀请码生成成功');
            }else {
                $this->error('邀请码生成失败');
            }
        }elseif (IS_GET) {
        //            $this->check_login();
        $this->display('add');
        }

    }
	//在线编辑	
	public function ajax()
	{
		$id = intval($_GET['id']);
		if(trim($_GET['branch']) == 'order' || trim($_GET['branch']) == 'state')
		{
  			$this->model->where('id='.$id)->setField($_GET['column'],intval($_GET['value']));
		}
	}
}