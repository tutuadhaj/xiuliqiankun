<?php
/**
 * 广告位置管理
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class AdvPositionController extends GlobalController {

	private $top_links = array(
		array('url'=>'act=AdvPosition&op=index','lang'=>'列表'),
		array('url'=>'act=AdvPosition&op=add','lang'=>'添加'),

	);

	public function _initialize() 
	{
        parent::_initialize();
		$this->model = M('adv_position_set');
	}

	//列表
	public function index()
	{
		$map = array();
		$name = trim($_GET['name']);
		if($name)$map['name'] = array('like','%'.$name.'%');
		
		$totalRows = $this->model->where($map)->count();
		$page = new Page($totalRows,10);	
		$list = $this->model->where($map)->limit($page->firstRow.','.$page->listRows)->order('id asc')->select();

		$this->assign('top_link',$this->sublink($this->top_links,'index'));	
		$this->assign('list',$list);
		$this->assign('search',$_GET);	
		$this->assign('page_show',$page->show());
		$this->display();
	}

	//添加
	public function add()
	{
		if(IS_POST)
		{
			$data = array();
	        $data['name']=$_REQUEST['name']; 
	        $data['ap_code']=$_REQUEST['ap_code']; 
			$return = $this->model->add($data);

			if($return)
			{
				$this->success("操作成功",U('index'));
				exit;
			}
		}else{
			$this->assign('top_link',$this->sublink($this->top_links,'add'));	
			$this->display('add');
		}
	}

    //编辑
    public function edit()
    {
        $id=$_GET['id'];
        if (IS_POST) {
            $data = array();
            if (!empty($_POST['name'])) {
                $data['name'] = str_rp(trimall($_POST['name']));
            }
            if (!empty($_POST['ap_code'])) $data['ap_code']=$_POST['ap_code']; 

            if (!empty($data) && $id) {
                $res = $this->model->where(array('id'=>$id))->save($data);
                try {
                	$this->success('编辑成功',U('index'));
                	exit;
                } catch (Exception $e) {
                	$this->error('编辑失败');
                }
            }
        }elseif (IS_GET) {
        	$this->top_links[]=array('url'=>'act=AdvPosition&op=edit','lang'=>'编辑');
        	$this->assign('top_link',$this->sublink($this->top_links,'edit'));	

            $info=$this->model->where(array('id'=>$id))->find();
            
            $this->assign('vo',$info);
            $this->display('edit');
        }
    }

	//删除
	public function del()
	{
		if(IS_POST){
			//多条删除
			if (!empty($_POST['del_id']))
			{
				if (is_array($_POST['del_id']))
				{
					foreach ($_POST['del_id'] as $del_id)
					{ 
						$this->model->where('id='.$del_id)->delete(); 
					}
					$this->success("操作成功");  	
					exit;						
				}
			}else {
				$this->error("请选择要操作的对象"); 	
			}		
		}elseif(IS_GET){
			//单条删除
			$id=intval($_GET['id']);
			if($id)
			{  
			    $this->model->where('id='.$id)->delete(); 
				$this->success('删除成功！');
				exit;
			}else{
		  		$this->error('添加失败！');
			}
		}
				
	}

}