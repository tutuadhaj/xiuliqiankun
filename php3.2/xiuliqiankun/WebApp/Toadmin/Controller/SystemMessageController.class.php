<?php
/**
 * 广告位置管理
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class SystemMessageController extends GlobalController {

	private $top_links = array(
		array('url'=>'act=SystemMessage&op=index','lang'=>'列表'),
		array('url'=>'act=SystemMessage&op=add','lang'=>'添加'),

	);

	public function _initialize() 
	{
        parent::_initialize();
		$this->model = M('app_system_msg_text');
		$this->model2 = M('app_system_msg');
		$this->jpush =new \Common\Org\Jpush(APP_KEY,MASTER_SECRET,PUSH_URL); //通用公共调用
	}

	//列表
	public function index()
	{
		$map = array();
		$name = trim($_GET['name']);
		if($name)$map['name'] = array('like','%'.$name.'%');

		
		$totalRows = $this->model->where($map)->count();
		$page = new Page($totalRows,10);	
		$list = $this->model->where($map)->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();

		$this->assign('top_link',$this->sublink($this->top_links,'index'));	
		$this->assign('list',$list);
		$this->assign('search',$_GET);	
		$this->assign('page_show',$page->show());
		$this->display();
	}

	//添加
	public function add()
	{

		if(IS_POST)
		{

			$data = array();
	        $data['title']=$_REQUEST['name']; 
	        $data['content']=$_REQUEST['content']; 
	        $data['add_time']=NOW_TIME;

	        /**
	         * 推送
	         */
			//组装需要的参数
			$receive = 'all';     //全部
			$extras  = array('title' => $data['title'],'content' => $data['content']);
			//调用推送,并处理
			$result = $this->jpush->push($receive, $data['title'], $extras);
			if($result['error_code'])
			{
				echo $result['msg'];exit; //推送失败
			}else{
				$return = $this->model->add($data);
				if($return)
				{
					//后期弄成分布发送
					$member_list=M('member')->field('member_id')->select();
					foreach ($member_list as $key => $value) {
						$data2['msg_id']=$return;
						$data2['uid']=0;  //发送者管理员
						$data2['reply_uid']=$value['member_id'];  //接受者id
						$data2['is_read']=0;
						$this->model2->add($data2); 
					}

					$this->success("操作成功",U('index'));
				}
				exit;
			}
			
		}else{
			$this->assign('top_link',$this->sublink($this->top_links,'add'));	
			$this->display('add');
		}
	}

    //编辑
    public function edit()
    {
        $id=$_GET['id'];
        if (IS_POST) {
            $data = array();
            if (!empty($_POST['name'])) {
                $data['title'] = str_rp(trimall($_POST['name']));
            }
            if (!empty($_POST['content'])) {
                $data['content'] = str_rp(trimall($_POST['content']));
            }

            if (!empty($data) && $id) {
                $res = $this->model->where(array('id'=>$id))->save($data);
                try {
                	$this->success('编辑成功',U('index'));
                	exit;
                } catch (Exception $e) {
                	$this->error('编辑失败');
                }
            }
        }elseif (IS_GET) {
        	$this->top_links[]=array('url'=>'act=SystemMessage&op=edit','lang'=>'编辑');
        	$this->assign('top_link',$this->sublink($this->top_links,'edit'));	

            $info=$this->model->where(array('id'=>$id))->find();
            
            $this->assign('vo',$info);
            $this->display('edit');
        }
    }

	//删除
	public function del()
	{
		if(IS_POST){
			//多条删除
			if (!empty($_POST['del_id']))
			{
				if (is_array($_POST['del_id']))
				{
					foreach ($_POST['del_id'] as $del_id)
					{ 
						$this->model->where('id='.$del_id)->delete(); 
						$this->model2->where('msg_id='.$del_id)->delete(); 
					}
					$this->success("操作成功");  	
					exit;						
				}
			}else {
				$this->error("请选择要操作的对象"); 	
			}		
		}elseif(IS_GET){
			//单条删除
			$id=intval($_GET['id']);
			if($id)
			{  
			    $this->model->where('id='.$id)->delete(); 
			    $this->model2->where('msg_id='.$id)->delete(); 
				$this->success('删除成功！');
				exit;
			}else{
		  		$this->error('添加失败！');
			}
		}
				
	}

}