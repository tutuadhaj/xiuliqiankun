<?php
/**
 * 广告
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class ClubManagerController extends GlobalController {
	public function _initialize() 
	{
        parent::_initialize();
		$this->model = M('club');
	}	
	//列表
	public function position()
	{		
		$map = array();
		
		if(trim($_GET['status'])){
			$map['status'] = $_GET['status'];
		}else{
			$map['status'] = 0;
		}
		if(trim($_GET['name']))$map['name'] = array('like','%'.trim($_GET['name']).'%');

			
		$totalRows = $this->model->where($map)->count();
		$page = new Page($totalRows,10);	
		$list = $this->model->where($map)->limit($page->firstRow.','.$page->listRows)->order('id desc')->select();				
		$this->assign('list',$list);
		$this->assign('search',$_GET);	
		$this->assign('show_page',$page->show());			
		$this->display('list');



	}
	
	//编辑
	public function query()
	{
		if(IS_POST)
		{
			$ap_id = intval($_POST['id']);
			$status = intval($_POST['status']);
			
			if($ap_id)
			{
				$data = array();
				$data['status'] = $status;
				$this->model->where('id='.$ap_id)->save($data);
				$this->success("操作成功",U('position'));  	
				exit;											
			}
		}else{
			$ap_id = intval($_GET['id']);
			$vo = $this->model->where('id='.$ap_id)->find();
			// print_r($vo);exit;
			$vo['logo']=$vo['logo'] ? UrlPath.'Uploads/'.$vo['logo'] : '';
			$vo['identification_photo']=$vo['identification_photo'] ? UrlPath.'Uploads/'.$vo['identification_photo'] : '';
			$vo['student_photo']=$vo['student_photo'] ? UrlPath.'Uploads/'.$vo['student_photo'] : '';
			$this->assign('vo',$vo);
			$this->display('query');		
		}
	}

}