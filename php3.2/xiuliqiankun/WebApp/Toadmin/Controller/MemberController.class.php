<?php
/**
 * 会员
 * @copyright  Copyright (c) 2014-2030 muxiangdao-cn Inc.(http://www.muxiangdao.cn)
 * @license    http://www.muxiangdao.cn
 * @link       http://www.muxiangdao.cn
 * @author	   muxiangdao-cn Team
 */
namespace Toadmin\Controller;
use Think\Page;
class MemberController extends GlobalController {
	public function _initialize() 
	{
        parent::_initialize();
		$this->model = M('Member');
	}

	//管理
	public function member()
	{
		$map = array();
		$member_name = trim($_GET['member_name']);
		if($member_name)$map['member_name'] = array('eq',$member_name);
		
		$totalRows = $this->model->where($map)->count();
		$page = new Page($totalRows,10);	
		$list = $this->model->where($map)->limit($page->firstRow.','.$page->listRows)->order('register_time desc')->select();
		$this->assign('list',$list);
		$this->assign('search',$_GET);	
		$this->assign('page_show',$page->show());
		$this->display();
	}

    //编辑用户信息
    public function member_edit()
    {
        $member_id=$_GET['id'];
        if (IS_POST) {
            $data = array();
            if (!empty($_POST['email'])) {
                $data['email'] = str_rp(trimall($_POST['email']));
            }
            if (!empty($_POST['nickname'])) {
                $data['nickname'] = str_rp(trimall($_POST['nickname']));
            }

            if (!empty($_POST['profession'])) {
                $data['profession'] = str_rp(trimall($_POST['profession']));;
            }

            //修改权限
            // $data['comment_status'] = str_rp(trimall($_POST['comment_status']));
            // if($_POST['comment_status']=='1'){
            //     $data['comment_lock_endtime'] =strtotime('+7 day'); //冻结时间暂时定为7天后自动解除
            // }
            //修改用户状态
            $data['status'] = str_rp(trimall($_POST['status']));

            if (!empty($data) && $member_id) {
                $res = $this->model->where(array('member_id'=>$member_id))->save($data);
                if ($res) {
                    $this->success('编辑成功');
                }else {
                    $this->error('编辑失败');
                }
            }
        }elseif (IS_GET) {
//            $this->check_login();
            $info=$this->model->where(array('member_id'=>$member_id))->find();
            $this->assign('vo',$info);
            $this->display('member_edit');
        }
    }

	//删除
	public function member_del()
	{
		if(IS_GET && $_GET['member_id'])
		{
			$this->model->where('member_id='.intval($_GET['member_id']))->delete(); 		
		}
		$this->success("操作成功",U('member'));  	
		exit;			
	}

	//重置密码
	public function resetpwd()
	{
		$member_id = intval($_GET['member_id']);
		if($member_id)
		{
			$pwd = '123456'; //默认重置密码为123456
			$pwd = re_md5($pwd);
			$this->model->where('member_id='.$member_id)->setField('pwd',$pwd);
			$this->success("操作成功",U('member'));  	
			exit;					
		}	
	}	
	//等级设置
	public function degree()
	{
		$MemberDegree = M('MemberDegree');
		if($_GET['ajax_submit'] == 'ok')
		{
			$type = trim($_GET['type']);
			$md_id = intval($_GET['md_id']);
			if($type == 'name')
			{
				$rs = $MemberDegree->where('md_id='.$md_id)->setField('md_name',trim($_GET['md_name']));	
			}else{
				$md_to = intval($_GET['md_to']);
				$md_from = $md_to+1;
				$md_fid = $md_id+1;
				$rs_a = $MemberDegree->where('md_id='.$md_id)->setField('md_to',$md_to);
				$rs_b = $MemberDegree->where('md_id='.$md_fid)->setField('md_from',$md_from);
				$rs = $rs_a && $rs_b;					
			}
			//更新缓存
			if($rs)
			{
/*				if(F('member_degree'))
				{
					F('member_degree',NULL);	
				}
				$member_degree = array();
				$tmp_list = $MemberDegree->order('md_id asc')->select();
				if(!empty($tmp_list))
				{
					foreach ($tmp_list as $val)
					{
						$member_degree[$val['md_from'].'-'.$val['md_to']] = $val;
					}
					F('member_degree', $member_degree); 	
				}*/
				echo json_encode(array('done'=>true));die;				
			}else{
				echo json_encode(array('done'=>false));die;	
			}
		}
		$list = $MemberDegree->order('md_id asc')->select();
		$this->assign('list',$list);
		$this->display('member_degree');	
	}

	//分数设置
	public function score()
	{
		$MemberScore = M('MemberScore');
		if($_GET['ajax_submit'] == 'ok')
		{
			$rs = $MemberScore->where('ss_id='.intval($_GET['ss_id']))->setField(trim($_GET['ss_type']),intval($_GET['value']));				
			if($rs)
			{
				echo json_encode(array('done'=>true));die;
			}else{
				echo json_encode(array('done'=>false));die;
			}
		}
		$list = $MemberScore->order('ss_id asc')->select();
		$this->assign('list',$list);		
		$this->display('member_score');	
	}

    //用户超级权限-----只是控制用户发论题的栏目权限
	public function vip(){
		$member_id = intval($_GET['member_id']);
		if ($member_id) {
			$vip_level = $this->model->where(array('member_id'=>$member_id))->getField('vip_level');
			if ($vip_level) {
				$res = $this->model->where(array('member_id'=>$member_id))->setField('vip_level',0);
			}else {
				$res = $this->model->where(array('member_id'=>$member_id))->setField('vip_level',1);
			}
			if ($res) {
				$this->success('编辑权限成功');
			}else {
				$this->error('编辑权限失败');
			}
		}
	}
}