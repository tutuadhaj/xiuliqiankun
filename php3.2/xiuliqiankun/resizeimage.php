<?php
function imageResize($url, $width, $height) {
header('Content-type: image/jpeg');
list($width_orig, $height_orig) = getimagesize($url);
$ratio_orig = $width_orig/$height_orig;
if ($width/$height > $ratio_orig) {
$width = $height*$ratio_orig;
} else {
$height = $width/$ratio_orig;
}
// This resamples the image
$image_p = imagecreatetruecolor($width, $height);
$image = imagecreatefromjpeg($url);
imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
// Output the image
imagejpeg($image_p, null, 100);
}
//works with both POST and GET
$method = $_SERVER['REQUEST_METHOD'];
if ($method == 'GET') {
imageResize($_GET['url'], $_GET['w'], $_GET['h']);
} elseif ($method == 'POST') {
imageResize($_POST['url'], $_POST['w'], $_POST['h']);
}
// makes the process simpler
function loadImage($url, $width, $height){
echo 'resizeimage.php?url=', urlencode($url) ,
'&w=',$width,
'&h=',$height;
}



/*
*判断类型
 */
/**
 * 获取文件类型
 * @param string $filename 文件名称
 * @return string 文件类型
 */
function getFileType($filename) {
   return substr($filename, strrpos($filename, '.') + 1);
}
 
/**
 * 获取文件类型2
 * @param string $filename 文件名称
 * @return string 文件类型
 */
function getFileType2($filename) {
   return strtolower(pathinfo($filename)['extension']);
}
 
/**
 * 获取文件类型3
 * @param string $filename 文件名称
 * @return string 文件类型
 */
function getFileType3($filename) {
  return $exten[count($exten = explode('.', $filename)) - 1];
}
 
/**
 * 获取文件类型4
 * @param string $filename 文件名称
 * @return string 文件类型
 */
function getFileType4($filename) {
   $exten = explode('.', $filename);
   return end($exten);
}

?>
