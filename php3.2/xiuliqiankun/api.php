<?php
// +----------------------------------------------------------------------
// | 系统版本 V1.0
// +----------------------------------------------------------------------
// | 2015年10月19日
// +----------------------------------------------------------------------
// | Author: David (626218581@qq.com)
// +----------------------------------------------------------------------

// APP客户端数据接口
// 应用入口文件
// 检测PHP环境
if(version_compare(PHP_VERSION,'5.3.0','<'))  die('require PHP > 5.3.0 !');

// define('BIND_MODULE', 'Api'); // 绑定Home模块到当前入口文件
// define('BIND_CONTROLLER','Test'); // 绑定Index控制器到当前入口文件

// 开启调试模式 建议开发阶段开启 部署阶段注释或者设为false
define('APP_DEBUG',True);

// 定义应用目录
define('APP_PATH','./WebApp/');

// 应用api模式 (正式环境改为此模式，本地ip测试会访问不了)
define('APP_MODE','api');

define('API_ITEM_PATH', APP_PATH.'Api/'); //接口项目路径

//绝对路径(根目录)
define('BasePath',str_replace('\\','/',dirname(__FILE__)));

//url项目路径
define('UrlPath','http://'.$_SERVER['HTTP_HOST'].substr($_SERVER['PHP_SELF'],0,strrpos($_SERVER['PHP_SELF'],'/')));

defined('API_COMMON_PATH')  or define('API_COMMON_PATH',    API_ITEM_PATH.'Common/');

//图片地址
defined('PicPath')  or define('PicPath',    UrlPath.'/Uploads/');
//默认头像
defined('DEFAULT_CIRCLE_AVATAR')  or define('DEFAULT_CIRCLE_AVATAR',    UrlPath.'/Uploads/circle_avatar1.png');
//默认头像
defined('DEFAULT_ADV_PIC')  or define('DEFAULT_ADV_PIC',    UrlPath.'/Uploads/default.png');
//默认头像
defined('DEFAULT_MSG_TOPIC_ICON')  or define('DEFAULT_MSG_TOPIC_ICON',    UrlPath.'/Uploads/message_topic_icon.png');
//默认封面
defined('DEFAULT_TOPIC_ICON')  or define('DEFAULT_TOPIC_ICON',    UrlPath.'/Uploads/topic_icon.png');

//全局通用数据定义
$GLOBALS['MsgCodeData']=include(API_ITEM_PATH.'Conf/msg_code_config.php');  //错误信息数组集
$GLOBALS['CheckRequestData']=include(API_ITEM_PATH.'Conf/check_request_data_config.php');  //接口请求参数配置验证数组集

// 引入Tp入口文件
require './ThinkYun/ThinkPHP.php';

// 亲^_^ 后面不需要任何代码了 就是如此简单


