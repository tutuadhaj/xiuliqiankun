
<?php
/************************************************************无限菜单相关函数和获取权限菜单导航*************************************************************************************************************/

/*
 * 自定义入栈函数
 */
function pushStack(&$stack,$channel,$dep){
    array_push($stack, array('channel'=>$channel,'dep'=>$dep));
}
/*
 * 自定义出栈函数
 */
function popStack(&$stack){
    return array_pop($stack);
}

/*
 * 使用非递归，即使用栈的方式实现栏目的无限极分类查询
 */
function get_tree_array($data=array()){
    $stack = array(); //定义一个空栈
    $html = array();  //用来保存各个栏目之间的关系以及该栏目的深度
    //首先将顶级栏目压入栈中
    foreach($data as $key=>$val){
        if($val['parId'] == 0)
            pushStack($stack,$val,0);
    }
    // 将栈中的元素出栈，查找其子栏目
    do{
        $par = popStack($stack); //将栈顶元素出栈
        //查找以此栏目为父级栏目的id，将这些栏目入栈
        for($i=0;$i<count($data);$i++){
            if($data[$i]['parId'] == $par['channel']['id']){
                pushStack($stack,$data[$i],$par['dep']+1);
            }
        }
        //将出栈的栏目以及该栏目的深度保存到数组中
        $html[] = array('parId'=>$par['channel']['parId'],'id'=>$par['channel']['id'],'name'=>$par['channel']['name'],'dep'=>$par['dep'],'args'=>$par['channel']['args']);
    }while(count($stack)>0);
    return $html;
}

/*
 * 获取权限菜单导航
 */
function getPurviewMenuNav($data=array()){
    $all_array=array();  //所有菜单二维数组，以id为索引 array(1=>array(id=>1,),2=>array(id=>2,),3=>array(id=>3,))
    $topMenu=array();  //一级菜单
    $leftMenu=array(); //二级菜单
    $leftMenu1=array();//顶部一级菜单（shopnc格式化）
    $leftMenu2=array();//左边二级菜单（shopnc格式化）
    $menu=array();   //合并顶部和左边菜单（shopnc格式化）
    $dataSources=get_tree_array($data);
    foreach($dataSources as $key=>$value){
        if($value['dep']==0){
//        $level1[$value['parId']][]=$value;
            $topMenu['top'][]=$value;
        }elseif($value['dep']==1){
//        $level2[$value['parId']][]=$value;
            $leftMenu['left'][]=$value;
        }elseif($value['dep']==2){
//        $level3[$value['parId']][]=$value;
        }
        $all_array[$value["id"]]=$value; //取得所有菜单二维数组，以id为索引（方便后面获取任何一个菜单的信息）
    }
//重组顶部菜单数组
    foreach($topMenu['top'] as $key=>$value){
        $value['text']=$all_array[$value['id']]['name'];
        $value['args']=$all_array[$value['id']]['args'];
        $leftMenu1['top'][$value['id']]['text']=$value['text'];
        $leftMenu1['top'][$value['id']]['args']=$value['args'];
    }
//重组左边菜单数组
    foreach($leftMenu['left'] as $key=>$value){
        $value['text']=$all_array[$value['parId']]['name'];
        $value['args']=$all_array[$value['parId']]['args'];
        $value['args_chilid']=$all_array[$value['id']]['args'];
        $temp=$value;
        $temp['args']=$temp['args_chilid'];
        $leftMenu2['left'][$temp['parId']]['nav']=$value['args'];
        $leftMenu2['left'][$temp['parId']]['text']=$temp['text'];
        unset($temp['args_chilid']);
        $temp['text']=$temp['name'];
        unset($temp['name']);
        $leftMenu2['left'][$temp['parId']]['list'][]=$temp;

    }
//合并顶部和左边菜单数组
    $menu['top']=$leftMenu1['top'];
    $menu['left']=$leftMenu2['left'];
    return $menu;
}

//测试方法
$channels = array(
    array('id'=>1,'name'=>"衣服",'parId'=>0,'args'=>'clothes'),
    array('id'=>2,'name'=>"书籍",'parId'=>0,'args'=>'books'),
    array('id'=>3,'name'=>"T恤",'parId'=>1,'args'=>'func1,upload,clothes'),
    array('id'=>4,'name'=>"裤子",'parId'=>1,'args'=>'func2,upload,clothes'),
    array('id'=>5,'name'=>"鞋子",'parId'=>1,'args'=>'func3,upload,clothes'),
//    array('id'=>6,'name'=>"皮鞋",'parId'=>5),
//    array('id'=>7,'name'=>"运动鞋",'parId'=>5),
//    array('id'=>8,'name'=>"耐克",'parId'=>7),
//    array('id'=>9,'name'=>"耐克",'parId'=>3),
//    array('id'=>10,'name'=>"鸿星尔克",'parId'=>7),
    array('id'=>11,'name'=>"小说",'parId'=>2,'args'=>'func1,upload,books'),
//    array('id'=>12,'name'=>"科幻小说",'parId'=>11),
//    array('id'=>13,'name'=>"古典名著",'parId'=>11),
    array('id'=>14,'name'=>"文学",'parId'=>2,'args'=>'func1,upload,books'),
//    array('id'=>15,'name'=>"四书五经",'parId'=>14)
);
$menu=getPurviewMenuNav($channels);

//将菜单写入文件缓存
//$content = var_export($menu, TRUE);
//$path=RUNTIME_PATH.'Exts/mebu.php';
//file_put_contents($path, "<?php "."\n"."return ".$content.";");
//读取缓存文件
//    $path=RUNTIME_PATH.'Exts/keyword.php';
//    $data= include($path);

print_r($menu);
exit;


/*
 * --------------------------------------------------------------------------------------------------------------------------
 *php常用代码:switch
 *
 *     switch (ACTION_NAME)
        {
            case 'index':
                $check_fields=$this->index_check_fields;
                break;
            case 2:
                $check_fields=$this->index_check_fields;
                break;
            case 3:
                $check_fields=$this->index_check_fields;
                break;
            default:
                break;
        }
 *
 *
 *
 * * --------------------------------------------------------------------------------------------------------------------------
*/

?>