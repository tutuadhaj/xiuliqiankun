<?php
/**
 * 递归无限级分类【先序遍历算】，获取任意节点下所有子孩子
 * @param array $arrCate 待排序的数组
 * @param int $parent_id 父级节点
 * @param int $level 层级数
 * @return array $arrTree 排序后的数组
 */
function getMenuTree($arrCat, $parent_id = 0, $level = 0)
{
    static  $arrTree = array(); //使用static代替global
    if( empty($arrCat)) return FALSE;
    $level++;
    foreach($arrCat as $key => $value)
    {
        if($value['parent_id' ] == $parent_id)
        {
            $value[ 'level'] = $level;
            $arrTree[] = $value;
            unset($arrCat[$key]); //注销当前节点数据，减少已无用的遍历
            getMenuTree($arrCat, $value[ 'id'], $level);
        }
    }
   
    return $arrTree;
}


/**
 * 测试数据
 */
$arrCate = array(  //待排序数组
  array( 'id'=>1, 'name' =>'顶级栏目一', 'parent_id'=>0),
  array( 'id'=>2, 'name' =>'顶级栏目二', 'parent_id'=>0),
  array( 'id'=>3, 'name' =>'栏目三', 'parent_id'=>1),
  array( 'id'=>4, 'name' =>'栏目四', 'parent_id'=>3),
  array( 'id'=>5, 'name' =>'栏目五', 'parent_id'=>4),
  array( 'id'=>6, 'name' =>'栏目六', 'parent_id'=>2),
  array( 'id'=>7, 'name' =>'栏目七', 'parent_id'=>6),
  array( 'id'=>8, 'name' =>'栏目八', 'parent_id'=>6),
  array( 'id'=>9, 'name' =>'栏目九', 'parent_id'=>7),
);

// header('Content-type:text/html; charset=utf-8'); //设置utf-8编码
// echo '<pre>';
// print_r(getMenuTree($arrCate, 0, 0));
// echo '</pre>';
// exit;
?>

<?php

/************************************************************无限菜单相关函数和获取权限菜单导航*************************************************************************************************************/

/*
 * 自定义入栈函数
 */
function pushStack(&$stack,$channel,$dep){
    array_push($stack, array('channel'=>$channel,'dep'=>$dep));
}
/*
 * 自定义出栈函数
 */
function popStack(&$stack){
    return array_pop($stack);
}

/*
 * 使用非递归，即使用栈的方式实现栏目的无限极分类查询
 */
function get_tree_array($data=array()){
    $stack = array(); //定义一个空栈
    $html = array();  //用来保存各个栏目之间的关系以及该栏目的深度
    //首先将顶级栏目压入栈中
    foreach($data as $key=>$val){
        if($val['parId'] == 0)
            pushStack($stack,$val,0);
    }
    // 将栈中的元素出栈，查找其子栏目
    do{
        $par = popStack($stack); //将栈顶元素出栈
        //查找以此栏目为父级栏目的id，将这些栏目入栈
        for($i=0;$i<count($data);$i++){
            if($data[$i]['parId'] == $par['channel']['id']){
                pushStack($stack,$data[$i],$par['dep']+1);
            }
        }
        //将出栈的栏目以及该栏目的深度保存到数组中
        $html[] = array('parId'=>$par['channel']['parId'],'id'=>$par['channel']['id'],'name'=>$par['channel']['name'],'dep'=>$par['dep'],'args'=>$par['channel']['args']);
    }while(count($stack)>0);
    return $html;
}

/*
 * 获取权限菜单导航
 */
function getPurviewMenuNav($data=array()){
    $all_array=array();  //所有菜单二维数组，以id为索引 array(1=>array(id=>1,),2=>array(id=>2,),3=>array(id=>3,))
    $topMenu=array();  //一级菜单
    $leftMenu=array(); //二级菜单
    $leftMenu1=array();//顶部一级菜单（shopnc格式化）
    $leftMenu2=array();//左边二级菜单（shopnc格式化）
    $menu=array();   //合并顶部和左边菜单（shopnc格式化）
    $dataSources=get_tree_array($data);
    foreach($dataSources as $key=>$value){
        if($value['dep']==0){
//        $level1[$value['parId']][]=$value;
            $topMenu['top'][]=$value;
        }elseif($value['dep']==1){
//        $level2[$value['parId']][]=$value;
            $leftMenu['left'][]=$value;
        }elseif($value['dep']==2){
//        $level3[$value['parId']][]=$value;
        }
        $all_array[$value["id"]]=$value; //取得所有菜单二维数组，以id为索引（方便后面获取任何一个菜单的信息）
    }
//重组顶部菜单数组
    foreach($topMenu['top'] as $key=>$value){
        $value['text']=$all_array[$value['id']]['name'];
        $value['args']=$all_array[$value['id']]['args'];
        $leftMenu1['top'][$value['id']]['text']=$value['text'];
        $leftMenu1['top'][$value['id']]['args']=$value['args'];
    }
//重组左边菜单数组
    foreach($leftMenu['left'] as $key=>$value){
        $value['text']=$all_array[$value['parId']]['name'];
        $value['args']=$all_array[$value['parId']]['args'];
        $value['args_chilid']=$all_array[$value['id']]['args'];
        $temp=$value;
        $temp['args']=$temp['args_chilid'];
        $leftMenu2['left'][$temp['parId']]['nav']=$value['args'];
        $leftMenu2['left'][$temp['parId']]['text']=$temp['text'];
        unset($temp['args_chilid']);
        $temp['text']=$temp['name'];
        unset($temp['name']);
        $leftMenu2['left'][$temp['parId']]['list'][]=$temp;

    }
//合并顶部和左边菜单数组
    $menu['top']=$leftMenu1['top'];
    $menu['left']=$leftMenu2['left'];
    return $all_array;
}

//测试方法
$channels = array(
    array('id'=>1,'name'=>"衣服",'parId'=>0,'args'=>'clothes'),
    array('id'=>2,'name'=>"书籍",'parId'=>0,'args'=>'books'),
    array('id'=>3,'name'=>"T恤",'parId'=>1,'args'=>'func1,upload,clothes'),
    array('id'=>4,'name'=>"裤子",'parId'=>1,'args'=>'func2,upload,clothes'),
    array('id'=>5,'name'=>"鞋子",'parId'=>1,'args'=>'func3,upload,clothes'),
    array('id'=>11,'name'=>"小说",'parId'=>2,'args'=>'func1,upload,books'),
    array('id'=>14,'name'=>"文学",'parId'=>2,'args'=>'func1,upload,books'),
);
$menu=getPurviewMenuNav($channels);

print_r($menu);
exit;

?>


